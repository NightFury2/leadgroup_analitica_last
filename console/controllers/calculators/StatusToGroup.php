<?php

namespace console\controllers\calculators;

use Yii;
use yii\helpers\Console;

class StatusToGroup extends BaseStatistic
{

    public static function calculate($controller, $interval = '-1 DAY')
    {
        $controller->stdout("Updating... ");

        Yii::$app->db->createCommand("
                 UPDATE `order` 
                 LEFT JOIN statuses_list
                     ON `order`.offer_id = statuses_list.offer_id AND `order`.status_id = statuses_list.personal_id
                 SET `order`.status_id = statuses_list.`group`
                 WHERE DATE(created_at) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();

        $controller->stdout("Ok\n", Console::FG_GREEN);

        return 0;
    }

}
