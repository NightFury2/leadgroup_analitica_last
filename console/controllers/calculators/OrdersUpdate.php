<?php

namespace console\controllers\calculators;

use Yii;
use yii\helpers\Console;

/**
 * Class OrdersUpdate
 * @package console\controllers\calculators
 */
class OrdersUpdate extends BaseStatistic
{
    /**
     * @inheritdoc
     */
    public static function calculate($controller, $interval = '-1 DAY')
    {
        $controller->stdout("Updating orders... ");

        Yii::$app->db->createCommand("
            UPDATE `order` 
              JOIN order_history ON `order`.id = order_history.order_id
               AND DATE(order_history.`datetime`) >= (CURDATE() + INTERVAL {$interval})
            SET status_id = status_id_to
        ")->execute();

        $controller->stdout("Ok\n", Console::FG_GREEN);

        return 0;
    }
}