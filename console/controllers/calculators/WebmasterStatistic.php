<?php

namespace console\controllers\calculators;

use Yii;
use yii\helpers\Console;

class WebmasterStatistic extends BaseStatistic implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public static function calculate($controller, $interval = '-1 MONTH')
    {
        $controller->stdout("Getting webmaster statistic... ");
        $orders = Yii::$app->db->createCommand("
            SELECT
               DATE(created_at) as `date`,
               country_id,
               offer_id,
               wm_id,
               count(*)            AS total,
               count(CASE WHEN status_id IN (1, 2)
                 THEN 1 END)       AS `approved`,
               count(CASE WHEN status_id = 2
                 THEN 1 END)       AS `paid`,
               count(CASE WHEN status_id = -2
                 THEN 1 END)       AS `error`,
               count(CASE WHEN status_id = -1
                 THEN 1 END)       AS `canceled`
             FROM `order`
             WHERE DATE(created_at) >= (CURDATE() + INTERVAL {$interval}) AND wm_id != 0
             GROUP BY wm_id, offer_id, DATE(created_at)       
        ")->queryAll();
        $controller->stdout("Ok\n", Console::FG_GREEN);

        ///
        $controller->stdout("Begin transaction\n", Console::FG_GREEN);

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            ///
            $controller->stdout("Clean old data... ");
            $connection->createCommand("
                DELETE FROM webmaster_statistic WHERE DATE(`date`) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

            ///
            $controller->stdout("Saving orders... ");
            Yii::$app->db->createCommand()->batchInsert('webmaster_statistic', array_keys($orders[0]), array_values($orders))->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

            ///
            $controller->stdout("Commit... ");
            $transaction->commit();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///
        } catch (\Exception $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        }
        ///

        return 0;
    }
}
