<?php

namespace console\controllers\calculators;


interface CalculatorInterface
{
    public static function calculate($controller, $interval);
}