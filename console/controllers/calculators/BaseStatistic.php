<?php

namespace console\controllers\calculators;

/**
 * Class BaseStatistic
 * @package console\controllers\calculators
 */
class BaseStatistic implements CalculatorInterface
{
    /**
     * @param $controller \console\controllers\CalculateController
     * @param $interval - Interval
     * @return bool
     */
    public static function calculate($controller, $interval)
    {
        return true;
    }

}