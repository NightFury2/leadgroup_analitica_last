<?php

namespace console\controllers\calculators;

use Yii;
use yii\helpers\Console;

/**
 * Class ConverseStatistic
 * @package console\controllers\calculators
 */
class ConverseStatistic extends BaseStatistic implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public static function calculate($controller, $interval)
    {
        $controller->stdout("Getting orders statistic... ");
        $orders = Yii::$app->db->createCommand("
            SELECT
               DATE(created_at) as `date`,  
               country_id,
               `order`.offer_id,
               count(*)            AS total,
               count(CASE WHEN status_id = 1
                 THEN 1 END)       AS `approved`,
               count(CASE WHEN status_id = 2
                 THEN 1 END)       AS `paid`,
               count(CASE WHEN status_id = -2
                 THEN 1 END)       AS `error`,
               count(CASE WHEN status_id = -1
                 THEN 1 END)       AS `canceled`,
               count(CASE WHEN status_id = -3
                 THEN 1 END)       AS `return`,
               count(CASE WHEN status_id = 0
                 THEN 1 END)       AS `processed`
             FROM `order`
             WHERE DATE(created_at) >= (CURDATE() + INTERVAL {$interval})
             GROUP BY `order`.offer_id, DATE(created_at)       
        ")->queryAll();
        $controller->stdout("Ok\n", Console::FG_GREEN);

        $controller->stdout("Getting average bills... ");

        $averages = Yii::$app->db->createCommand("
            SELECT
              AVG(total) as average_bill,
              `order`.offer_id,
              DATE(created_at) as `date`
            FROM `order`
            WHERE status_id IN (2, 1) AND DATE(created_at) >= (CURDATE() + INTERVAL {$interval})
            GROUP BY `order`.offer_id, DATE(created_at)        
        ")->queryAll();
        $controller->stdout("Ok\n", Console::FG_GREEN);

        ///
        $controller->stdout("Begin transaction\n", Console::FG_GREEN);

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            ///
            $controller->stdout("Clean old data... ");
            $connection->createCommand("
                DELETE FROM converse_statistic WHERE DATE(`date`) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

            ///
            $controller->stdout("Saving orders... ");
            Yii::$app->db->createCommand()->batchInsert('converse_statistic', array_keys($orders[0]), array_values($orders))->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

            ///
            $controller->stdout("Saving average bills");

            foreach ($averages as $average) {
                $controller->stdout(".");

                Yii::$app->db->createCommand()->update('converse_statistic', [
                    'average_bill' => $average['average_bill']
                ], [
                    'offer_id' => $average['offer_id'],
                    'date' => $average['date']
                ])->execute();
            }

            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

            ///
            $controller->stdout("Commit... ");
            $transaction->commit();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///
        } catch (\Exception $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        }
        ///

        return 0;
    }
}