<?php

namespace console\controllers\calculators;

use yii\helpers\Console;
use Yii;

class OperatorStatistic extends BaseStatistic implements CalculatorInterface
{

    /**
     * @inheritdoc
     */
    public static function calculate($controller, $interval = '-1 MONTH')
    {
        $controller->stdout("Getting operator statistic... ");
        $statistic = Yii::$app->db->createCommand("
            SELECT
              offer_id,
              user_id                  AS operator_id,
              operator.country_id      AS country_id,
              date(datetime)           AS `date`,
              count(CASE WHEN status_id_from = 0 AND status_id_to = 1
                THEN 1 END)            AS `approved`,
              count(CASE WHEN status_id_from = 1 AND status_id_to = 2
                THEN 1 END)            AS `buyout`,
              count(DISTINCT order_id) AS total
            FROM order_history
              LEFT JOIN operator
                ON operator.id = user_id
            WHERE order_history.user_type = 2 AND DATE(datetime) >= (CURDATE() + INTERVAL {$interval})
            GROUP BY DATE(datetime), offer_id, user_id
        ")->queryAll();
        $controller->stdout("Ok\n", Console::FG_GREEN);

        $controller->stdout("Getting average bills... ");
        $averages = Yii::$app->db->createCommand("
            SELECT
              AVG(total)     AS average_bill,
              operator.id    AS operator_id,
              DATE(datetime) AS `date`
            FROM `order`
              LEFT JOIN order_history
                ON order_id = `order`.id AND user_type = 2
              RIGHT JOIN operator
                ON operator.id = order_history.user_id
            WHERE (status_id_from = 0 AND status_id_to = 1
                   OR status_id_from = 1 AND status_id_to = 2)
                  AND DATE(datetime) >= (CURDATE() + INTERVAL {$interval})
            GROUP BY operator_id, DATE(datetime)  
        ")->queryAll();
        $controller->stdout("Ok\n", Console::FG_GREEN);

        ///
        $controller->stdout("Begin transaction\n", Console::FG_GREEN);

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            ///
            $controller->stdout("Clean old data... ");
            $connection->createCommand("
                DELETE FROM operator_statistic WHERE DATE(`date`) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///
            ///
            $controller->stdout("Saving statistic... ");
            Yii::$app->db->createCommand()->batchInsert('operator_statistic', array_keys($statistic[0]), array_values($statistic))->execute();
            $controller->stdout("Ok\n", Console::FG_GREEN);

            ///
            $controller->stdout("Saving average bills");

            foreach ($averages as $average) {
                $controller->stdout(".");

                Yii::$app->db->createCommand()->update('operator_statistic', [
                    'average_bill' => $average['average_bill']
                ], [
                    'operator_id' => $average['operator_id'],
                    'date' => $average['date']
                ])->execute();
            }

            $controller->stdout("Ok\n", Console::FG_GREEN);
            ///

        } catch (\Exception $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $controller->stdout("ROLLBACK\n", Console::FG_RED);
            $transaction->rollBack();
            throw $e;
        }


        return 0;
    }

}
