<?php

namespace console\controllers\calculators;

use console\controllers\ConsoleController;
use Scheb\YahooFinanceApi\Exception\ApiException;
use Yii;
use yii\helpers\Console;
use frontend\models\Country;
use Scheb\YahooFinanceApi\ApiClient as CurrencyClient;

/**
 * Class CurrencyUpdate
 * @package console\controllers\calculators
 */
class CurrencyUpdate extends BaseStatistic implements CalculatorInterface
{
    /**
     * @inheritdoc
     */
    public static function calculate($controller, $interval)
    {

        ///
        $controller->stdout("Getting countries... ");
        $currenciesSource = Country::find()->select(['currency'])->all();
        $currencies = [];

        foreach ($currenciesSource as $currency) {
            $currencies[] = 'USD' . $currency['currency'];
        }
        $controller->stdout("Ok\n", Console::FG_GREEN);
        ///

        $data = self::getRates($controller, $currencies);

        ///
        $controller->stdout("Updating");
        foreach ($data['query']['results']['rate'] as $rate) {
            $controller->stdout('.');

            $currency = explode('/', $rate['Name'])[1];
            $rate = $rate['Rate'];

            Yii::$app->db->createCommand("
                UPDATE converse_statistic 
                  INNER JOIN country ON country_id = country.id 
                SET average_bill_usd = average_bill / {$rate}
                WHERE country.currency = '{$currency}' 
                  AND DATE(date) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();

            Yii::$app->db->createCommand("
                UPDATE operator_statistic 
                  INNER JOIN country ON country_id = country.id 
                SET average_bill_usd = average_bill / {$rate}  
                WHERE country.currency = '{$currency}' 
                  AND DATE(date) >= (CURDATE() + INTERVAL {$interval})
            ")->execute();
        }
        $controller->stdout("Ok\n", Console::FG_GREEN);
        ///

        return 0;
    }

    /**
     * @param $controller ConsoleController
     * @param $currencies
     *
     * @return array $data
     */
    private static function getRates($controller, $currencies)
    {
        $controller->stdout("Getting rates... ");
        $client = new CurrencyClient();

        try {
            $data = $client->getCurrenciesExchangeRate($currencies);
            Yii::$app->redis->set('currency', serialize($data));
        } catch (ApiException $e) {
            $controller->stdout("{$e->getMessage()}\n", Console::FG_RED);
            $controller->stdout("Try use cached rates\n", Console::FG_GREEN);

            $data = unserialize(Yii::$app->redis->get('currency'));

            if (!$data || !is_array($data)) {
                $controller->stdout("No data in redis =( return empty array\n", Console::FG_RED);

                return [];
            }
        }

        $controller->stdout("Ok\n", Console::FG_GREEN);

        return $data;
    }
}