<?php

namespace console\controllers;

use yii\helpers\Console;
use yii;

class MonthReplicateController extends ConsoleController
{
    public function actionOrders($month, $day)
    {
        return $this->simpleReplication('Orders', 'order',
            (new yii\db\Query())
                ->select([
                    'id', 'total', 'datetime as created_at', 'status as status_id', 'offerID as offer_id', 'ownerID as country_id'
                ])->from('orders')
                ->where("YEAR(datetime) = 2017 AND MONTH(datetime) = {$month} AND DAY(datetime) = {$day}")
        );
    }

    public function actionOrderHistory($month, $day)
    {
        return $this->simpleReplication('Order history', 'order_history',
            (new yii\db\Query())
                ->select([
                    'id', 'orderID as order_id', 'oldStatusGroup as status_id_from', 'newStatusGroup as status_id_to', 'timeSave as datetime',
                    'userType as user_type', 'userID as user_id', 'actionType as action_id'
                ])->from('orderHistory')
                ->where("YEAR(timeSave) = 2017 AND MONTH(timeSave) = {$month} AND DAY(timeSave) = {$day}")
        );
    }

    public function actionOperatorQueries($month, $day)
    {
        return $this->simpleReplication('Operators statistic by queries', 'operator_query_statistic',
            (new yii\db\Query())
                ->select([
                    'id', '`datetime`', 'userID as operator_id', '`count`'
                ])->from('operatorStatByQueries')
                ->where("YEAR(datetime) = 2017 AND MONTH(datetime) = {$month} AND DAY(datetime) = {$day}")
        );
    }
}