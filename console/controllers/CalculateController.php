<?php

namespace console\controllers;

use console\controllers\calculators\ConverseStatistic;
use console\controllers\calculators\CurrencyUpdate;
use console\controllers\calculators\OrdersUpdate;
use console\controllers\calculators\StatusToGroup;
use console\controllers\calculators\OperatorStatistic;
use console\controllers\calculators\WebmasterStatistic;
use yii\helpers\Console;

/**
 * Class CalculateController
 * @package console\controllers
 */
class CalculateController extends ConsoleController
{

    /**
     * @inheritdoc
     */
    public function getHelpSummary()
    {
        return 'Calculate statistics';
    }

    /**
     * Calculate all statistics
     */
    public function actionAll()
    {
        $this->stdout("CALCULATE ALL STATISTICS\n\n", Console::FG_RED);

        $this->actionUpdateStatus();
        $this->actionUpdateOrders();
        $this->actionConversions();
        $this->actionOperatorStatistic();
        $this->actionWebmasters();
        $this->actionCurrency();

        return self::EXIT_CODE_NORMAL;
    }

    public function actionUpdateStatus($interval = "-1 DAY")
    {
        return StatusToGroup::calculate($this, $interval);
    }

    /**
     * Calculate conversions statistic
     * @param $interval - calculate interval
     * @return bool
     */
    public function actionConversions($interval = "-1 MONTH")
    {
        return ConverseStatistic::calculate($this, $interval);
    }

    /**
     * Calculate operator statistic
     * @param $interval - calculate interval
     * @return bool
     */
    public function actionOperatorStatistic($interval = "-1 DAY")
    {
        return OperatorStatistic::calculate($this, $interval);
    }

    /**
     * Update orders
     * @param $interval - calculate interval
     * @return bool
     */
    public function actionUpdateOrders($interval = "-1 DAY")
    {
        return OrdersUpdate::calculate($this, $interval);
    }

    /**
     * Update currency
     * @param $interval - calculate interval
     * @return bool
     */
    public function actionCurrency($interval = "-1 MONTH")
    {
        return CurrencyUpdate::calculate($this, $interval);
    }

    /**
     * Calculate webmasters statistic
     * @param $interval - calculate interval
     * @return bool
     */
    public function actionWebmasters($interval = "-1 MONTH")
    {
        return WebmasterStatistic::calculate($this, $interval);
    }

}