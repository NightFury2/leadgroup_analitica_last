<?php

namespace console\controllers;

use yii\helpers\Console;
use yii;

/**
 * Class ConsoleController
 * @package console\controllers
 */
class ReplicateController extends ConsoleController
{
    /**
     * @inheritdoc
     */
    public function getHelpSummary()
    {
        return 'Replicate DB';
    }

    /**
     * Replicate all tables
     * @return bool
     */
    public function actionAll()
    {
        $this->stdout("REPLICATE ALL TABLES\n\n", Console::FG_RED);

        $this->actionOperators();
        $this->actionOperatorToOffer();
        $this->actionStatuses();
        $this->actionOrders();
        $this->actionOperatorsStatistic();
        $this->actionOrderHistory();
        $this->actionOffers();
        $this->actionCountries();
        $this->actionWebmasters();

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * Replicate operators table
     * @return bool
     */
    public function actionOperators()
    {
        return $this->simpleReplication('Operators', 'operator',
            (new yii\db\Query())
                ->select([
                    'id', 'ownerID as country_id', 'fio as name', 'login'
                ])->from('operators')
                //->where('ownerID != 0')
            , true);
    }

    /**
     * Replicate operators to offer table
     * @return bool
     */
    public function actionOperatorToOffer()
    {
        return $this->simpleReplication('Operator to offer', 'operator_to_offer',
            (new yii\db\Query())
                ->select([
                    'id', 'offerID as offer_id', 'userID as operator_id', 'activeTo as active_to'
                ])->from('operator2offer')
                ->where("offerID IN (SELECT id FROM offers WHERE ownerID IN (SELECT id FROM admins WHERE banned = 0) AND tariff NOT IN ('demo', 'primary'))")
            , true);
    }

    /**
     * Replicate operators statistic by queries table
     * @return bool
     */
    public function actionOperatorsStatistic()
    {
        return $this->simpleReplication('Operators statistic by queries', 'operator_query_statistic',
            (new yii\db\Query())
                ->select([
                    'id', '`datetime`', 'userID as operator_id', '`count`'
                ])->from('operatorStatByQueries')
                ->where("DATE(datetime) = (CURDATE() + INTERVAL -1 DAY)")
        );
    }

    /**
     * Replicate statuses table
     * @return bool
     */
    public function actionStatuses()
    {
        return $this->simpleReplication('Statuses', 'statuses_list',
            (new yii\db\Query())
                ->select([
                    'id', 'personalID as personal_id', 'group', 'offerID as offer_id'
                ])->from('statuses')
                ->where("offerID IN (SELECT id FROM offers WHERE ownerID IN (SELECT id FROM admins WHERE banned = 0) AND tariff NOT IN ('demo', 'primary'))")
            , true);
    }

    /**
     * Replicate orders
     * @return bool
     */
    public function actionOrders()
    {
        return $this->simpleReplication('Orders', 'order',
            (new yii\db\Query())
                ->select([
                    'id', 'total', 'datetime as created_at', 'status as status_id', 'offerID as offer_id', 'ownerID as country_id'
                ])->from('orders')
                ->where("DATE(datetime) = (CURDATE() + INTERVAL -1 DAY)")
        );
    }

    /**
     * Replicate order history table
     * @return bool
     */
    public function actionOrderHistory()
    {
        return $this->simpleReplication('Order history', 'order_history',
            (new yii\db\Query())
                ->select([
                    'id', 'orderID as order_id', 'oldStatusGroup as status_id_from', 'newStatusGroup as status_id_to', 'timeSave as datetime',
                    'userType as user_type', 'userID as user_id', 'offerID as offer_id'
                ])->from('orderHistory')
                ->where("DATE(timeSave) = (CURDATE() + INTERVAL -1 DAY)")
        );
    }

    /**
     * Replicate offers
     * @return bool
     */
    public function actionOffers()
    {
        return $this->simpleReplication('Offers', 'offer',
            (new yii\db\Query())
                ->select([
                    'id', 'title', 'name', 'ownerID as country_id', 'activeTo as active_to'
                ])->from('offers')
                ->where("ownerID IN (SELECT id FROM admins WHERE banned = 0) AND tariff NOT IN ('demo', 'primary') AND activeTo >= NOW()")
            , true);
    }

    /**
     * Replicate countries
     * @return bool
     */
    public function actionCountries()
    {
        return $this->simpleReplication('Countries', 'country',
            (new yii\db\Query())
                ->select([
                    'id', 'country as code', 'realName as name', 'currency', 'login'
                ])->from('admins')
                ->where('banned = 0')
            , true);
    }

    /**
     * Replicate webmasters table
     * @return bool
     */
    public function actionWebmasters()
    {
        return $this->simpleReplication('Webmasters', 'webmaster',
            (new yii\db\Query())
                ->select([
                    'id', 'personalID as personal_id', 'ownerID as country_id', 'login'
                ])->from('webmasters')
            , true);
    }
}