<?php

namespace console\controllers;

use backend\models\ConsoleHistory;
use yii;
use yii\helpers\Console;
use yii\console\Controller;

/**
 * Class ConsoleController
 * @package console\controllers
 */
class ConsoleController extends Controller
{
    /**
     * @var float script start microtime
     */
    protected $startTime = 0.0;

    /**
     * @var string console output
     */
    protected $output;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->startTime = microtime(true);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $output
     * @param null $color
     * @return bool|int
     */
    public function stdout($output, $color = null)
    {
        $this->output .= $output;

        return parent::stdout($output, $color);
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $totalTime = round(microtime(true) - $this->startTime, 3);
        $this->stdout("Done for {$totalTime}s\n\n");

        $consoleHistory = new ConsoleHistory();

        $consoleHistory->total_time = $totalTime;
        $consoleHistory->total_memory = memory_get_usage(true) / 1024 / 1024;
        $consoleHistory->controller = self::className() . '/'. $action->id;
        $consoleHistory->status = $result;
        $consoleHistory->action = self::className() . '/'. $action->id;
        $consoleHistory->output = $this->output;

        $consoleHistory->save();
    }

    /**
     * Truncate table
     *
     * @param $table - Local table
     * @return bool
     */
    protected function truncateTable($table)
    {
        $this->stdout("Cleaning {$table}...");
        Yii::$app->db->createCommand("TRUNCATE TABLE `{$table}`")->execute();
        $this->stdout("Ok\n", Console::FG_GREEN);

        return true;
    }

    /**
     * Replication skeleton
     *
     * @param $action - Action name
     * @param $table - Local table
     * @param $truncate - Truncate local table
     * @param $data \yii\db\Query
     *
     * @return integer exit code
     */
    protected function simpleReplication($action, $table, $data, $truncate = false)
    {
        $this->stdout("{$action}\n", Console::FG_GREEN);

        if ($truncate) {
            $this->truncateTable($table);
        }

        $this->stdout("Working");

        foreach ($data->batch(1000, Yii::$app->db_remote) as $list) {
            $this->stdout(".");
            Yii::$app->db->createCommand()->batchInsert($table, array_keys($list[0]), array_values($list))->execute();
        }

        $this->stdout(" Ok\n", Console::FG_GREEN);

        return self::EXIT_CODE_NORMAL;
    }
}