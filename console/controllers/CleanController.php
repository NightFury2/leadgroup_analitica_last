<?php

namespace console\controllers;

use yii\helpers\Console;
use yii;
use filsh\yii2\oauth2server\models\OauthAccessTokens;
use filsh\yii2\oauth2server\models\OauthRefreshTokens;

/**
 * Class CleanController
 * @package console\controllers
 */
class CleanController extends ConsoleController
{
    /**
     * @inheritdoc
     */
    public function getHelpSummary()
    {
        return 'Clean database';
    }

    /**
     * Clean all temp tables
     */
    public function actionAll()
    {
        $this->actionOauth();
        $this->actionCache();

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * Clean oAuth2 old access and refresh tokens
     */
    public function actionOauth()
    {
        $this->stdout("Cleaning oAuth2 old access tokens... ");

        OauthAccessTokens::deleteAll(new \yii\db\Expression('expires < NOW()'));
        OauthRefreshTokens::deleteAll(new \yii\db\Expression('expires < NOW()'));

        $this->stdout("Ok\n", Console::FG_GREEN);

        return self::EXIT_CODE_NORMAL;
    }

    /**
     * Clean cache
     */
    public function actionCache()
    {
        $this->stdout("Cleaning cache...\n");
        $this->stdout(Yii::$app->runAction('cache/flush', ['cache', 'interactive' => false]));
        $this->stdout("\nOk\n", Console::FG_GREEN);

        return self::EXIT_CODE_NORMAL;
    }


}