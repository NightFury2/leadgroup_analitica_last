<?php

use yii\db\Migration;

class m170606_144304_add_console_history_output_column extends Migration
{
    public function up()
    {
        $this->addColumn('console_history', 'output', $this->text());
    }

    public function down()
    {
        echo "m170606_144304_add_console_log_output_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
