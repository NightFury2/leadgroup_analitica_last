<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m170406_114207_create_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey()->unique(),
            'code' => $this->string(3)->notNull(),
            'name' => $this->string(32)->notNull(),
            'currency' => $this->string(5)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
    }
}
