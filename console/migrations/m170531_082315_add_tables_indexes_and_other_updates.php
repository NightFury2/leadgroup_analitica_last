<?php

use yii\db\Migration;

class m170531_082315_add_tables_indexes_and_other_updates extends Migration
{
    public function up()
    {
        $this->createIndex('IK_order_history', 'order_history', ['user_type']);
        $this->createIndex('IK_offer', 'offer', ['country_id']);
        $this->createIndex('IK_operator', 'operator', ['country_id']);
        $this->createIndex('IK_converse_statistic', 'converse_statistic', ['country_id', 'offer_id']);
        $this->createIndex('IK_operator_query_statistic', 'operator_query_statistic', ['operator_id']);
        $this->createIndex('IK_operator_to_offer', 'operator_to_offer', ['operator_id']);
        $this->createIndex('IK_statuses_list', 'statuses_list', ['personal_id']);

        $this->addColumn('order_history', 'offer_id', $this->integer(6)->notNull());
        $this->addColumn('operator_statistic', 'offer_id', $this->integer(6)->notNull());

        $this->createIndex('IK_operator_statistic', 'operator_statistic', ['operator_id', 'country_id', 'offer_id']);

        $this->dropColumn('order_history', 'action_id');
    }

    public function down()
    {
        echo "m170531_082315_add_tables_indexes_and_other_updates cannot be reverted.\n";

        return false;
    }
}
