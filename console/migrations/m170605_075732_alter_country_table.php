<?php

use yii\db\Migration;

class m170605_075732_alter_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('country', 'login', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m170605_075732_alter_country_table cannot be reverted.\n";
    }
}
