<?php

use yii\db\Migration;

/**
 * Handles the creation of table `operator`.
 */
class m170406_114244_create_operator_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operator', [
            'id' => $this->primaryKey(),
            'login' => $this->string(100)->notNull(),
            'country_id' => $this->integer(11)->notNull(),
            'name' => $this->string(100)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operator');
    }
}
