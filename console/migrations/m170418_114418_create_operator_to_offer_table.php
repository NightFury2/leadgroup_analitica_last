<?php

use yii\db\Migration;

/**
 * Handles the creation of table `operator_to_offer`.
 */
class m170418_114418_create_operator_to_offer_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operator_to_offer', [
            'id' => $this->primaryKey(),
            'operator_id' => $this->integer(6)->notNull(),
            'offer_id' => $this->integer(6)->notNull(),
            'active_to' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operator_to_offer');
    }

}
