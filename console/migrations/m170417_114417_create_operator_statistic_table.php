<?php

use yii\db\Migration;

/**
 * Handles the creation of table `operator_statistic`.
 */
class m170417_114417_create_operator_statistic_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operator_statistic', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(6)->notNull(),
            'operator_id' => $this->integer(6)->notNull(),
            'date' => $this->date()->notNull(),
            'total' => $this->integer(6)->notNull()->defaultValue(0),
            'approved' => $this->integer(6)->notNull()->defaultValue(0),
            'buyout' => $this->integer(6)->notNull()->defaultValue(0),
            'average_bill' => $this->float(1)->notNull()->defaultValue(0),
            'average_bill_usd' => $this->float(1)->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operator_statistic');
    }

}
