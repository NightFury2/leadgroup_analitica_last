<?php

use yii\db\Migration;

/**
 * Handles the creation of table `webmaster_statistic`.
 */
class m170601_084055_create_webmaster_statistic_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('webmaster_statistic', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'country_id' => $this->integer(6)->notNull()->defaultValue(0),
            'wm_id' => $this->integer(6)->notNull()->defaultValue(0),
            'offer_id' => $this->integer(6)->notNull(),
            'approved' => $this->integer(6)->notNull()->defaultValue(0),
            'paid' => $this->integer(6)->notNull()->defaultValue(0),
            'canceled' => $this->integer(6)->notNull()->defaultValue(0),
            'error' => $this->integer(6)->notNull()->defaultValue(0),
            'total' => $this->integer(6)->notNull()->defaultValue(0),
        ]);

        $this->createIndex('IK_webmaster_statistic', 'webmaster_statistic', ['country_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('webmaster_statistic');
    }
}
