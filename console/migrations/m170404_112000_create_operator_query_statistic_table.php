<?php

use yii\db\Migration;

/**
 * Handles the creation of table `operator_query_statistic`.
 */
class m170404_112000_create_operator_query_statistic_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operator_query_statistic', [
            'id' => $this->primaryKey(3),
            'datetime' => $this->datetime()->notNull(),
            'operator_id' => $this->string(8)->notNull(),
            'count' => $this->string(8)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operator_query_statistic');
    }
}
