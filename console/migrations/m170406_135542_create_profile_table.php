<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m170406_135542_create_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('profile', [
            'id' => $this->primaryKey(11),
            'user_id' => $this->integer(5)->notNull(),
            'first_name' => $this->string(32)->notNull(),
            'last_name' => $this->string(32)->notNull(),
            'photo' => $this->integer(2)->defaultValue(0)
        ]);

        $this->batchInsert('profile', ['user_id', 'first_name', 'last_name', 'photo'], [
            [1, 'John', 'Doe', 1],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('profile');
    }
}
