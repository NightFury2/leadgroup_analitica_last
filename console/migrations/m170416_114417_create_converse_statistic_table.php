<?php

use yii\db\Migration;

/**
 * Handles the creation of table `converse_statistic`.
 */
class m170416_114417_create_converse_statistic_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('converse_statistic', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'country_id' => $this->integer(6)->notNull()->defaultValue(0),
            'offer_id' => $this->integer(6)->notNull(),
            'total' => $this->integer(6)->notNull()->defaultValue(0),
            'approved' => $this->integer(6)->notNull()->defaultValue(0),
            'canceled' => $this->integer(6)->notNull()->defaultValue(0),
            'error' => $this->integer(6)->notNull()->defaultValue(0),
            'return' => $this->integer(6)->notNull()->defaultValue(0),
            'paid' => $this->integer(6)->notNull()->defaultValue(0),
            'processed' => $this->integer(6)->notNull()->defaultValue(0),
            'average_bill' => $this->float(1)->notNull()->defaultValue(0),
            'average_bill_usd' => $this->float(1)->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('converse_statistic');
    }
}
