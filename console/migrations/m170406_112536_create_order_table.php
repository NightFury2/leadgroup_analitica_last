<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170406_112536_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(11),
            'country_id' => $this->integer(11)->defaultValue(0),
            'status_id' => $this->integer(3)->defaultValue(null),
            'created_at' => $this->dateTime()->defaultValue(null),
            'total' => $this->float(1)->defaultValue(0),
            'product_id' => $this->integer(8)->defaultValue(null),
            'offer_id' => $this->integer(8)->defaultValue(null),
            'postindex' => $this->string(32)->defaultValue(null),
            'courier_id' => $this->integer(11)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}
