<?php

use yii\db\Migration;

/**
 * Handles the creation of table `console_history`.
 */
class m170406_135543_create_console_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('console_history', [
            'id' => $this->primaryKey(11),
            'controller' => $this->string('128')->notNull(),
            'action' => $this->string('15')->notNull(),
            'total_time' => $this->float(10)->notNull(),
            'total_memory' => $this->float(10)->notNull(),
            'status' => $this->integer(1)->defaultValue(0),
            'datetime' => $this->timestamp(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('console_history');
    }
}
