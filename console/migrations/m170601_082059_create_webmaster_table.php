<?php

use yii\db\Migration;

/**
 * Handles the creation of table `webmaster`.
 */
class m170601_082059_create_webmaster_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('webmaster', [
            'id' => $this->primaryKey(),
            'personal_id' => $this->integer(6),
            'country_id' => $this->integer(6),
            'login' => $this->string(32),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('webmaster');
    }
}
