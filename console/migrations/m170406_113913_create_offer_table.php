<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offer`.
 */
class m170406_113913_create_offer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('offer', [
            'id' => $this->primaryKey(11),
            'country_id' => $this->integer(11)->notNull(),
            'title' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'active_to' => $this->date()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('offer');
    }
}
