<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170406_112521_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(3),
            'status' => $this->string(255)->notNull()
        ]);

        $this->batchInsert('status', ['id', 'status'], [
            [-2, 'Спам'],
            [-1, 'Отмена'],
            [-3, 'Ворзват'],
            [0, 'Обработка'],
            //[1, 'Одобрен'],
            [2, 'Оплачен'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
