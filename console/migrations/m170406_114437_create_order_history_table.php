<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_history`.
 */
class m170406_114437_create_order_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_history', [
            'id' => $this->primaryKey(11),
            'order_id' => $this->integer(11),
            'datetime' => $this->datetime(),
            'status_id_from' => $this->integer(11),
            'status_id_to' => $this->integer(11),
            'user_type' => $this->integer(3)->defaultValue(0),
            'user_id' => $this->integer(11),
            'action_id' => $this->integer(8)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_history');
    }
}
