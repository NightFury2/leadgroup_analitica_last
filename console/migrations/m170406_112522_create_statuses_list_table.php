<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_list`.
 */
class m170406_112522_create_statuses_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('statuses_list', [
            'id' => $this->primaryKey(11),
            'personal_id' => $this->integer(6)->notNull(),
            'group' => $this->integer(6)->notNull(),
            'offer_id' => $this->integer(6)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('statuses_list');
    }
}
