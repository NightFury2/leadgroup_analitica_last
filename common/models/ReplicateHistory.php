<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "replicate_history".
 *
 * @property integer $id
 * @property double $total_time
 * @property double $total_memory
 * @property integer $status
 * @property string $datetime
 */
class ReplicateHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'replicate_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total_time', 'total_memory'], 'required'],
            [['total_time', 'total_memory'], 'number'],
            [['status'], 'integer'],
            [['action'], 'string'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'total_time' => 'Total Time',
            'total_memory' => 'Total Memory',
            'status' => 'Status',
            'action' => 'Action',
            'datetime' => 'Datetime',
        ];
    }
}
