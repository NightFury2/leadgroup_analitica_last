<?php

namespace common\helpers;

/**
 * Class ControllerActions
 * Get actions from controller metadata
 *
 * @package common\helpers
 */
class ControllerActions
{

    /**
     * @param $namespace - Controller namespace
     * @return array - Actions
     * @throws \Exception
     */
    public static function get($namespace)
    {
        $actions = [];
        $methods = get_class_methods($namespace);

        if (is_array($methods)) {
            $className = explode('\\', $namespace);
            $controller = strtolower(str_replace('Controller', '', end($className)));

            foreach (get_class_methods($namespace) as $method) {
                if (preg_match('/action(\w..+)/', $method, $display)) {
                    $action = strtolower($controller . '/' .
                        preg_replace(
                            '/\\B([A-Z])/',
                            '-${1}',
                            substr($display[0], 6)
                        )
                    );

                    $actions[$action] = $action;
                }
            }
        } else {
            throw new \Exception("Could not find controller {$namespace}");
        }

        return [$controller => $actions];
    }

}
