#!/usr/bin/env bash
cd /code
php yii replicate/all --interactive=0
php yii calculate/all --interactive=0
php yii clean/all --interactive=0