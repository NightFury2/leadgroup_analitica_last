<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'db_remote' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=leadvertex2',
            'username' => 'root',
            'password' => 'toor',
            'charset' => 'utf8',
        ],
    ]
];
