#!/usr/bin/env bash
composer install
cd frontend/dev
npm i
npm run build-prod
cd ../../
php init --env=Production --overwrite=y
php yii migrate --interactive=0