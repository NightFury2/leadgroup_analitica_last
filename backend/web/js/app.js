$(window).load(function() {
    $(".navbar .sidebar-toggle").on("click", function () {
        toggleNavigation();
    });

    function toggleNavigation() {
        if ($("body").hasClass("sidebar-collapse")) {
            localStorage.setItem('sidebar-collapse', 'show');
        } else {
            localStorage.setItem('sidebar-collapse', 'collapse');
        }
    }

    if (localStorage.getItem('sidebar-collapse') && localStorage.getItem('sidebar-collapse') === 'collapse') {
        $("body").addClass("sidebar-collapse")
    }
});