<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "console_history".
 *
 * @property integer $id
 * @property string $controller
 * @property string $action
 * @property double $total_time
 * @property double $total_memory
 * @property integer $status
 * @property string $datetime
 *  * @property string $output
 */
class ConsoleHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'console_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller', 'action', 'total_time', 'total_memory'], 'required'],
            [['total_time', 'total_memory'], 'number'],
            [['status'], 'integer'],
            [['datetime'], 'safe'],
            [['output'], 'text'],
            [['controller'], 'string', 'max' => 128],
            [['action'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller' => 'Controller',
            'action' => 'Action',
            'total_time' => 'Total Time',
            'total_memory' => 'Total Memory',
            'status' => 'Status',
            'datetime' => 'Datetime',
            'output' => 'Output'
        ];
    }
}
