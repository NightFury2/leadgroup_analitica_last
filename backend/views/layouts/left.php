<aside class="main-sidebar">

    <section class="sidebar">
        <?php if (!Yii::$app->user->isGuest): ?>
            <?php
            $debugHeader = [];
            $debugItems = [];

            if (YII_ENV == 'dev') {
                $debugHeader = ['label' => 'Система', 'options' => ['class' => 'header']];

                $debugItems = [
                    'label' => 'Yii2 tools',
                    'icon' => 'cog',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    ],
                ];
            }
            ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => 'Главная', 'icon' => 'home', 'url' => ['/']],
                        ['label' => 'Консоль', 'icon' => 'copy', 'url' => ['/console']],
                        ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users']],
                        ['label' => 'Сессии', 'icon' => 'id-card-o', 'url' => ['/sessions']],
                        $debugHeader,
                        $debugItems
                    ],
                ]
            ) ?>
        <?php endif; ?>

    </section>

</aside>
