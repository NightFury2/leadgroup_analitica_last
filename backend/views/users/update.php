<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Обновление пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->username]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="user-update">

    <?= $this->render('_form', [
        'title' => $this->title,
        'model' => $model,
        'profileModel' => $profileModel,
    ]) ?>

</div>
