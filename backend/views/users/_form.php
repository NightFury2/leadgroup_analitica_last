<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($title) ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->textInput() ?>

        <?= $form->field($profileModel, 'first_name')->textInput() ?>

        <?= $form->field($profileModel, 'last_name')->textInput() ?>

        <?= $form->field($profileModel, 'photo')->textInput() ?>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>