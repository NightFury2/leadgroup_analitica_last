<?php

use yii\helpers\Html;

$this->title = 'Сессии';

?>

<div class="row">
    <div class="col-md-4">
        <div class="box">
            <?= Html::beginForm(['/sessions/clean-old'], 'post'); ?>
            <div class="box-header with-border">
                <h3 class="box-title">Истекшие сессии</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul>
                    <li><i class="fa fa-refresh" aria-hidden="true"></i> refresh tokens:
                        <b><?= $refreshTokensCount ?></b></li>
                    <li><i class="fa fa-id-badge" aria-hidden="true"></i> access tokens:
                        <b><?= $accessTokensCount ?></b></li>
                </ul>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= Html::submitButton('Очистить', ['class' => 'btn btn-success', 'disabled' => ($refreshTokensCount + $accessTokensCount == 0)]) ?>
            </div>
            <?= Html::endForm(); ?>
        </div>

        <div class="box">
            <?= Html::beginForm(['/sessions/clean-all'], 'post'); ?>
            <div class="box-header with-border">
                <h3 class="box-title">Активные сессии</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul>
                    <li><i class="fa fa-refresh" aria-hidden="true"></i> refresh tokens:
                        <b><?= $activeRefreshTokensCount ?></b></li>
                    <li><i class="fa fa-id-badge" aria-hidden="true"></i> access tokens:
                        <b><?= $activeAccessTokensCount ?></b></li>
                </ul>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= Html::submitButton('Очистить', ['class' => 'btn btn-danger',
                    'disabled' => ($activeRefreshTokensCount + $activeAccessTokensCount == 0),
                    'onClick' => 'return confirm(\'Вы уверены?\') ? true : false;']) ?>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>


    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Активные пользователи</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Логин</th>
                        <th style="width: 40px">Действие</th>
                    </tr>
                    <?php foreach ($activeSessions as $session): ?>
                        <tr>
                            <td><?= $session['id'] ?></td>
                            <td><?= $session['username'] ?></td>
                            <td>
                                <?= Html::beginForm(['/sessions/destroy']) ?>
                                <?= Html::hiddenInput('id', $session['id']) ?>
                                <?= Html::submitButton('X', ['class' => 'btn btn-danger', 'onClick' => 'return confirm(\'Вы уверены?\') ? true : false;']) ?>
                                <?= Html::endForm() ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
        </div>
    </div>

</div>
