<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\FileHelper;

/* @var $this yii\web\View */

$this->title = 'Профиль';
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Основная информация</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?= $form
                            ->field($userModel, 'username')
                            ->label('Логин')
                            ->textInput(['placeholder' => 'Логин']) ?>
                        <?= $form->field($userModel, 'email')->input('email')->label('E-Mail') ?>
                        <?= $form->field($userModel, 'password')->passwordInput()->label('Пароль') ?>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Профиль</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <?= $form
                            ->field($profileModel, 'first_name')
                            ->label('Имя')
                            ->textInput(['placeholder' => 'Имя']) ?>

                        <?= $form
                            ->field($profileModel, 'last_name')
                            ->label('Фамилия')
                            ->textInput(['placeholder' => 'Фамилия']) ?>

                        <?= $form->field($profileModel, 'photo')
                            ->label('Фото')
                            ->textInput(['placeholder' => 'Аватар (id)']) ?>
                        <?= $form->errorSummary($profileModel) ?>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <?= Html::submitButton('Сохранить (все)', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>