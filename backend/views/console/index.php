<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'Консоль';
?>

<div class="row">
    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Консоль</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?= Html::beginForm(['console/action'], 'post', ['enctype' => 'multipart/form-data']) ?>
            <div class="box-body">
                <div class="form-group">
                    <label>Action</label>
                    <?= Html::dropDownList('action', 'select', array_merge([
                        'select' => 'Please select action',
                    ], $consoleActions), [
                        'class' => 'form-control',
                        'options' => [
                            'select' => [
                                'disabled' => true
                            ]
                        ]
                    ]) ?>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-body">
                <?php
                if (Yii::$app->session->get('console_log')) {
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-success',
                        ],
                        'body' => 'Задание выполнено',
                    ]);

                    echo Html::textarea('console_log', Yii::$app->session->get('console_log'), [
                        'readonly' => true,
                        'class' => 'form-control',
                        'rows' => 10
                    ]);
                }
                ?>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?= Html::submitButton('Выполнить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>

    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">История</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Задача</th>
                        <th style="width: 40px">Статус</th>
                        <th>Время</th>
                        <th>Память</th>
                        <th>Дата</th>
                        <th>Лог</th>
                    </tr>
                    <?php foreach ($models as $model): ?>
                        <tr>
                            <td><?= $model->id ?></td>
                            <td><?= $model->action ?></td>
                            <td>
                                <?= $model->status == 0 ? '<span class="badge bg-green">Успешно</span>' : '<span class="badge bg-red">Ошибка</span>' ?>
                            </td>
                            <td><?= $model->total_time ?>s</td>
                            <td><?= $model->total_memory ?>mb</td>
                            <td><?= $model->datetime ?></td>
                            <td><?= Html::button('>', ['class' => 'btn btn-primary',
                                    'data-toggle' => 'modal',
                                    'data-target' => "#outputLog{$model->id}"
                                ]) ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                ]); ?>
                <?= Html::beginForm(['/console/clean']) ?>
                <?= Html::submitButton('Очистить', ['class' => 'btn btn-danger', 'disabled' => (count($models) == 0)]) ?>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>

</div>
<div>
    <?php foreach ($models as $model): ?>
        <div class="modal fade" id="outputLog<?= $model->id ?>">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="color:red;"><span class="glyphicon glyphicon-lock"></span> Лог</h4>
                    </div>
                    <div class="modal-body">
                        <?=
                        Html::textarea("outputLog", $model->output, [
                            'readonly' => true,
                            'class' => 'form-control',
                            'rows' => 25
                        ]);
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span
                                    class="glyphicon glyphicon-remove"></span> Закрыть
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>