<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use filsh\yii2\oauth2server\models\OauthAccessTokens;
use filsh\yii2\oauth2server\models\OauthRefreshTokens;

class SessionsController extends BackendController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'clean', 'destroy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'clean-all' => ['post'],
                    'clean-old' => ['post'],
                    'destroy' => ['post']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $accessTokensCount = OauthAccessTokens::find()->where(new \yii\db\Expression('expires < NOW()'))->count();
        $refreshTokensCount = OauthRefreshTokens::find()->where(new \yii\db\Expression('expires < NOW()'))->count();

        $activeAccessTokensCount = OauthAccessTokens::find()->where(new \yii\db\Expression('expires > NOW()'))->count();
        $activeRefreshTokensCount = OauthRefreshTokens::find()->where(new \yii\db\Expression('expires > NOW()'))->count();

        $activeSessions = Yii::$app->db->createCommand('
                SELECT id, username 
                FROM user 
                JOIN oauth_refresh_tokens ON user_id = id 
                LEFT JOIN oauth_access_tokens a ON a.user_id = id 
                GROUP BY username
            ')->queryAll();

        return $this->render('index', [
            'accessTokensCount' => $accessTokensCount,
            'refreshTokensCount' => $refreshTokensCount,
            'activeSessions' => $activeSessions,
            'activeAccessTokensCount' => $activeAccessTokensCount,
            'activeRefreshTokensCount' => $activeRefreshTokensCount
        ]);
    }

    public function actionDestroy()
    {
        $userId = Yii::$app->request->post('id');

        OauthAccessTokens::deleteAll(['user_id' => $userId]);
        OauthRefreshTokens::deleteAll(['user_id' => $userId]);

        Yii::$app->session->setFlash('success', 'Сессия удалена');

        return $this->redirect(['index']);
    }

    public function actionCleanOld()
    {
        OauthAccessTokens::deleteAll(new \yii\db\Expression('expires < NOW()'));
        OauthRefreshTokens::deleteAll(new \yii\db\Expression('expires < NOW()'));

        Yii::$app->session->setFlash('success', 'Сессии очищены');

        return $this->redirect(['index']);
    }

    public function actionCleanAll()
    {
        OauthAccessTokens::deleteAll();
        OauthRefreshTokens::deleteAll();

        Yii::$app->session->setFlash('success', 'Сессии очищены');

        return $this->redirect(['index']);
    }
}