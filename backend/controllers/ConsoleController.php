<?php
namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\ControllerActions;
use yii\data\Pagination;
use backend\models\ConsoleHistory;
use yii\helpers\ArrayHelper;
/**
 * Site controller
 */
class ConsoleController extends BackendController
{
    private $consoleActions = [];

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->consoleActions = array_merge(
                ControllerActions::get('console\controllers\ReplicateController'),
                ControllerActions::get('console\controllers\CalculateController'),
                ControllerActions::get('console\controllers\CleanController')
            );

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'action', 'clean'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'action' => ['post'],
                    'clean' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = ConsoleHistory::find()->orderBy(['id' => SORT_DESC]);
        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('index', [
            'consoleActions' => $this->consoleActions,
            'pages' => $pages,
            'models' => $models
        ]);
    }

    public function actionAction()
    {
        $action = Yii::$app->request->post('action');
        $list = explode('/', $action)[0];


        if (!isset($this->consoleActions[$list][$action])) {
            Yii::$app->session->setFlash('error', 'Invalid action');
        } else {
            $log = shell_exec('php ../../yii ' . $action);
            Yii::$app->session->setFlash('console_log', $log);
        }

        return $this->redirect(['index']);
    }


    public function actionClean()
    {
        ConsoleHistory::deleteAll();

        Yii::$app->session->setFlash('success', 'Лог очищен');

        return $this->redirect(['index']);
    }
}
