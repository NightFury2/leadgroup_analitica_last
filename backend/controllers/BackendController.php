<?php

namespace backend\controllers;

use yii\web\Controller;
use Yii;
use common\models\Profile;

class BackendController extends Controller
{
    public $profile = [];

    public function beforeAction($action)
    {
        $action = parent::beforeAction($action);

        if (!Yii::$app->user->isGuest) {
            $this->profile = Profile::findOne(['user_id' => Yii::$app->user->id]);
        }

        return $action;
    }
}