<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\User;

/**
 * Site controller
 */
class ProfileController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = User::findOne(['id' => Yii::$app->user->id]);
        $profile = $this->profile;

        $user->scenario = 'update';
        $profile->scenario = 'update';

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($user->validate() && $profile->validate()) {
                $user->save(false);
                $profile->save(false);

                Yii::$app->session->setFlash('success', 'Профиль сохранен');
            }
        }

        return $this->render('index', [
            'userModel' => $user,
            'profileModel' => $profile
        ]);
    }

}
