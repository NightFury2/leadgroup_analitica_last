<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <h1 class="pt-1"><?= Html::encode($this->title) ?></h1>
                <p class="text-muted"><?= nl2br(Html::encode($message)) ?></p>
            </div>
        </div>
    </div>
</div>