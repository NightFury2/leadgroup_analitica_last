<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "webmaster".
 *
 * @property integer $id
 * @property integer $personal_id
 * @property integer $country_id
 * @property string $login
 */
class Webmaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'webmaster';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personal_id', 'country_id'], 'integer'],
            [['login'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personal_id' => 'Personal ID',
            'country_id' => 'Country ID',
            'login' => 'Login',
        ];
    }
}
