<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "operator_query_statistic".
 *
 * @property integer $id
 * @property string $datetime
 * @property string $operator_id
 * @property string $count
 */
class OperatorQueryStatistic extends \yii\db\ActiveRecord
{
    public $date;
    public $time;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operator_query_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'operator_id', 'count'], 'required'],
            [['datetime'], 'safe'],
            [['operator_id', 'count'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Datetime',
            'operator_id' => 'Operator ID',
            'count' => 'Count',
        ];
    }
}
