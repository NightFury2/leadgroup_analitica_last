<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "webmaster_statistic".
 *
 * @property integer $id
 * @property integer $approved
 * @property integer $paid
 * @property integer $canceled
 * @property integer $error
 * @property integer $total
 */
class WebmasterStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'webmaster_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approved', 'paid', 'canceled', 'error', 'total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'approved' => 'Approved',
            'paid' => 'Paid',
            'canceled' => 'Canceled',
            'error' => 'Error',
            'total' => 'Total',
        ];
    }
}
