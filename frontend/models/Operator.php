<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "operator".
 *
 * @property integer $id
 * @property string $name
 * @property integer $country_id
 */
class Operator extends \yii\db\ActiveRecord
{
    public $offer_id;
    public $operator_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country_id' => 'Country ID',
        ];
    }
}
