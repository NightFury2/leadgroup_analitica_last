<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "offer".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $title
 * @property string $name
 * @property string $active
 */
class Offer extends \yii\db\ActiveRecord
{
    public $active;

    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param array $dirtyAttributes
     */
    public function setDirtyAttributes(array $dirtyAttributes)
    {
        $this->dirtyAttributes = $dirtyAttributes;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'title', 'name', 'active'], 'required'],
            [['country_id'], 'integer'],
            [['active_to'], 'date'],
            [['title', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'title' => 'Title',
            'name' => 'Name',
            'active' => 'Active',
        ];
    }


}
