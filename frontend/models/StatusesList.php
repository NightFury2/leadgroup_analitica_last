<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "statuses_list".
 *
 * @property integer $id
 * @property integer $personal_id
 * @property integer $group
 * @property integer $offer_id
 */
class StatusesList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personal_id', 'group', 'offer_id'], 'required'],
            [['personal_id', 'group', 'offer_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personal_id' => 'Personal ID',
            'group' => 'Group',
            'offer_id' => 'Offer ID',
        ];
    }
}
