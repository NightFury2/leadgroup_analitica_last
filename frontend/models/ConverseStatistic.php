<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "converse_statistic".
 *
 * @property integer $id
 * @property string $date
 * @property integer $total
 * @property integer $approved
 * @property integer $canceled
 * @property integer $error
 * @property integer $return
 * @property integer $paid
 * @property integer $processed
 * @property double $average_bill
 * @property integer $country_id
 * @property integer $offer_id
 * @property double $average_bill_usd
 */
class ConverseStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'converse_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'offer_id'], 'required'],
            [['date'], 'safe'],
            [['total', 'approved', 'canceled', 'error', 'return', 'paid', 'processed', 'country_id', 'offer_id'], 'integer'],
            [['average_bill', 'average_bill_usd'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'total' => 'Total',
            'approved' => 'Approved',
            'canceled' => 'Canceled',
            'error' => 'Error',
            'return' => 'Return',
            'paid' => 'Paid',
            'processed' => 'Processed',
            'average_bill' => 'Average Bill',
            'country_id' => 'Country ID',
            'offer_id' => 'Offer ID',
            'average_bill_usd' => 'Average Bill Usd',
        ];
    }
}
