<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "operator_statistic".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $operator_id
 * @property string $date
 * @property integer $total
 * @property integer $approved
 * @property integer $buyout
 * @property double $average_bill
 * @property double $average_bill_usd
 */
class OperatorStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operator_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'operator_id', 'date'], 'required'],
            [['country_id', 'operator_id', 'total', 'approved', 'buyout'], 'integer'],
            [['date'], 'safe'],
            [['average_bill', 'average_bill_usd'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'operator_id' => 'Operator ID',
            'date' => 'Date',
            'total' => 'Total',
            'approved' => 'Approved',
            'buyout' => 'Buyout',
            'average_bill' => 'Average Bill',
            'average_bill_usd' => 'Average Bill Usd',
        ];
    }
}
