<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $status_id
 * @property string $created_at
 * @property double $total
 * @property integer $product_id
 * @property integer $offer_id
 * @property string $postindex
 * @property integer $courier_id
 * @property integer $country_id
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'product_id', 'offer_id', 'courier_id', 'country_id'], 'integer'],
            [['created_at'], 'safe'],
            [['total'], 'number'],
            [['postindex'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'total' => 'Total',
            'product_id' => 'Product ID',
            'offer_id' => 'Offer ID',
            'postindex' => 'Postindex',
            'courier_id' => 'Courier ID',
            'country_id' => 'Country ID',
        ];
    }
}
