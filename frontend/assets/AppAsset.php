<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
    ];

    public function __construct(array $config = [])
    {
        if (YII_ENV == 'dev') {
            //$this->js[] = 'http://192.168.1.224:8081/dist/bundle.js';
            //$this->css[] = 'http://192.168.1.224:8081/dist/style.css';
            $this->js[] = '/dist/bundle.js';
            $this->css[] = '/dist/style.css';
        } else {
            $this->js[] = '/dist/bundle.js';
            $this->css[] = '/dist/style.css';
        }

        parent::__construct($config);
    }
}