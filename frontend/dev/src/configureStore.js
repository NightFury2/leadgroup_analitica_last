import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './reducers'
import rootSaga from './sagas/root'

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

let finalCreateStore;

if (__DEVELOPMENT__ && __CLIENT__ && __DEVTOOLS__) {
  const { persistState } = require('redux-devtools');
  const DevTools = require('./containers/DevTools/DevTools');
  finalCreateStore = compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
  )(createStore);
} else {
  finalCreateStore = applyMiddleware(...middleware)(createStore);
}

const store = finalCreateStore(rootReducer);

if (__DEVELOPMENT__ && module.hot) {
  module.hot.accept('./reducers', () => {
    store.replaceReducer(require('./reducers'));
  });
}

sagaMiddleware.run(rootSaga);

export default store