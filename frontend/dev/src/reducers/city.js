import {
  GET_CITY_STATS,
  REPLACE_CITY_STATS
} from '../actions/actionTypes'

export default function handleCity(
  state = {
    data: [],
    show: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_CITY_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        show: false
      })
    case REPLACE_CITY_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        show: true,
        data: action.data
      })
    default:
      return state
  }
}