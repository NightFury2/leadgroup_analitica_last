import {
  AUTH_FAIL,
  AUTH_SUCCESS,
  AUTH_LOGGING,
  CHANGE_PROFILE_INFO_REQUEST,
  CHANGE_PROFILE_INFO_FAIL,
  CHANGE_PROFILE_INFO_SUCCESS,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAIL,
  AUTH_LOGOUT,
  GET_PROFILE_AVATAR_LIST_REQUEST,
  GET_PROFILE_AVATAR_LIST_SUCCESS,
  GET_PROFILE_AVATAR_LIST_FAIL,
  CHANGE_PROFILE_AVATAR_REQUEST,
  CHANGE_PROFILE_AVATAR_SUCCESS,
  CHANGE_PROFILE_AVATAR_FAIL
} from '../actions/actionTypes'

export default function handleLogin(
  state = {
    loggedIn: null,
    accessToken: null,
    refreshToken: null,
    receivedAt: null,
    expiresIn: null,
    authFailed: false,
    isLoadingAuth: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case AUTH_LOGGING:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        isLoadingAuth: true,
        authFailed: false,
        loggedIn: false,
        errorMessage: '',
      })
    case AUTH_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        loggedIn: true,
        accessToken: action.accessToken,
        isLoadingAuth: false,
        refreshToken: action.refreshToken,
        receivedAt: action.receivedAt,
        expiresIn: action.expiresIn,
        authFailed: false,
        errorMessage: '',
        isLoading: false,
      })
    case AUTH_FAIL:
      return Object.assign({}, state, {
        ...state,
        loggedIn: false,
        isLoadingAuth: false,
        errorMessage: action.errorMessage,
        authFailed: true,
        isLoading: false,
      })
    case GET_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        email: action.email,
        password: action.password,
        firstName: action.firstName,
        lastName: action.lastName,
        photo: action.photo
      })
    case GET_PROFILE_FAIL:
      return Object.assign({}, state, {
        ...state,
        profileError: action.profileError
      })
    case CHANGE_PROFILE_INFO_REQUEST:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        changeInfoSuccess: false,
        changeInfoFail: false
      })
    case CHANGE_PROFILE_INFO_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        changeInfoSuccess: true,
        changeInfoFail: false,
        email: action.email,
        photo: action.photo,
        firstName: action.firstName,
        lastName: action.lastName
      })
    case CHANGE_PROFILE_INFO_FAIL:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        changeInfoSuccess: false,
        changeInfoFail: true,
        profileErrorMessage: action.errorMessage
      })
    case AUTH_LOGOUT:
      return Object.assign({}, state, {
        ...state,
        loggedIn: false,
        firstName: null,
        lastName: null,
        photo: null,
        authFailed: null,
        isLoading: false,
        errorMessage: '',
        accessToken: null,
        refreshToken: null,
        receivedAt: null,
        expiresIn: null,
      })
    case GET_PROFILE_AVATAR_LIST_REQUEST:
      return Object.assign({}, state, {
        ...state,
        avatarsLoading: true,
        avatarList: []
      })
    case GET_PROFILE_AVATAR_LIST_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        avatarList: action.avatars,
        avatarsLoading: false
      })
    case GET_PROFILE_AVATAR_LIST_FAIL:
      return Object.assign({}, state, {
        avatarList: [],
        avatarsLoading: false
      })
    default:
      return state
  }
}