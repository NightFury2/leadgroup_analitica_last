import {
  GET_COUNTRY_LIST_REQUEST,
  GET_COUNTRY_LIST_SUCCESS,
  GET_COUNTRY_LIST_FAIL,
  GET_OFFER_LIST_REQUEST,
  GET_OFFER_LIST_SUCCESS,
  GET_OFFER_LIST_FAIL,
  GET_OPERATORS_LIST_REQUEST,
  GET_OPERATORS_LIST_SUCCESS,
  GET_OPERATORS_LIST_FAIL,
  COUNTRY_CHOSEN_ID,
  OFFER_CHOSEN_LIST,
  CLEAR_OFFER_LIST,
  OPERATOR_CHOSEN_LIST,
  CLEAR_OPERATOR_LIST,
  GET_CONV_STATS_SUCCESS,
  CHOSEN_DATE_RANGE_CUSTOM,
  CHOSEN_DATE_RANGE_START,
  CHOSEN_DATE_RANGE_END,
  GET_CURRENCY_LIST_REQUEST,
  GET_CURRENCY_LIST_SUCCESS,
  GET_CURRENCY_LIST_FAIL
} from '../actions/actionTypes'

export default function handleFilters(
  state = {
    countryList: [],
    offerList: [],
    operatorsList: [],
    error: '',
    selectedCountries: [],
    selectedOffers: [],
    selectedOperators: [],
    dateFrom: '',
    dateTo: '',
    currencySymbols: [],
    chosenCountryId: 77
  }, action) {
  switch (action.type) {
    case CHOSEN_DATE_RANGE_START:
      return Object.assign({}, state, {
        ...state,
        dateFrom: action.from
      })
    case CHOSEN_DATE_RANGE_END:
      return Object.assign({}, state, {
        ...state,
        dateTo: action.to
      })
    case CHOSEN_DATE_RANGE_CUSTOM:
      return Object.assign({}, state, {
        ...state,
        dateFrom: action.from,
        dateTo: action.to
      })
    case GET_CONV_STATS_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        currentCurrency: state.chosenCountryCurrency
      })
    case COUNTRY_CHOSEN_ID:
      return Object.assign({}, state, {
        chosenCountryId: action.id,
        chosenCountryCurrency: action.currency,
        selectedOffers: [],
        selectedOperators: [],
        error: ''
      })
    case OFFER_CHOSEN_LIST:
      return Object.assign({}, state, {
        ...state,
        selectedOffers: action.val
      })
    case CLEAR_OFFER_LIST:
      return Object.assign({}, state, {
        ...state,
        selectedOffers: [],
        selectedOperators: [],
      })
    case GET_COUNTRY_LIST_REQUEST:
      return Object.assign({}, state, {
        ...state,
        countryIsLoading: true,
        error: ''
      })
    case GET_COUNTRY_LIST_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        countryIsLoading: false,
        countryList: action.data,
        error: ''
      })
    case GET_COUNTRY_LIST_FAIL:
      return Object.assign({}, state, {
        ...state,
        countryIsLoading: false,
        countryList: [],
        countryError: action.err
      })
    case GET_OFFER_LIST_REQUEST:
      return Object.assign({}, state, {
        ...state,
        offerIsLoading: true,
      })
    case GET_OFFER_LIST_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        offerIsLoading: false,
        offerList: action.data
      })
    case GET_OFFER_LIST_FAIL:
      return Object.assign({}, state, {
        ...state,
        offerIsLoading: false,
        offerList: [],
        offerError: action.err
      })
    case GET_OPERATORS_LIST_REQUEST:
      return Object.assign({}, state, {
        ...state,
        operatorsIsLoading: true,
      })
    case GET_OPERATORS_LIST_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        operatorsIsLoading: false,
        operatorsList: action.data
      })
    case GET_OPERATORS_LIST_FAIL:
      return Object.assign({}, state, {
        ...state,
        operatorsIsLoading: false,
        operatorsList: [],
        operatorsError: action.err
      })
    case OPERATOR_CHOSEN_LIST:
      return Object.assign({}, state, {
        ...state,
        selectedOperators: action.val
      })
    case CLEAR_OPERATOR_LIST:
      return Object.assign({}, state, {
        ...state,
        selectedOperators: []
      })
    case GET_CURRENCY_LIST_REQUEST:
      return Object.assign({}, state, {
        ...state,
        success: false,
        isLoading: true,
        error: '',
        currencySymbols: []
      })
    case GET_CURRENCY_LIST_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        success: true,
        isLoading: false,
        error: '',
        currencySymbols: action.data
      })
    case GET_CURRENCY_LIST_FAIL:
      return Object.assign({}, state, {
        ...state,
        success: false,
        isLoading: false,
        error: action.error,
        currencySymbols: []
      })
    default:
      return state
  }
}