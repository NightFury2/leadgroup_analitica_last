import {
  GET_CONV_STATS_REQUEST,
  GET_CONV_STATS_SUCCESS,
  GET_CONV_STATS_FAIL,
  COUNTRY_CHOSEN_ID
} from '../actions/actionTypes'


export default function handleConversion(
  state = {
    data: [],
    error: '',
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_CONV_STATS_REQUEST:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        error: '',
        data: []
      })
    case GET_CONV_STATS_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        offerData: action.offerData,
        basicData: action.basicData,
        error: ''
      })
    case GET_CONV_STATS_FAIL:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        error: action.error,
        data: []
      })
    default:
      return state
  }
}