import { combineReducers } from 'redux'
import handleConversion from './conversions'
import handleLogin from './login'
import handleOffer from './offer'
import handleOperators from './operators'
import handleProducts from './products'
import handleRedemption from './redemption'
import handleOrder from './order'
import handleLeadProfit from './leadprofit'
import handleCourier from './courier'
import handleCity from './city'
import handleActivity from './activity'
import handleFilters from './filters'
// polyfill
import 'babel-polyfill'

const rootReducer = combineReducers({
  handleLogin,
  handleConversion,
  handleOffer,
  handleOperators,
  handleProducts,
  handleRedemption,
  handleOrder,
  handleLeadProfit,
  handleCourier,
  handleCity,
  handleActivity,
  handleFilters
})

export default rootReducer
