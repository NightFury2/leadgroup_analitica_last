import {
  GET_OPERATORS_STATS_REQUEST,
  GET_OPERATORS_STATS_SUCCESS,
  GET_OPERATORS_STATS_FAIL
} from '../actions/actionTypes'

export default function handleOperators(
  state = {
    data: [],
    error: '',
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_OPERATORS_STATS_REQUEST:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        data: [],
        error: ''
      })
    case GET_OPERATORS_STATS_SUCCESS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        data: action.data,
        error: ''
      })
    case GET_OPERATORS_STATS_FAIL:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        data: [],
        error: action.error
      })
    default:
      return state
  }
}