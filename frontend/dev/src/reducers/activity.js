import {
  GET_ACTIVITY_STATS_REQUEST,
  GET_ACTIVITY_STATS_SUCCESS,
  GET_ACTIVITY_STATS_FAIL
} from '../actions/actionTypes'

export default function handleActivity(
  state = {
    data: [],
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_ACTIVITY_STATS_REQUEST:
      return Object.assign({}, state, {
        isLoading: true,
        success: false,
        data: []
      })
    case GET_ACTIVITY_STATS_SUCCESS:
      return Object.assign({}, state, {
        success: true,
        isLoading: false,
        data: action.data
      })
    case GET_ACTIVITY_STATS_FAIL:
      return Object.assign({}, state, {
        isLoading: false,
        data: [],
        error: action.error
      })
    default:
      return state
  }
}