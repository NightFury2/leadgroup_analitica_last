import {
  GET_OFFER_STATS,
  REPLACE_OFFER_STATS
} from '../actions/actionTypes'

export default function handleOffer(
  state = {
    data: [],
    show: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_OFFER_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        show: false
      })
    case REPLACE_OFFER_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        show: true,
        data: action.data
      })
    default:
      return state
  }
}