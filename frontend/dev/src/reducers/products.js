import {
  GET_PRODUCTS_STATS,
  REPLACE_PRODUCTS_STATS
} from '../actions/actionTypes'

export default function handleProducts(
  state = {
    data: [],
    show: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_PRODUCTS_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        show: false
      })
    case REPLACE_PRODUCTS_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        show: true,
        data: action.data
      })
    default:
      return state
  }
}