import {
  GET_ORDER_STATS,
  REPLACE_ORDER_STATS
} from '../actions/actionTypes'

export default function handleOrder(
  state = {
    data: [],
    show: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_ORDER_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        show: false
      })
    case REPLACE_ORDER_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        show: true,
        data: action.data
      })
    default:
      return state
  }
}