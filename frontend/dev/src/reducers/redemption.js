import {
  GET_REDEMPTION_STATS_REQUEST,
  GET_REDEMPTION_STATS_SUCCESS,
  GET_REDEMPTION_STATS_FAIL,
} from '../actions/actionTypes'

export default function handleRedemption(
  state = {
    data: [],
    isLoading: false,
    error: '',
    success: false,
  }, action) {
  switch (action.type) {
    case GET_REDEMPTION_STATS_REQUEST:
      return Object.assign({}, state, {
        isLoading: true,
        data: [],
        success: false,
        error: '',
      })
    case GET_REDEMPTION_STATS_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        data: action.data,
        success: true,
        error: '',
      })
    case GET_REDEMPTION_STATS_FAIL:
      return Object.assign({}, state, {
        isLoading: false,
        data: [],
        success: false,
        error: action.error,
      })
    default:
      return state
  }
}