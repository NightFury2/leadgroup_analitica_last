import {
  GET_LEADPROFIT_STATS,
  REPLACE_LEADPROFIT_STATS
} from '../actions/actionTypes'

export default function handleLeadProfit(
  state = {
    data: [],
    show: false,
    isLoading: false
  }, action) {
  switch (action.type) {
    case GET_LEADPROFIT_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: true,
        show: false
      })
    case REPLACE_LEADPROFIT_STATS:
      return Object.assign({}, state, {
        ...state,
        isLoading: false,
        show: true,
        data: action.data
      })
    default:
      return state
  }
}