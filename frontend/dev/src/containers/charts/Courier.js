import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import CourierChart from '../../components/charts/CourierChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import { connect } from 'react-redux'

class Courier extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <Panel>
        <FilterForm
          title="Статистика по курьерам">
          <Row>
            <Col xs="12" sm="12" md="6" lg="6">
              <DateRangePicker/>
            </Col>
            <Col xs="12" sm="12" md="6" lg="6">
              <CountryPicker/>
            </Col>
          </Row>
        </FilterForm>
        <CourierChart/>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(Courier)