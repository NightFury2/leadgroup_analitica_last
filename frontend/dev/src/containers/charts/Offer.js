import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import OfferChart from '../../components/charts/OfferChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import MultiCountryPicker from "../../components/filters/MultiCountryPicker";
import { connect } from 'react-redux'

class Offer extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <Panel>
        <FilterForm
          title="Статистика по офферам">
          <Row>
            <Col xs="12" sm="12" md="6" lg="6">
              <DateRangePicker/>
            </Col>
            <Col xs="12" sm="12" md="6" lg="6">
              <MultiCountryPicker/>
            </Col>
          </Row>
        </FilterForm>
        <OfferChart/>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(Offer)