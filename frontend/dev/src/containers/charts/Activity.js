import React, { PureComponent } from 'react'
import { Col } from 'reactstrap'
import ActivityChart from '../../components/charts/ActivityChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import OperatorPicker from '../../components/filters/OperatorPicker'
import OfferPicker from '../../components/filters/OfferPicker'
import { connect } from 'react-redux'
import { Row } from 'reactstrap'
import { AutoSizer } from 'react-virtualized'
import { activityStatsRequest } from '../../actions/activity'

class Activity extends PureComponent {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { token, startDate, endDate, requestActivity, selectedOperators } = this.props;
    requestActivity(token, 77, startDate, endDate, selectedOperators)
  }
  render() {
    const { isLoading, operators, data, success, shouldRerender, requestActivity, chosenCountryId, selectedOperators, startDate, endDate, token } = this.props;
    return (
      <div>
        <Panel style={{marginBottom: '0'}}>
          <FilterForm
            submit={() => requestActivity(token, chosenCountryId, startDate, endDate, selectedOperators)}
            title="Статистика активности"
          >
            <Row>
              <Col
                xs="12"
                sm="12"
                md="3"
                lg="3"
              >
                <DateRangePicker/>
              </Col>
              <Col
                xs="12"
                sm="12"
                md="3"
                lg="3"
              >
                <CountryPicker/>
              </Col>
              <Col
                xs="12"
                sm="12"
                md="3"
                lg="3"
              >
                <OfferPicker/>
              </Col>
              <Col
                xs="12"
                sm="12"
                md="3"
                lg="3"
              >
                <OperatorPicker/>
              </Col>
            </Row>
          </FilterForm>
          <ActivityChart
            data={data}
            success={success}
            startDate={startDate}
            endDate={endDate}
            token={token}
            isLoading={isLoading}
            shouldRerender={shouldRerender}
            operators={operators}
          />
        </Panel>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    data: state.handleActivity.data,
    startDate: state.handleFilters.dateFrom,
    endDate: state.handleFilters.dateTo,
    selectedOperators: state.handleFilters.selectedOperators,
    chosenCountryId: state.handleFilters.chosenCountryId,
    operators: state.handleFilters.operatorsList,
    success: state.handleActivity.success,
    isLoading: state.handleActivity.isLoading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestActivity: (token, country, startDate, endDate, selectedOperators) => dispatch(activityStatsRequest(token, country, startDate, endDate, selectedOperators))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Activity)