import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import Panel from '../../components/common/Panel'
import ProductsChart from '../../components/charts/ProductsChart'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import { connect } from 'react-redux'

class Products extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <Panel>
        <FilterForm
          title="Статистика по товарам"
        >
          <Row>
            <Col xs="12" sm="12" md="6" lg="6">
              <DateRangePicker/>
            </Col>
            <Col xs="12" sm="12" md="6" lg="6">
              <CountryPicker/>
            </Col>
          </Row>
        </FilterForm>
        <ProductsChart/>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(Products)