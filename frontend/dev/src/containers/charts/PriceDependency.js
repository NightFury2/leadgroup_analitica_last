import React, { Component } from 'react'
import PriceDependencyChart from '../../components/charts/PriceDependencyChart'
import FilterForm from '../../components/forms/FilterForm'
import { Row, Col } from 'reactstrap'
import DateRangePicker from '../../components/filters/DateRangePicker'
import Panel from '../../components/common/Panel'
import { connect } from 'react-redux'

class PriceDependency extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Panel>
        <FilterForm
          title="Статистика по зависимости выкупа от цены">
          <Row>
            <Col xs="0" sm="0" md="4" lg="4" />
            <Col xs="12" sm="12" md="4" lg="4">
              <DateRangePicker/>
            </Col>
            <Col xs="0" sm="0" md="4" lg="4" />
          </Row>
        </FilterForm>
        <PriceDependencyChart />
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(PriceDependency)