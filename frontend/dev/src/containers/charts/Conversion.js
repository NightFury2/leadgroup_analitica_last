import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import ConversionChart from '../../components/charts/ConversionChart'
import BasicConversionChart from '../../components/charts/BasicConversionChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import OfferPicker from '../../components/filters/OfferPicker'
import CountryPicker from '../../components/filters/CountryPicker'
import ChartNav from '../../components/common/ChartNav'
import { connect } from 'react-redux'
import { conversionStatsRequest } from '../../actions/conversions'
import { chosenCountry } from '../../actions/filters'
import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import ReactDOMServer from 'react-dom/server'

class Conversion extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { token, requestConversion, selectedOffers, chooseCountry } = this.props;
    chooseCountry(77, 'RUB');
    requestConversion(token, 77, selectedOffers);
  }
  render() {
    const { isLoading, token, requestConversion, chosenCountryId, selectedOffers, offerData, basicData, currentCurrency, currencySymbols, startDate, endDate, offerList } = this.props;
    // console.log(offerData, basicData, offerList);
    const toPercents = (current, total) => {
      if (current !== 0) {
        return `${(current / total * 100).toFixed(1)}%`
      } else {
        return `${current}%`
      }
    }
    const parseSum = (sum) => {
      if (!Number.isInteger(sum)) {
        return sum.toFixed(2)
      } else {
        return sum
      }
    }
    return (
      <Panel>
        <FilterForm
          submit={() => requestConversion(token, chosenCountryId, selectedOffers, startDate, endDate)}
          title="Статистика по конверсии"
        >
          <Row>
            <Col
              xs="12"
              sm="12"
              md="4"
              lg="4"
            >
              <DateRangePicker/>
            </Col>
            <Col
              xs="12"
              sm="12"
              md="4"
              lg="4"
            >
              <CountryPicker/>
            </Col>
            <Col
              xs="12"
              sm="12"
              md="4"
              lg="4"
            >
              <OfferPicker/>
            </Col>
          </Row>
        </FilterForm>
        <ChartNav
          offerStats={
            <ConversionChart
              data={offerData}
              offerList={offerList}
              symbols={currencySymbols}
              currentCurrency={currentCurrency}
              toPercents={toPercents}
              parseSum={parseSum}
              isLoading={isLoading}
            />
          }
          basicStats={
            <BasicConversionChart
              isLoading={isLoading}
              data={basicData}
              offerList={offerList}
              toPercents={toPercents}
              parseSum={parseSum}
              symbols={currencySymbols}
              currentCurrency={currentCurrency}
            />
          }
        />
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.handleConversion.isLoading,
    offerData: state.handleConversion.offerData,
    basicData: state.handleConversion.basicData,
    loggedIn: state.handleLogin.loggedIn,
    token: state.handleLogin.accessToken,
    chosenCountryId: state.handleFilters.chosenCountryId,
    selectedOffers: state.handleFilters.selectedOffers,
    offerList: state.handleFilters.offerList,
    chosenCountryCurrency: state.handleFilters.chosenCountryCurrency,
    currentCurrency: state.handleFilters.currentCurrency,
    currencySymbols: state.handleFilters.currencySymbols,
    startDate: state.handleFilters.dateFrom,
    endDate: state.handleFilters.dateTo
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestConversion: (token, country, offers, startDate, endDate) => dispatch(conversionStatsRequest(token, country, offers, startDate, endDate)),
    chooseCountry: (id, currency) => dispatch(chosenCountry(id, currency))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Conversion)