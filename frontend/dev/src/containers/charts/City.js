import React, { Component } from 'react'
import { Col, Row, Card } from 'reactstrap'
import CityChart from '../../components/charts/CityChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import CountryPicker from '../../components/filters/CountryPicker'
import { connect } from 'react-redux'

class City extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return(
      <Panel>
        <FilterForm
          title="Статистика по городам">
          <Row>
            <Col xs="0" sm="0" md="4" lg="4" />
            <Col xs="12" sm="12" md="4" lg="4">
              <CountryPicker/>
            </Col>
            <Col xs="0" sm="0" md="4" lg="4" />
          </Row>
        </FilterForm>
        <CityChart/>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(City)