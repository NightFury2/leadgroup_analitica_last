import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import RedemptionChart from '../../components/charts/RedemptionChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import { redemptionStatsRequest } from '../../actions/redemption'
import { connect } from 'react-redux'

class Redemption extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { token, country, startDate, endDate, requestRedemption } = this.props
    requestRedemption(token, country, startDate, endDate)
  }
  render() {
    const toPercents = (current, total) => {
      if (current != 0) {
        return `${(current / total * 100).toFixed(1)}%`
      } else {
        return `${current}%`
      }
    }
    const parseSum = (sum) => {
      if (!Number.isInteger(sum)) {
        return sum.toFixed(2)
      } else {
        return sum
      }
    }
    const {
      data,
      isLoading,
      token,
      country,
      startDate,
      endDate,
      requestRedemption
    } = this.props
    return(
      <Panel>
        <FilterForm
          title="Статистика по выкупаемости"
          submit={() => requestRedemption(token, country, startDate, endDate)}
        >
          <Row>
            <Col xs="12" sm="12" md="6" lg="6">
              <DateRangePicker/>
            </Col>
            <Col xs="12" sm="12" md="6" lg="6">
              <CountryPicker/>
            </Col>
          </Row>
        </FilterForm>
        <RedemptionChart
          data={data}
          isLoading={isLoading}
          toPercents={toPercents}
          parseSum={parseSum}
        />
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    country: state.handleFilters.chosenCountryId,
    startDate: state.handleFilters.dateFrom,
    endDate: state.handleFilters.dateTo,
    data: state.handleRedemption.data,
    isLoading: state.handleRedemption.isLoading,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestRedemption: (token, country, startDate, endDate) => dispatch(redemptionStatsRequest(token, country, startDate, endDate))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Redemption)