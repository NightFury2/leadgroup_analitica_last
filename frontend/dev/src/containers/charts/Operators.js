import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import OperatorsChart from '../../components/charts/OperatorsChart'
import Panel from '../../components/common/Panel'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import OfferPicker from '../../components/filters/OfferPicker'
import OperatorPicker from '../../components/filters/OperatorPicker'
import { connect } from 'react-redux'
import { operatorsStatsRequest } from '../../actions/operators'

class Operators extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { token, requestOperators, operatorsList } = this.props
    requestOperators(token, 77, null, null, null, operatorsList)
  }
  render() {
    const { token, data, isLoading, error, operatorsList, requestOperators, chosenCountryId, startDate, endDate, selectedOperators } = this.props
    const parseSum = (sum) => {
      if (!Number.isInteger(sum)) {
        return sum.toFixed(1)
      } else {
        return sum
      }
    }
    const toPercents = (current, total) => {
      if (current != 0) {
        return `${(current / total * 100).toFixed(1)}%`
      } else {
        return `${current}%`
      }
    }
    return(
      <Panel>
        <FilterForm
          submit={ () => requestOperators(token, chosenCountryId, selectedOperators, startDate, endDate, operatorsList) }
          title="Статистика по операторам"
        >
          <Row>
            <Col
              xs="12"
              sm="12"
              md="3"
              lg="3"
            >
              <DateRangePicker/>
            </Col>
            <Col
              xs="12"
              sm="12"
              md="3"
              lg="3"
            >
              <CountryPicker/>
            </Col>
            <Col
              xs="12"
              sm="12"
              md="3"
              lg="3"
            >
              <OfferPicker/>
            </Col>
            <Col
              xs="12"
              sm="12"
              md="3"
              lg="3"
            >
              <OperatorPicker/>
            </Col>
          </Row>
        </FilterForm>
        <OperatorsChart
          data={data}
          isLoading={isLoading}
          error={error}
          operatorsList={operatorsList}
          parseSum={parseSum}
          toPercents={toPercents}
        />
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    startDate: state.handleFilters.dateFrom,
    endDate: state.handleFilters.dateTo,
    operatorsList: state.handleFilters.operatorsList,
    chosenCountryId: state.handleFilters.chosenCountryId,
    isLoading: state.handleOperators.isLoading,
    error: state.handleOperators.error,
    data: state.handleOperators.data,
    selectedOperators: state.handleFilters.selectedOperators,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestOperators: (token, countryId, operators, startDate, endDate, operatorsList) => dispatch(operatorsStatsRequest(token, countryId, operators, startDate, endDate, operatorsList))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Operators)