import React, { Component } from 'react'
import { Progress } from 'reactstrap'
import { Col, Row } from 'reactstrap'
import Panel from '../../components/common/Panel'
import OrderChart from '../../components/charts/OrderChart'
import FilterForm from '../../components/forms/FilterForm'
import DateRangePicker from '../../components/filters/DateRangePicker'
import CountryPicker from '../../components/filters/CountryPicker'
import OfferPicker from '../../components/filters/OfferPicker'
import { connect } from 'react-redux'

class Order extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Panel>
        <FilterForm
          title="Статистика по заказам"
        >
          <Row>
            <Col xs="12" sm="12" md="4" lg="4">
              <DateRangePicker/>
            </Col>
            <Col xs="12" sm="12" md="4" lg="4">
              <CountryPicker/>
            </Col>
            <Col xs="12" sm="12" md="4" lg="4">
              <OfferPicker/>
            </Col>
          </Row>
        </FilterForm>
        <OrderChart>
          <tr>
            <td>Новых заказов</td>
            <td>100%</td>
            <td>
              <Progress value="100" color='success' />
            </td>
            <td>5ч</td>
            <td>30000</td>
            <td>500000$</td>
          </tr>
          <tr>
            <td>Подтверждено</td>
            <td>75%</td>
            <td>
              <Progress value="75" color='success' />
            </td>
            <td>12ч</td>
            <td>22000</td>
            <td>375000$</td>
          </tr>
          <tr>
            <td>Выгружено</td>
            <td>70%</td>
            <td>
              <Progress value="70" color='success' />
            </td>
            <td>10ч</td>
            <td>20500</td>
            <td>350000$</td>
          </tr>
          <tr>
            <td>Доставляется</td>
            <td>12%</td>
            <td>
              <Progress value="12" color='success' />
            </td>
            <td>36ч</td>
            <td>4000</td>
            <td>60000$</td>
          </tr>
          <tr>
            <td>Выполнено</td>
            <td>58%</td>
            <td>
              <Progress value="58" color='success' />
            </td>
            <td>25ч</td>
            <td>16500</td>
            <td>290000$</td>
          </tr>
          <tr>
            <td>Из них выкуплено</td>
            <td>60%</td>
            <td>
              <Progress value="60" color='success' />
            </td>
            <td>27ч</td>
            <td>10000</td>
            <td>175000$</td>
          </tr>
          <tr>
            <td>Из них отменено</td>
            <td>40%</td>
            <td>
              <Progress value="40" color='success' />
            </td>
            <td>23ч</td>
            <td>6500</td>
            <td>115000$</td>
          </tr>
        </OrderChart>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(Order)