import React, { Component } from 'react'
import { Col, Row } from 'reactstrap'
import Panel from '../components/common/Panel'
import AnalyzerColumn from '../components/analyzer/AnalyzerColumn'
import AnalyzerBody from '../components/analyzer/AnalyzerBody'
import Select from 'react-select'
import { connect } from 'react-redux'

class Analyzer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryValue: null,
      courierValue: null,
      periodValue: null,
      taxValue: null,
      articleOneValue: null,
      articleTwoValue: null,
      articleThreeValue: null,
      articleFourValue: null,
      articleFiveValue: null,
      articleSixValue: null,
    }
    this.handleCountryValue = this.handleCountryValue.bind(this)
    this.handleCourierValue = this.handleCourierValue.bind(this)
    this.handlePeriodValue = this.handlePeriodValue.bind(this)
    this.handleTaxValue = this.handleTaxValue.bind(this)
    this.handleArticleOneValue = this.handleArticleOneValue.bind(this)
    this.handleArticleTwoValue = this.handleArticleTwoValue.bind(this)
    this.handleArticleThreeValue = this.handleArticleThreeValue.bind(this)
    this.handleArticleFourValue = this.handleArticleFourValue.bind(this)
    this.handleArticleFiveValue = this.handleArticleFiveValue.bind(this)
    this.handleArticleSixValue = this.handleArticleSixValue.bind(this)
  }
  handleCountryValue(val) {
    this.setState({
      countryValue: val
    })
  }
  handleCourierValue(val) {
    this.setState({
      courierValue: val
    })
  }
  handlePeriodValue(val) {
    this.setState({
      periodValue: val
    })
  }
  handleTaxValue(val) {
    this.setState({
      taxValue: val
    })
  }
  handleArticleOneValue(val) {
    this.setState({
      articleOneValue: val
    })
  }
  handleArticleTwoValue(val) {
    this.setState({
      articleTwoValue: val
    })
  }
  handleArticleThreeValue(val) {
    this.setState({
      articleThreeValue: val
    })
  }
  handleArticleFourValue(val) {
    this.setState({
      articleFourValue:val
    })
  }
  handleArticleFiveValue(val) {
    this.setState({
      articleFiveValue: val
    })
  }
  handleArticleSixValue(val) {
    this.setState({
      articleSixValue: val
    })
  }
  render() {
    const countryData = [
      { value: 'TN', label: 'TN' },
      { value: 'RU', label: 'RU' },
      { value: 'UK', label: 'UK' }
    ]
    const courierData = [
      {value: 'D-post', label: 'D-post'},
      {value: 'EMS', label: 'EMS'},
      {value: 'OSM', label: 'OSM'}
    ]
    const periodData = [
      {value: 'День', label: 'День'},
      {value: 'Неделя', label: 'Неделя'},
      {value: 'Месяц', label: 'Месяц'},
      {value: 'Год', label: 'Год'}
    ]
    const articleData = [
      {value: 'coffee', label: 'coffee'},
      {value: 'sugar', label: 'sugar'},
      {value: 'icecream', label: 'icecream'},
      {value: 'juice', label: 'juice'}
    ]
    const taxData = [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' }
    ]
    const analyzerLeftColumn = (
      <AnalyzerColumn
        articleData={articleData}
        periodData={periodData}
        taxData={taxData}
        courierData={courierData}
        countryData={countryData}
        countryValue={this.state.countryValue}
        courierValue={this.state.courierValue}
        periodValue={this.state.periodValue}
        taxValue={this.state.taxValue}
        articleOneValue={this.state.articleOneValue}
        articleTwoValue={this.state.articleTwoValue}
        articleThreeValue={this.state.articleThreeValue}
        articleFourValue={this.state.articleFourValue}
        articleFiveValue={this.state.articleFiveValue}
        articleSixValue={this.state.articleSixValue}
        handleCountryValue={this.handleCountryValue}
        handleCourierValue={this.handleCourierValue}
        handlePeriodValue={this.handlePeriodValue}
        handleTaxValue={this.handleTaxValue}
        handleArticleOneValue={this.handleArticleOneValue}
        handleArticleTwoValue={this.handleArticleTwoValue}
        handleArticleThreeValue={this.handleArticleThreeValue}
        handleArticleFourValue={this.handleArticleFourValue}
        handleArticleFiveValue={this.handleArticleFiveValue}
        handleArticleSixValue={this.handleArticleSixValue}
        clearable={false}
        backspaceRemoves={false}
      />
    )
    const analyzerBody = (
      <AnalyzerBody />
    )

    return (
      <Panel>
        <Row>
          {analyzerLeftColumn}
          {analyzerBody}
        </Row>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

export default connect(mapStateToProps)(Analyzer)