import React, { Component } from 'react';
import RouterLink from '../../components/common/RouterLink'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import classnames from 'classnames'

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this)
    this.state = {
      activeTab: null
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    const { loggedIn } = this.props
    const { activeTab } = this.state
    return (
      loggedIn
        ?
        <div className="sidebar">
          <nav className="sidebar-nav">
            <ul className="nav">
              <RouterLink
                icon="bar-chart fa-lg"
                onClick={() => { this.toggle('1'); }}
                className={classnames({ active: activeTab === '1' })}
                to="/conversion"
              >
                Конверсия
              </RouterLink>
              <RouterLink
                icon="industry fa-lg"
                onClick={() => { this.toggle('3'); }}
                className={classnames({ active: activeTab === '3' })}
                to="/activity"
              >
                Активность
              </RouterLink>
              <RouterLink
                icon="shekel fa-lg"
                onClick={() => { this.toggle('4'); }}
                className={classnames({ active: activeTab === '4' })}
                to="/redemption"
              >
                Выкупаемость
              </RouterLink>
              <RouterLink
                icon="product-hunt fa-lg"
                onClick={() => { this.toggle('5'); }}
                className={classnames({ active: activeTab === '5' })}
                to="/products"
              >
                Товары
              </RouterLink>
              <RouterLink
                icon="braille fa-lg"
                onClick={() => { this.toggle('6'); }}
                className={classnames({ active: activeTab === '6' })}
                to="/city"
              >
                Города
              </RouterLink>
              <RouterLink
                icon="taxi fa-lg"
                onClick={() => { this.toggle('7'); }}
                className={classnames({ active: activeTab === '7' })}
                to="/courier"
              >
                Курьеры
              </RouterLink>
              <RouterLink
                icon="level-up fa-lg fa-2x"
                onClick={() => { this.toggle('8'); }}
                className={classnames({ active: activeTab === '8' })}
                to="/offers"
              >
                Показатели
              </RouterLink>
              <RouterLink
                icon="group fa-lg"
                onClick={() => { this.toggle('9'); }}
                className={classnames({ active: activeTab === '9' })}
                to="/operators"
              >
                Операторы
              </RouterLink>
              <RouterLink
                icon="exchange fa-lg"
                onClick={() => { this.toggle('10'); }}
                className={classnames({ active: activeTab === '10' })}
                to="/leadprofit"
              >
                Лиды
              </RouterLink>
              <RouterLink
                icon="area-chart fa-lg"
                onClick={() => { this.toggle('11'); }}
                className={classnames({ active: activeTab === '11' })}
                to="/order"
              >
                Заказы
              </RouterLink>
              <RouterLink
                icon="calculator fa-lg"
                onClick={() => { this.toggle('12'); }}
                className={classnames({ active: activeTab === '12' })}
                to="/analyzer"
              >
                Анализатор
              </RouterLink>
              <RouterLink
                icon="line-chart fa-lg"
                onClick={() => { this.toggle('13'); }}
                className={classnames({ active: activeTab === '13' })}
                to="/price-dependency"
              >
                Зависимость цены
              </RouterLink>
            </ul>
          </nav>
        </div>
        : null
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    firstName: state.handleLogin.firstName,
    lastName: state.handleLogin.lastName,
    photo: state.handleLogin.photo
  }
}

export default connect(mapStateToProps)(Sidebar)
