import React, { PureComponent } from 'react'
import { Container, Jumbotron } from 'reactstrap'
import { connect } from 'react-redux'
import { authLogout } from '../../actions/user'

class Logout extends PureComponent {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { logout, accessToken } = this.props
    logout(accessToken)
  }
  render() {
    return (
      <Container>
        <Jumbotron>
          <h1>Выход...</h1>
        </Jumbotron>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.handleLogin.isLoading,
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: (token) => {
      localStorage.removeItem('refreshToken')
      setTimeout(() => {
        dispatch(authLogout(token))
      }, 300)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)