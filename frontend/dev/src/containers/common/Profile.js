import React, { Component } from 'react'
import { Row, Col, Container, Button, Card, CardText, CardImg, CardBlock, CardHeader } from 'reactstrap'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import noImage from '../../styles/img/no-profile-pic-but-trust-me-i-m-cute.png'
import { changeProfileInfoRequest, getAvatarListRequest, changeProfileAvatarRequest } from '../../actions/user'
import EditProfileForm from "../../components/forms/EditProfileForm"
import EditAvatarForm from "../../components/forms/EditAvatarForm"
let self
class Profile extends Component {
  constructor(props) {
    super(props)
    this.handleForm = this.handleForm.bind(this)
    this.handleAvatar = this.handleAvatar.bind(this)
    this.state = {
      editForm: false,
      avatarForm: false
    }
    self = this
  }
  handleForm(e) {
    e.preventDefault()
    this.setState({
      editForm: !this.state.editForm
    })
  }
  handleAvatar(e) {
    e.preventDefault()
    this.setState({
      avatarForm: !this.state.avatarForm
    })
  }
  render() {
    const { photo, firstName, lastName, password, email, accessToken, getAvatars, changeAvatar, avatarList, changeInfo } = this.props
    const { avatarForm, editForm } = this.state
    const editProfileForm = (
      <EditProfileForm
        firstName={firstName}
        lastName={lastName}
        email={email}
        photo={photo}
        password={password}
        accessToken={accessToken}
        changeInfo={changeInfo}
      />
    )
    const editAvatarForm = (
      <EditAvatarForm
        avatarList={avatarList}
        firstName={firstName}
        lastName={lastName}
        email={email}
        photo={photo}
        password={password}
        accessToken={accessToken}
        changeAvatar={changeAvatar}
      />
    )
    return (
      <Container>
        <Row>
          <Col xs="12" sm="6" md="6" lg="6">
            <Card>
              <CardHeader tag="h3">Аватар</CardHeader>
              <CardBlock className="text-center">
                <CardImg src={typeof photo==='number' ? `/images/${photo}.png` : noImage} />
              </CardBlock>
            </Card>
          </Col>
          <Col xs="12" sm="6" md="6" lg="6">
            <Card>
              <CardHeader tag="h3">
                Информация
              </CardHeader>
              <CardBlock>
                <CardText>
                  Имя: {firstName ? firstName : 'Не указано'}
                </CardText>
                <CardText>
                  Фамилия: {lastName ? lastName : 'Не указано'}
                </CardText>
                <hr />
                <CardText>
                  <Button
                    color="info"
                    outline
                    block
                    onClick={this.handleForm}
                  >
                    Редактировать данные
                  </Button>
                </CardText>
                {
                  editForm ?
                  editProfileForm
                  : null
                }
                <hr/>
                <CardText>
                  <Button
                    color="info"
                    outline
                    block
                    onClick={() => getAvatars(accessToken)}
                  >
                    Изменить аватар
                  </Button>
                </CardText>
                {
                  avatarForm ?
                  editAvatarForm
                  : null
                }
                <hr />
                <CardText>
                  <Link
                    to="/logout"
                    className="logout-btn"
                  >
                    <Button
                      color="alert"
                      block
                    >
                      Выйти
                    </Button>
                  </Link>
                </CardText>
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    password: state.handleLogin.password,
    email: state.handleLogin.email,
    photo: state.handleLogin.photo,
    firstName: state.handleLogin.firstName,
    lastName: state.handleLogin.lastName,
    isLoading: state.handleLogin.isLoading,
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage,
    avatarList: state.handleLogin.avatarList
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getAvatars: (token) => {
      self.setState({
        avatarForm: !self.state.avatarForm
      })
      if (!self.state.avatarForm) {
        dispatch(getAvatarListRequest(token))
      }
    },
    changeAvatar: (email, password, firstName, lastName, photo, token) => {
      dispatch(changeProfileAvatarRequest(email, password, firstName, lastName, photo, token))
    },
    changeInfo: (email, password, firstName, lastName, photo, token) => {
      dispatch(changeProfileInfoRequest(email, password, firstName, lastName, photo, token))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)