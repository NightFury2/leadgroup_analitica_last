import React, { Component } from 'react'
import { authRequest, authLogout } from '../../actions/user'
import LoginForm from "../../components/forms/LoginForm"
import { connect } from 'react-redux'
import { Col, Alert } from 'reactstrap'

class Mainpage extends Component {
  constructor(props) {
    super(props)
    this.authWithLoginPass = this.authWithLoginPass.bind(this)
    this.logout = this.logout.bind(this)
  }
  authWithLoginPass(username, password) {
    const { auth } = this.props
    auth(username, password)
  }
  logout() {
    const { logout, accessToken } = this.props
    logout(accessToken)
  }
  render() {
    const { isLoading, authFailed, errorMessage } = this.props
    return(
      <LoginForm
        submit={this.authWithLoginPass}
        title={'Вход'}
        isLoading={isLoading}
        fail={authFailed}>
          {
            authFailed ?
              <Col>
                <Alert color="danger">
                  {errorMessage}
                </Alert>
              </Col>
            : null
          }
      </LoginForm>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.handleLogin.isLoading,
    loggedIn: state.handleLogin.loggedIn,
    authFailed: state.handleLogin.authFailed,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: (token) => {
      dispatch(authLogout(token))
    },
    auth: (username, password) => {
      dispatch(authRequest(username, password))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mainpage)