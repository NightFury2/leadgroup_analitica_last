import React, {Component} from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom'
import { connect } from 'react-redux'
import { Col } from 'reactstrap'
import { needUpdateToken } from '../actions/user'

// common

import PrivateRoute from '../components/common/PrivateRoute'
import NotFound from '../components/common/NotFound'
import NoAccess from '../components/common/NoAccess'

// containers


import Products from './charts/Products'
import City from './charts/City'
import Conversion from './charts/Conversion'
import Courier from './charts/Courier'
import LeadProfit from './charts/LeadProfit'
import Activity from './charts/Activity'
import ActivityChart from '../components/charts/ActivityChart'
import Operators from './charts/Operators'
import Offer from './charts/Offer'
import Redemption from './charts/Redemption'
import OperatorEfficiency from './charts/OperatorEfficiency'
import Order from './charts/Order'
import PriceDependency from './charts/PriceDependency'
import Profile from './common/Profile'
import Analyzer from './Analyzer'
import Mainpage from './common/Mainpage'
import Logout from './common/Logout'
import Dashboard from '../components/common/Dashboard'
import Full from '../components/common/Full'

// styles

import 'react-datepicker/dist/react-datepicker.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'fixed-data-table-2/dist/fixed-data-table.min.css'
import '../../src/styles/css/react-select.css'
import '../../src/styles/css/simple-line-icons.css'
import '../../src/styles/css/style.css'
import 'react-virtualized/styles.css'
import '../../src/styles/css/spritesheet.css'
import 'react-loading-spinner/src/css/index.css'
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import 'react-table/react-table.css'

class Root extends Component {
  constructor(props) {
    super(props)
    this.handleOffset = this.handleOffset.bind(this)
    this.state = {
      needOffset: false
    }
  }
  handleOffset() {
    this.setState({
      needOffset: !this.state.needOffset
    })
  }
  componentWillMount() {
    document.body.classList.toggle('sidebar-compact');
    const { tryToRefreshSession } = this.props;
    tryToRefreshSession();
    if (__DEVELOPMENT__) {
      console.log(`loading`)
    }
  }
  render() {
    const { loggedIn, isLoadingAuth } = this.props;
    return (
      <Router>
        <Full>
            <Col xs="12" sm="12" md="12" lg="12">
              <Switch>
                <Route exact path="/" component={loggedIn && !isLoadingAuth ? Dashboard : Mainpage} />
                <PrivateRoute loggedIn={loggedIn} path="/conversion" component={Conversion}/>
                <PrivateRoute loggedIn={loggedIn} path="/operator-efficiency" component={OperatorEfficiency}/>
                <PrivateRoute loggedIn={loggedIn} path="/activity" component={Activity} />
                <PrivateRoute loggedIn={loggedIn} path="/redemption" component={Redemption} />
                <PrivateRoute loggedIn={loggedIn} path="/products" component={Products} />
                <PrivateRoute loggedIn={loggedIn} path="/city" component={City} />
                <PrivateRoute loggedIn={loggedIn} path="/courier" component={Courier} />
                <PrivateRoute loggedIn={loggedIn} path="/offers" component={Offer} />
                <PrivateRoute loggedIn={loggedIn} path="/operators" component={Operators} />
                <PrivateRoute loggedIn={loggedIn} path="/leadprofit" component={LeadProfit} />
                <PrivateRoute loggedIn={loggedIn} path="/order" component={Order} />
                <PrivateRoute loggedIn={loggedIn} path="/analyzer" component={Analyzer} />
                <PrivateRoute loggedIn={loggedIn} path="/home" component={Dashboard} />
                <PrivateRoute loggedIn={loggedIn} path="/profile" component={Profile} />
                <PrivateRoute loggedIn={loggedIn} path="/price-dependency" component={PriceDependency} />
                <PrivateRoute loggedIn={loggedIn} path="/logout" component={Logout} />
                <Route path="/404" component={NotFound}/>
                <Route path="/401" component={NoAccess} />
                <Redirect from="*" to="/404"/>
              </Switch>
            </Col>
        </Full>
      </Router>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    isLoadingAuth: state.handleLogin.isLoadingAuth,
    accessToken: state.handleLogin.accessToken,
    refreshToken: state.handleLogin.refreshToken,
    errorMessage: state.handleLogin.errorMessage
  }
}
function mapDispatchToProps(dispatch) {
  return {
    tryToRefreshSession: () => {
      const refreshToken = localStorage.getItem('refreshToken');
      if (refreshToken) {
        dispatch(needUpdateToken())
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Root)