import 'babel-polyfill';
import React from 'react'
import ReactDOM from 'react-dom'
import Root from './containers/Root'
import store from './configureStore'
import { Provider } from 'react-redux'

const dest = document.getElementById('root');

ReactDOM.render(
  <Provider store={store} key="provider">
    <Root />
  </Provider>,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger

  if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
    console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
  }
}

if (__DEVTOOLS__ && !window.devToolsExtension) {
  const DevTools = require('./containers/DevTools/DevTools');
  ReactDOM.render(
    <Provider store={store} key="provider">
      <div>
        <Root />
        <DevTools/>
      </div>
    </Provider>,
    dest
  );
}