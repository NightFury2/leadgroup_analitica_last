import {
  GET_COUNTRY_LIST_REQUEST,
  GET_COUNTRY_LIST_SUCCESS,
  GET_COUNTRY_LIST_FAIL,
  GET_OFFER_LIST_REQUEST,
  GET_OFFER_LIST_SUCCESS,
  GET_OFFER_LIST_FAIL,
  GET_OPERATORS_LIST_REQUEST,
  GET_OPERATORS_LIST_SUCCESS,
  GET_OPERATORS_LIST_FAIL,
  COUNTRY_CHOSEN_ID,
  OFFER_CHOSEN_LIST,
  CLEAR_OFFER_LIST,
  CHOSEN_DATE_RANGE_START,
  CHOSEN_DATE_RANGE_END,
  CHOSEN_DATE_RANGE_CUSTOM,
  OPERATOR_CHOSEN_LIST,
  CLEAR_OPERATOR_LIST,
  GET_CURRENCY_LIST_REQUEST,
  GET_CURRENCY_LIST_SUCCESS,
  GET_CURRENCY_LIST_FAIL
} from './actionTypes'

export function dateRangeStart(from) {
  return {
    type: CHOSEN_DATE_RANGE_START,
    from
  }
}

export function dateRangeEnd(to) {
  return {
    type: CHOSEN_DATE_RANGE_END,
    to
  }
}

export function dateRangeCustom(from, to) {
  return {
    type: CHOSEN_DATE_RANGE_CUSTOM,
    from,
    to
  }
}

export function chosenCountry(id, currency) {
  return {
    type: COUNTRY_CHOSEN_ID,
    id,
    currency
  }
}

export function chosenOfferList(val) {
  return {
    type: OFFER_CHOSEN_LIST,
    val
  }
}

export function chosenOperatorList(val) {
  return {
    type: OPERATOR_CHOSEN_LIST,
    val
  }
}

export function clearOfferList() {
  return {
    type: CLEAR_OFFER_LIST
  }
}

export function clearOperatorList() {
  return {
    type: CLEAR_OPERATOR_LIST
  }
}

export function currencyListRequest(token) {
  return {
    type: GET_CURRENCY_LIST_REQUEST,
    token
  }
}

export function currencyListSuccess(data) {
  return {
    type: GET_CURRENCY_LIST_SUCCESS,
    data
  }
}

export function currencyListFail(error) {
  return {
    type: GET_CURRENCY_LIST_FAIL,
    error
  }
}

export function countryListRequest(token) {
  return {
    type: GET_COUNTRY_LIST_REQUEST,
    token
  }
}

export function countryListSuccess(data) {
  return {
    type: GET_COUNTRY_LIST_SUCCESS,
    data
  }
}

export function countryListFail(err) {
  return {
    type: GET_COUNTRY_LIST_FAIL,
    err
  }
}

export function offerListRequest(token, countryID) {
  return {
    type: GET_OFFER_LIST_REQUEST,
    token,
    countryID
  }
}

export function offerListSuccess(data) {
  return {
    type: GET_OFFER_LIST_SUCCESS,
    data
  }
}

export function offerListFail(err) {
  return {
    type: GET_OFFER_LIST_FAIL,
    err
  }
}

export function operatorsListRequest(token) {
  return {
    type: GET_OPERATORS_LIST_REQUEST,
    token
  }
}

export function operatorsListSuccess(data) {
  return {
    type: GET_OPERATORS_LIST_SUCCESS,
    data
  }
}

export function operatorsListFail(err) {
  return {
    type: GET_OPERATORS_LIST_FAIL,
    err
  }
}