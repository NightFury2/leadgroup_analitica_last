import {
  GET_CONV_STATS_REQUEST,
  GET_CONV_STATS_SUCCESS,
  GET_CONV_STATS_FAIL
} from './actionTypes'

export function conversionStatsRequest(token, country, offers, startDate, endDate) {
  console.log(country, offers, startDate, endDate);
  return {
    type: GET_CONV_STATS_REQUEST,
    token,
    country,
    offers,
    startDate,
    endDate
  }
}

export function conversionStatsSuccess(offerData, basicData) {
  return {
    type: GET_CONV_STATS_SUCCESS,
    offerData,
    basicData
  }
}

export function conversionStatsFail(error) {
  return {
    type: GET_CONV_STATS_FAIL,
    error
  }
}