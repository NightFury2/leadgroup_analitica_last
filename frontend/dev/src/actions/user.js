import {
  AUTH_SUCCESS,
  AUTH_FAIL,
  CHANGE_PROFILE_INFO_REQUEST,
  CHANGE_PROFILE_INFO_SUCCESS,
  CHANGE_PROFILE_INFO_FAIL,
  AUTH_REQUEST,
  GET_PROFILE_REQUEST,
  GET_PROFILE_FAIL,
  GET_PROFILE_SUCCESS,
  NEED_UPDATE_TOKEN,
  AUTH_LOGOUT,
  CHANGE_PROFILE_AVATAR_REQUEST,
  CHANGE_PROFILE_AVATAR_SUCCESS,
  CHANGE_PROFILE_AVATAR_FAIL,
  GET_PROFILE_AVATAR_LIST_REQUEST,
  GET_PROFILE_AVATAR_LIST_SUCCESS,
  GET_PROFILE_AVATAR_LIST_FAIL
} from './actionTypes'

export function authSuccess(accessToken, refreshToken, expiresIn) {
  return {
    type: AUTH_SUCCESS,
    accessToken,
    refreshToken,
    expiresIn,
    receivedAt: Date.now()
  }
}

export function authRequest(username, password) {
  return {
    type: AUTH_REQUEST,
    username: username,
    password: password
  }
}

export function authFail(error) {
  return {
    type: AUTH_FAIL,
    errorMessage: error
  }
}

export function needUpdateToken() {
  return {
    type: NEED_UPDATE_TOKEN
  }
}

export function getProfileInfoSuccess(email, firstName, lastName, photo) {
  return {
    type: GET_PROFILE_SUCCESS,
    email,
    firstName,
    lastName,
    photo
  }
}
export function getProfileInfoFail(error) {
  return {
    type: GET_PROFILE_FAIL,
    profileError: error
  }
}

export function getProfileRequest(token) {
  return {
    type: GET_PROFILE_REQUEST,
    token: token
  }
}

export function authLogout(token) {
  return {
    type: AUTH_LOGOUT,
    token: token
  }
}
export function changeProfileInfoRequest(email, password, firstName, lastName, photo, token) {
  return {
    type: CHANGE_PROFILE_INFO_REQUEST,
    email,
    password,
    firstName,
    lastName,
    photo,
    token
  }
}
export function changeProfileInfoSuccess(email, password, firstName, lastName, photo) {
  return {
    type: CHANGE_PROFILE_INFO_SUCCESS,
    email,
    password,
    firstName,
    lastName,
    photo
  }
}

export function changeProfileInfoFail(errorMessage) {
 return {
   type: CHANGE_PROFILE_INFO_FAIL,
   errorMessage
 }
}

export function changeProfileAvatarRequest(email, password, firstName, lastName, photo, token) {
  return {
    type: CHANGE_PROFILE_AVATAR_REQUEST,
    email,
    password,
    firstName,
    lastName,
    photo,
    token
  }
}

export function changeProfileAvatarSuccess(email, password, firstName, lastName, photo) {
  return {
    type: CHANGE_PROFILE_AVATAR_SUCCESS,
    email,
    password,
    firstName,
    lastName,
    photo
  }
}

export function changeProfileAvatarFail(error) {
  return {
    type: CHANGE_PROFILE_AVATAR_FAIL,
    error
  }
}

export function getAvatarListRequest(token) {
  return {
    type: GET_PROFILE_AVATAR_LIST_REQUEST,
    token
  }
}

export function getAvatarListSuccess(avatars) {
  return {
    type: GET_PROFILE_AVATAR_LIST_SUCCESS,
    avatars
  }
}

export function getAvatarListFail(err) {
  return {
    type: GET_PROFILE_AVATAR_LIST_FAIL,
    err
  }
}