import {
  GET_ACTIVITY_STATS_REQUEST,
  GET_ACTIVITY_STATS_SUCCESS,
  GET_ACTIVITY_STATS_FAIL
} from './actionTypes'

export function activityStatsRequest(token, country, startDate, endDate, operatorsList) {
  return {
    type: GET_ACTIVITY_STATS_REQUEST,
    token,
    country,
    startDate,
    endDate,
    operatorsList
  }
}

export function activityStatsSuccess(data) {
  return {
    type: GET_ACTIVITY_STATS_SUCCESS,
    data
  }
}

export function  activityStatsFail(error) {
  return {
    type: GET_ACTIVITY_STATS_FAIL,
    error
  }
}