import {
  GET_OPERATORS_STATS_REQUEST,
  GET_OPERATORS_STATS_SUCCESS,
  GET_OPERATORS_STATS_FAIL
} from './actionTypes'

export function operatorsStatsRequest(token, country, operators, startDate, endDate, operatorsList) {
  return {
    type: GET_OPERATORS_STATS_REQUEST,
    token,
    country,
    operators,
    startDate,
    endDate,
    operatorsList
  }
}

export function operatorsStatsSuccess(data) {
  return {
    type: GET_OPERATORS_STATS_SUCCESS,
    data
  }
}

export function  operatorsStatsFail(error) {
  return {
    type: GET_OPERATORS_STATS_FAIL,
    error
  }
}