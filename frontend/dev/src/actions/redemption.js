import {
  GET_REDEMPTION_STATS_REQUEST,
  GET_REDEMPTION_STATS_SUCCESS,
  GET_REDEMPTION_STATS_FAIL
} from './actionTypes'

export function redemptionStatsRequest(token, country, data, startDate, endDate) {
  return {
    type: GET_REDEMPTION_STATS_REQUEST,
    token,
    country,
    data,
    startDate,
    endDate
  }
}

export function redemptionStatsSuccess(data) {
  return {
    type: GET_REDEMPTION_STATS_SUCCESS,
    data
  }
}

export function  redemptionStatsFail(error) {
  return {
    type: GET_REDEMPTION_STATS_FAIL,
    error
  }
}