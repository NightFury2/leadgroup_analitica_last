import { take, call, put, cancelled, fork } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import axios from 'axios'
import {
  API_HOST,
  CHANGE_PROFILE_INFO_REQUEST,
  GET_PROFILE_REQUEST,
  CHANGE_PROFILE_AVATAR_REQUEST,
  GET_PROFILE_AVATAR_LIST_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/user'

// get-request for profile data, NOT password

export function* profileFlow() {
  while (true) {
    const {token} = yield take(GET_PROFILE_REQUEST)
    yield call(profile, token)
  }
}

function* profile(token) {
  try {
    const profile = yield call(getProfileInfo, token)
    yield put(actions.getProfileInfoSuccess(profile.email, profile.first_name, profile.last_name, profile.photo))
  } catch(err) {
    yield put(actions.getProfileInfoFail('Не удалось получить данные о пользователе'))
  }
}

function getProfileInfo(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/user',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      if (__DEVELOPMENT__) {
        console.log(response);
      }
      return response.data
    })
}

// get-request for avatar list like ids

export function* getAvatarListFlow() {
  while (true) {
    const {token} = yield take(GET_PROFILE_AVATAR_LIST_REQUEST)
    yield call(getAvatars, token)
  }
}

function* getAvatars(token) {
  try {
    const avatars = yield call(getAvatarList, token)
    yield put(actions.getAvatarListSuccess(avatars))
  } catch(err) {
    yield put(actions.getAvatarListFail(err))
  }
}

function getAvatarList(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/data/avatars',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}

// flow for changing some profile info like firstname, lastname, password and etc

export function* changeProfileInfoFlow() {
  while (true) {
    const {email, password, firstName, lastName, photo, token} = yield take(CHANGE_PROFILE_INFO_REQUEST)
    yield call(replaceProfileInfo, email, password, firstName, lastName, photo, token)
  }
}

function* replaceProfileInfo(email, password, firstName, lastName, photo, token) {
  try {
    yield call(changeProfileInfo, email, password, firstName, lastName, photo, token)
    yield put(actions.changeProfileInfoSuccess(email, password, firstName, lastName, photo))
  } catch(err) {
    yield put(actions.changeProfileInfoFail('Не удалось редактировать данные пользователя'))
  }
}

// flow for changing avatar picture, i.e. replacing photo id parameter

export function* changeProfileAvatarFlow() {
  while (true) {
    const {email, password, firstName, lastName, photo, token} = yield take(CHANGE_PROFILE_AVATAR_REQUEST)
    yield call(replaceProfileAvatar, email, password, firstName, lastName, photo, token)
  }
}

function* replaceProfileAvatar(email, password, firstName, lastName, photo, token) {
  try {
    yield call(changeProfileInfo, email, password, firstName, lastName, photo, token)
    yield put(actions.changeProfileAvatarSuccess(email, password, firstName, lastName, photo))
    yield put(actions.getProfileRequest(token))
  } catch(err) {
    yield put(actions.changeProfileAvatarFail(err))
  }
}

// post-request to change some parameters

function changeProfileInfo(email, password, firstName, lastName, photo, token) {
  return axios({
    method: 'post',
    url: `${API_HOST}`+'/api/' + 'user',
    data: {
      "email": email,
      "password": password,
      "first_name": firstName,
      "last_name": lastName,
      "photo": photo
    },
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      if (__DEVELOPMENT__) {
        console.log(response);
      }
      return response.data
    })
}