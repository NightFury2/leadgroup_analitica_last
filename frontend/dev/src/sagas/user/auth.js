
import { take, call, put, cancelled } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import axios from 'axios'
import {
  API_HOST,
  AUTH_REQUEST,
  AUTH_CANCEL,
  NEED_UPDATE_TOKEN,
  AUTH_LOGOUT,
  AUTH_FAIL,
  AUTH_LOGGING,
  GET_PROFILE_REQUEST,
} from '../../actions/actionTypes'
import * as actions from '../../actions/user'

// simply store and clear refreshToken from localstorage

function storeToken(refreshToken) {
  localStorage.setItem('refreshToken', refreshToken)
}
function clearToken(token) {
  localStorage.removeItem(token)
}

// login

export function* loginFlow() {
  while (true) {
    const {username, password} = yield take(AUTH_REQUEST)
    yield call(authorize, username, password)
  }
}

function* authorize(username, password) {
  try {
    yield put({type: AUTH_LOGGING})
    yield call(delay, 300)
    const data = yield call(loginWithUserPass, username, password)
    yield put(actions.authSuccess(data.access_token, data.refresh_token, data.expires_in))
    yield put(actions.getProfileRequest(data.access_token))
    yield call(storeToken, data.refresh_token)
  } catch(error) {
    yield call(clearToken, 'refreshToken')
    yield put(actions.authFail('Неверный логин или пароль'))
  } finally {
    if (yield cancelled()) {
      yield put({type: AUTH_CANCEL})
    }
  }
}

function loginWithUserPass(username, password) {
  return axios.post(`${API_HOST}`+'/api/oauth2/token', {
    "grant_type": "password",
    "username": username,
    "password": password,
    "client_id": "testclient",
    "client_secret": "testpass"
  })
    .then(response => response.data)
}

// auth by refreshToken and refreshing session

export function* refreshSessionFlow() {
  while (true) {
    yield take(NEED_UPDATE_TOKEN)
    yield call(refreshSession)
    yield take([AUTH_LOGOUT, AUTH_FAIL])
    yield call(clearToken, 'refreshToken')
  }
}

function* refreshSession() {
  try {
    const refreshToken = localStorage.getItem('refreshToken');
    const data = yield call(getNewToken, refreshToken);
    yield call(storeToken, data.refresh_token);
    yield put(actions.authSuccess(data.access_token, data.refresh_token, data.expires_in));
    yield put({type: GET_PROFILE_REQUEST, token: data.access_token})
  } catch(err) {
    yield call(clearToken, 'refreshToken')
  }
}

function getNewToken(refreshToken) {
  return axios.post(`${API_HOST}`+'/api/oauth2/token', {
    "grant_type": "refresh_token",
    "client_id": "testclient",
    "client_secret": "testpass",
    "refresh_token": refreshToken
  })
    .then(response => response.data)
}

// logout

export function* logoutFlow() {
  while (true) {
    const {token} = yield take(AUTH_LOGOUT)
    yield call(logout, token)
  }
}

function logout(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/user/logout',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}
