import { take, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
  API_HOST,
  GET_OPERATORS_STATS_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/operators'

export function* operatorsStatsFlow() {
  while (true) {
    const {token, country, operators, startDate, endDate, operatorsList} = yield take(GET_OPERATORS_STATS_REQUEST)
    console.log(operators)
    const operatorIds = yield call(sortOperatorsToString, operators)
    yield call(getOperators, token, country, operatorIds, startDate, endDate, operatorsList)
  }
}

export function sortOperatorsToString(operatorsObject) {
  let operatorIds = []
  console.log(operatorsObject)
  if (operatorsObject && operatorsObject.length > 0) {
    operatorsObject.map((item) => {
      operatorIds.push(item.value)
    })
    operatorIds.join()
  } else {
    return false
  }
  return operatorIds
}

function* getOperators(token, countryId, operators, startDate, endDate, operatorsList) {
  try {
    const data = yield call(getOperatorsData, token, countryId, operators, startDate, endDate, operatorsList)
    yield put(actions.operatorsStatsSuccess(data))
  } catch(err) {
    console.log(`error`)
    console.log(err)
    yield put(actions.operatorsStatsFail(err))
  }
}

const parseSum = (sum) => {
  if (!Number.isInteger(sum)) {
    return sum.toFixed(1)
  } else {
    return sum
  }
}
const toPercents = (current, total) => {
  if (current != 0) {
    return `${(current / total * 100).toFixed(1)}%`
  } else {
    return `${current}%`
  }
}

function getOperatorsData(token, countryId, operators, startDate, endDate, operatorsList) {
  let url = `${API_HOST}/api/statistic/operator-statistic?country_id=${countryId}`
  if (operators && operators.length > 0) {
    url = `${url}&operator_id=${operators}`
  }
  if (startDate && endDate) {
    url = `${url}&date_from=${startDate}&date_to=${endDate}`
  }
  return axios({
    method: 'get',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      let data = response.data
      console.group('operatorsList')
      console.log(operatorsList)
      console.groupEnd()
      console.group('got data')
      console.log(`GOT DATA`)
      console.log(data)
      console.groupEnd()
      data.map( item => {
        Object.keys(operatorsList).map( element => {
          let operator = operatorsList[element]
          if (operator.id == item.operator_id) {
            if (operator.name) {
              item.operatorName = `${operator.login} (${operator.name})`
            } else {
              item.operatorName = `(No name. Id: ${operator.id})`
            }
          }
        } )
        item.approvedPercents = toPercents(item.approved, item.total)
        item.buyoutPercents = toPercents(item.buyout, item.total)
        item.buyoutSummary = (item.buyout * item.average_bill_usd).toFixed(0)
        item.approvedSummary = (item.approved * item.average_bill_usd).toFixed(0)
        item.average_bill_usd = item.average_bill_usd.toFixed(0)
      } )
      return data
    })
}