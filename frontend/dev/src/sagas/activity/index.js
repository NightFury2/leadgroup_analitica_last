import { take, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
  API_HOST,
  GET_ACTIVITY_STATS_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/activity'

export function* activityStatsFlow() {
  while (true) {
    const { token, country, startDate, endDate, operatorsList } = yield take(GET_ACTIVITY_STATS_REQUEST)
    const operatorIds = yield call(sortOperatorsToString, operatorsList)
    yield call(getActivity, token, country, startDate, endDate, operatorIds)
  }
}

export function sortOperatorsToString(operatorsObject) {
  let operatorIds = []
  console.log(operatorsObject)
  if (operatorsObject.length > 0) {
    operatorsObject.map((item) => {
      operatorIds.push(item.value)
    })
    operatorIds.join()
  } else {
    return false
  }
  return operatorIds
}

function* getActivity(token, country, startDate, endDate, operatorIds) {
  try {
    const activityData = yield call(getActivityData, token, country, startDate, endDate, operatorIds)
    yield put(actions.activityStatsSuccess(activityData))
  } catch(err) {
    yield put(actions.activityStatsFail(err))
  }
}

function getActivityData(token, country, startDate, endDate, operatorIds) {
  let url = `${API_HOST}/api/statistic/operator-queries?country_id=${country}`;
  if (startDate && endDate) {
    url = `${url}&date_from=${startDate}&date_to=${endDate}`
  }
  if (operatorIds) {
    url = `${url}&operator_id=${operatorIds}`
  }
  console.log(`getActivityData: sending to ${url}`)
  return axios({
    method: 'get',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}