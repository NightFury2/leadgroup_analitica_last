import { take, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
  API_HOST,
  GET_CONV_STATS_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/conversions'

export function* conversionStatsFlow() {
  while (true) {
    const {token, country, offers, startDate, endDate} = yield take(GET_CONV_STATS_REQUEST)
    const offerIds = yield call(sortOffersToString, offers)
    yield call(getConversion, token, country, offerIds, startDate, endDate)
  }
}

export function sortOffersToString(offersObject) {
  let offerIds = []
  if (offersObject.length > 0) {
    offersObject.map((item) => {
      offerIds.push(item.value)
    })
    offerIds.join()
  } else {
    return false
  }
  return offerIds
}

function* getConversion(token, countryId, offerId, startDate, endDate) {
  try {
    const basicData = yield call(getBasicData, token, countryId, startDate, endDate)
    const offerData = yield call(getOfferData, token, countryId, offerId, startDate, endDate)
    yield put(actions.conversionStatsSuccess(offerData, basicData))
  } catch(err) {
    yield put(actions.conversionStatsFail(err))
  }
}

const parseSum = (sum) => {
  if (!Number.isInteger(sum)) {
    return sum.toFixed(1)
  } else {
    return sum
  }
}
const toPercents = (current, total) => {
  if (current != 0) {
    return `${(current / total * 100).toFixed(1)}%`
  } else {
    return `${current}%`
  }
}

function getBasicData(token, countryId, startDate, endDate) {
  let url = `${API_HOST}/api/statistic/conversions?country_id=${countryId}`
  if (startDate && endDate) {
    url = `${url}&date_from=${startDate}&date_to=${endDate}`
  }
  return axios({
    method: 'get',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      let data = response.data
      data.map(i => {
        i.totalPercents = toPercents(i.total, i.total)
        i.processedPercents = toPercents(i.processed, i.total)
        i.approvedPercents = toPercents(i.approved, i.total)
        i.canceledPercents = toPercents(i.canceled, i.total)
        i.errorPercents = toPercents(i.error, i.total)
        i.returnPercents = toPercents(i.return, i.total)
        i.paidPercents = toPercents(i.paid, i.total)
        i.average_bill_usd = parseSum(i.average_bill_usd)
        i.average_bill = parseInt(i.average_bill)
      })
      return data
    })
}

function getOfferData(token, countryId, offerId, startDate, endDate) {
  let url = `${API_HOST}/api/statistic/conversions?country_id=${countryId}`
  if (offerId && offerId.length > 0) {
    url = `${url}&offer_id=${offerId}`
  }
  if (startDate && endDate) {
    url = `${url}&date_from=${startDate}&date_to=${endDate}`
  }
  return axios({
    method: 'get',
    url: `${url}&apart=1`,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      let data = response.data
      Object.keys(data).map( item => {
        data[item].map( i => {
          i.totalPercents = toPercents(i.total, i.total)
          i.processedPercents = toPercents(i.processed, i.total)
          i.approvedPercents = toPercents(i.approved, i.total)
          i.canceledPercents = toPercents(i.canceled, i.total)
          i.errorPercents = toPercents(i.error, i.total)
          i.returnPercents = toPercents(i.return, i.total)
          i.paidPercents = toPercents(i.paid, i.total)
        })
      } )
      return data
    })
}