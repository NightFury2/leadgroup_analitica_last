import { fork } from 'redux-saga/effects'
import 'babel-polyfill'
import * as auth from './user/auth'
import * as profile from './user/profile'
import * as countries from './filters/countries'
import * as offers from './filters/offers'
import * as operators from './filters/operators'
import * as conversion from './conversion/index'
import * as activity from './activity/index'
import * as currency from './filters/currency'
import * as redemptionStats from './redemption'
import * as operatorsStats from './operators'

export default function* rootSaga() {
  yield fork(auth.refreshSessionFlow)
  yield fork(auth.loginFlow)
  yield fork(auth.logoutFlow)
  yield fork(profile.profileFlow)
  yield fork(profile.changeProfileInfoFlow)
  yield fork(profile.changeProfileAvatarFlow)
  yield fork(profile.getAvatarListFlow)
  yield fork(countries.countryListFlow)
  yield fork(offers.offerListFlow)
  yield fork(operators.operatorsListFlow)
  yield fork(conversion.conversionStatsFlow)
  yield fork(activity.activityStatsFlow)
  yield fork(currency.currencyListFlow)
  yield fork(operatorsStats.operatorsStatsFlow)
  yield fork(redemptionStats.redemptionStatsFlow)
}