import { take, call, put } from 'redux-saga/effects'
import axios from 'axios'
import {
  API_HOST,
  GET_REDEMPTION_STATS_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/redemption'

export function* redemptionStatsFlow() {
  while (true) {
    const {token, country, startDate, endDate} = yield take(GET_REDEMPTION_STATS_REQUEST)
    yield call(getRedemption, token, country, startDate, endDate)
  }
}

function* getRedemption(token, countryId, startDate, endDate) {
  try {
    const data = yield call(getRedemptionData, token, countryId, startDate, endDate)
    yield put(actions.redemptionStatsSuccess(data))
  } catch(err) {
    yield put(actions.redemptionStatsFail(err))
  }
}

const parseSum = (sum) => {
  if (!Number.isInteger(sum)) {
    return sum.toFixed(1)
  } else {
    return sum
  }
}
const toPercents = (current, total) => {
  if (current != 0) {
    return `${(current / total * 100).toFixed(1)}%`
  } else {
    return `${current}%`
  }
}

function getRedemptionData(token, countryId, startDate, endDate) {
  let url = `${API_HOST}/api/statistic/buyout-statistic?country_id=${countryId}`
  if (startDate && endDate) {
    url = `${url}&date_from=${startDate}&date_to=${endDate}`
  }
  console.log(`url is ${url}`)
  return axios({
    method: 'get',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => {
      let data = response.data
      data.map( item => {
        item.paidPercents = toPercents(item.paid, item.total)
        item.returnPercents = toPercents(item.return, item.total)
        item.processedPercents = toPercents(item.processed, item.total)
        item.approvedPercents = toPercents(item.paid + item.processed, item.total)
      } )
      console.log(data)
      return data
    })
}