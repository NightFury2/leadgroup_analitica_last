
import { take, call, put } from 'redux-saga/effects'
import {
  API_HOST,
  GET_OPERATORS_LIST_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/filters'
import axios from 'axios'

export function* operatorsListFlow() {
  while (true) {
    const {token} = yield take(GET_OPERATORS_LIST_REQUEST)
    yield call(operatorsList, token)
  }
}

function* operatorsList(token) {
  try {
    const list = yield call(getOperatorsList, token)
    if (list) {
      yield put(actions.operatorsListSuccess(list))
    } else {
      yield put(actions.operatorsListFail('Список операторов пуст'))
    }
  } catch(err) {
    yield put(actions.operatorsListFail('Не удалось получить список операторов'))
  }
}

function getOperatorsList(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/data/operators',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}