
import { take, call, put } from 'redux-saga/effects'
import {
  API_HOST,
  GET_CURRENCY_LIST_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/filters'

import axios from 'axios'

export function* currencyListFlow() {
  while (true) {
    const {token} = yield take(GET_CURRENCY_LIST_REQUEST)
    yield call(currencyList, token)
  }
}

function* currencyList(token) {
  try {
    const list = yield call(getCurrencyList, token)
    if (list) {
      yield put(actions.currencyListSuccess(list))
    } else {
      yield put(actions.currencyListFail('Список символов валют пуст'))
    }
  } catch(err) {
    yield put(actions.currencyListFail('Не удалось получить список символов валют'))
  }
}

function getCurrencyList(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/data/currency',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}