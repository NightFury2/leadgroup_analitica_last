
import { take, call, put } from 'redux-saga/effects'
import {
  API_HOST,
  GET_COUNTRY_LIST_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/filters'

import axios from 'axios'

export function* countryListFlow() {
  while (true) {
    const {token} = yield take(GET_COUNTRY_LIST_REQUEST)
    yield call(countryList, token)
  }
}

function* countryList(token) {
  try {
    const list = yield call(getCountryList, token)
    if (list.length > 0) {
      yield put(actions.countryListSuccess(list))
    } else {
      yield put(actions.countryListFail('Список стран пуст'))
    }
  } catch(err) {
    yield put(actions.countryListFail('Не удалось получить список стран'))
  }
}

function getCountryList(token) {
  return axios({
    method: 'get',
    url: `${API_HOST}`+'/api/data/countries',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}