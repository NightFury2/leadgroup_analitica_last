
import { take, call, put } from 'redux-saga/effects'
import {
  API_HOST,
  DEFAULT_COUNTRY,
  GET_OFFER_LIST_REQUEST
} from '../../actions/actionTypes'
import * as actions from '../../actions/filters'

import axios from 'axios'

export function* offerListFlow() {
  while (true) {
    const {token, countryID} = yield take(GET_OFFER_LIST_REQUEST);
    yield call(offerList, token, countryID)
  }
}

function* offerList(token, countryID) {
  try {
    const list = yield call(getOfferList, token, countryID);
    if (list.length > 0) {
      yield put(actions.offerListSuccess(list))
    } else {
      yield put(actions.offerListFail('Список офферов пуст'))
    }
  } catch(err) {
    yield put(actions.offerListFail('Не удалось получить список офферов'))
  }
}

function getOfferList(token, countryID) {
  countryID = countryID !== undefined ? countryID : parseInt(DEFAULT_COUNTRY);
  return axios({
    method: 'get',
    url: `${API_HOST}/api/data/offers?country_id=${countryID}`,
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
    .then(response => response.data)
}