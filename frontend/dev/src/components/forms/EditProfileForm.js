import React, { Component } from 'react'
import { Button, Form, FormGroup, InputGroup, InputGroupAddon, Input, FormFeedback, Alert, Card, CardHeader, CardBlock } from 'reactstrap'

class EditProfileForm extends Component {
  constructor(props) {
    super(props)
    const { firstName, lastName, email } = this.props
    this.state = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      submit: false,
      password: "",
      confirmPassword: "",
      submitPassword: false,
      validatePassMsg: null,
      validatePassSuccess: false
    }
    this.handleFirstName = this.handleFirstName.bind(this)
    this.handleLastName = this.handleLastName.bind(this)
    this.handleEmail = this.handleEmail.bind(this)
    this.submit = this.submit.bind(this)
    this.handlePassword = this.handlePassword.bind(this)
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this)
    this.validatePass = this.validatePass.bind(this)
  }
  handleFirstName(e) {
    this.setState({
      firstName: e.target.value
    })
  }
  handleLastName(e) {
    this.setState({
      lastName: e.target.value
    })
  }
  handleEmail(e) {
    this.setState({
      email: e.target.value
    })
  }
  submit(e) {
    e.preventDefault()
    const { changeInfo, accessToken, photo, password } = this.props
    const { email, firstName, lastName } = this.state
    this.setState({
      submit: true
    })
    setTimeout(() => {
      this.setState({
        submit: false
      })
    }, 300)
    changeInfo(email, password, firstName, lastName, photo, accessToken)
  }
  handlePassword(e) {
    this.setState({
      password: e.target.value
    })
  }
  handleConfirmPassword(e) {
    this.setState({
      confirmPassword: e.target.value
    })
  }
  validatePass(e) {
    e.preventDefault()
    if (!this.state.password.match(/^[\w]{1,20}$/)) {
      this.setState({
        validatePassMsg: 'Пароль может содержать только латинские буквы и цифры',
        validatePassSuccess: false
      })
    } else if (this.state.password.length < 6) {
      this.setState({
        validatePassMsg: 'Длина пароля должна быть не меньше 6 символов',
        validatePassSuccess: false
      })
    } else if (this.state.password.length > 5 && this.state.password !== this.state.confirmPassword) {
      this.setState({
        validatePassMsg: 'Пароли не совпадают',
        validatePassSuccess: false
      })
      console.log(`Пароли не совпадают`)
    } else if (this.state.password.length > 16) {
      this.setState({
        validatePassMsg: 'Длина пароля должна быть не больше 16 символов',
        validatePassSuccess: false
      })
    } else if (this.state.password === this.props.password) {
      this.setState({
        validatePassMsg: 'Вы уже используете этот пароль',
        validatePassSuccess: false
      })
    } else {
      this.setState({
        validatePassMsg: null,
        validatePassSuccess: false
      })
      const { password } = this.state
      const { email, firstName, lastName, accessToken, photo, changeInfo } = this.props
      changeInfo(email, password, firstName, lastName, photo, accessToken)
      this.setState({
        validatePassMsg: 'Пароль изменен',
        validatePassSuccess: true
      })
    }
  }
  render() {
    const validateError = (
      <Alert
        color={this.state.validatePassSuccess ? "success" : "danger"}
      >
        {this.state.validatePassMsg}
      </Alert>
    )
    return (
      <div>
        <Form onSubmit={this.submit} className="animated fadeIn">
          <Card>
            <CardHeader tag="h3">Информация о пользователе</CardHeader>
            <CardBlock>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon>Имя</InputGroupAddon>
                  <Input required type="text" value={this.state.firstName} onChange={this.handleFirstName} placeholder="Введите текст..."/>
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon>Фамилия</InputGroupAddon>
                  <Input required type="text" value={this.state.lastName} onChange={this.handleLastName} placeholder="Введите текст..."/>
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon>Email</InputGroupAddon>
                  <Input required type="text" value={this.state.email} onChange={this.handleEmail} placeholder="Введите текст..."/>
                </InputGroup>
              </FormGroup>
              <Button
                block
                type="submit"
                color={this.state.submit ? "success" : "info"}
              >
                Сохранить
              </Button>
            </CardBlock>
          </Card>
        </Form>
        <hr />
        <Form className="animated fadeIn" onSubmit={this.validatePass}>
          <Card>
            <CardHeader tag="h3">Смена пароля</CardHeader>
            <CardBlock>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon>Новый пароль</InputGroupAddon>
                  <Input
                    required
                    type="password"
                    value={this.state.password}
                    onChange={this.handlePassword}
                    placeholder="Пароль"
                  />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon>Подтверждение</InputGroupAddon>
                  <Input
                    required
                    type="password"
                    value={this.state.confirmPassword}
                    onChange={this.handleConfirmPassword}
                    placeholder="Пароль"
                  />
                </InputGroup>
              </FormGroup>
              <FormFeedback>
                {typeof this.state.validatePassMsg === 'string' ? validateError : null}
              </FormFeedback>
              <Button
                block
                type="submit"
                color={this.state.submitPassword ? "danger" : "info"}
              >
                Сменить пароль
              </Button>
            </CardBlock>
          </Card>
        </Form>
      </div>
    )
  }
}

export default EditProfileForm