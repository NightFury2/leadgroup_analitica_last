import React, { Component } from 'react'
import { Button, Form, Card, CardBlock, CardHeader } from 'reactstrap'

class EditAvatarForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: null,
      chosenAvatar: this.props.photo
    }
    this.handleAvatarItem = this.handleAvatarItem.bind(this)
    this.submitAvatar = this.submitAvatar.bind(this)
  }
  handleAvatarItem(e) {
    this.setState({
      chosenAvatar: e.target.id
    })
  }
  submitAvatar() {
    const { changeAvatar, email, password, firstName, lastName, photo, accessToken } = this.props
    const { chosenAvatar } = this.state
    if (chosenAvatar != photo) {
      changeAvatar(email, password, firstName, lastName, chosenAvatar, accessToken)
    }
  }
  render() {
    const { avatarList } = this.props
    const { chosenAvatar } = this.state
    const avatars = avatarList.map((item, index) => (
        <div key={index} className={chosenAvatar==index ? "active" : null}>
          <img
            id={index}
            data-source={item}
            className="avatar"
            src={item}
            onClick={this.handleAvatarItem}
          />
        </div>
      )
    )
    return (
      <Form className="animated fadeIn">
        <Card>
          <CardHeader tag="h3">
            Выберите аватар
          </CardHeader>
          <CardBlock className="avatar-list">
            {
              avatarList.length > 1
                ?
                avatars
                : null
            }
          </CardBlock>
        </Card>
        <Button
          color="info"
          block
          type="button"
          onClick={this.submitAvatar}
        >
          Сохранить
        </Button>
      </Form>
    )
  }
}

export default EditAvatarForm