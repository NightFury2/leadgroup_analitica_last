import React, { Component } from 'react'
import { Row, Col, FormFeedback, Form, FormGroup, InputGroupAddon, Input, InputGroup, Button } from 'reactstrap'
import { Icon } from 'react-fa'
import logo from '../../styles/img/leadgroup.png'

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.handleLoginForm = this.handleLoginForm.bind(this)
    this.handlePasswordForm = this.handlePasswordForm.bind(this)
    this.submitData = this.submitData.bind(this)
    this.state = {
      username: '',
      password: ''
    }
  }
  handleLoginForm(event) {
    this.setState({
      username: event.target.value
    })
  }
  handlePasswordForm(event) {
    this.setState({
      password: event.target.value
    })
  }
  submitData(event) {
    event.preventDefault()
    console.log(this.state.username, this.state.password)
    this.props.submit(this.state.username, this.state.password)
  }
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center" id="auth-form">
          <div className="col-md-4">
            <div className="card-group mb-0">
              <div className="card p-4">
                <div className="card-block">
                  <Form>
                    <h1
                      className="text-center"
                    >
                      <img src={logo} className="logo" />
                    </h1>
                    <FormGroup color={this.props.fail ? 'danger' : null}>
                      <InputGroup>
                        <InputGroupAddon><Icon name="user" /></InputGroupAddon>
                        <Input
                          state={this.props.fail ? 'danger' : null}
                          value={this.state.username}
                          onChange={this.handleLoginForm}
                          placeholder="Логин"
                          className="zero-border-radius"
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup
                      color={this.props.fail ? 'danger' : null}
                    >
                      <InputGroup>
                        <InputGroupAddon><Icon name="lock" /></InputGroupAddon>
                        <Input
                          type="password"
                          state={this.props.fail ? 'danger' : null}
                          value={this.state.password}
                          onChange={this.handlePasswordForm}
                          placeholder="Пароль"
                          className="zero-border-radius"
                        />
                      </InputGroup>
                      <FormFeedback>{this.props.children}</FormFeedback>
                    </FormGroup>
                    <Row className="text-center">
                      <Col>
                        <Button
                          type="submit"
                          onClick={this.submitData}
                          className="btn btn-primary px-4"
                          color={this.props.fail ? 'danger' : 'info'}
                        >
                          Войти
                          {this.props.isLoading ? <Icon name="fa circle fa-spin fa-sm" /> : null}
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default LoginForm