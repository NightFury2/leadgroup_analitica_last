import React, { Component } from 'react'
import { Button, Col, Row } from 'reactstrap'
import { Icon } from 'react-fa'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { conversionStatsRequest } from '../../actions/conversions'

class FilterForm extends Component {
  constructor(props) {
    super(props)
    this.handleFormView = this.handleFormView.bind(this)
    this.state = {
      showForm: false
    }
  }
  handleFormView() {
    this.setState({
      showForm: !this.state.showForm
    })
  }
  render() {
    const { title, submit, children } = this.props
    const { showForm } = this.state
    return (
      <Row>
        <Col xs="12" sm="12" md="12" lg="12" className="text-center">
            <Link
              to="/home"
              className="pull-left d-md-down-none"
            >
              <Button>
                <Icon name="arrow-left" />
                {'  '}На главную
              </Button>
            </Link>
            <h3>
              {title}
              <Button
                size="sm"
                onClick={this.handleFormView}
                className="filter-btn"
              >
                <Icon name="filter"/>
              </Button>
            </h3>
              {showForm
                ?
                <div>
                  {children}
                  <Button
                    id="filter"
                    color="success"
                    size="large"
                    onClick={submit}
                  >
                    Выполнить
                  </Button>
                </div>
                :
                null
              }
              <div className="pad-a-bit" />
        </Col>
      </Row>
    )
  }
}

function mapStateToProps(state) {
  return {
    chosenCountryId: state.handleFilters.chosenCountryId,
    selectedOffers: state.handleFilters.selectedOffers,
    token: state.handleLogin.accessToken
  }
}

function mapDispatchToProps(dispatch) {
  return {
    submitFilterData: (token, country, offers) => dispatch(conversionStatsRequest(token, country, offers))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterForm)