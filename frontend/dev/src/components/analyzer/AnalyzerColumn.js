import React, {Component} from 'react'
import { Col } from 'reactstrap'
import Select from 'react-select'

class AnalyzerColumn extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Col xs={12} sm={12} md={2} lg={2}>
        <Select
          name="country"
          placeholder="Страна"
          value={this.props.countryValue}
          options={this.props.countryData}
          onChange={this.props.handleCountryValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="courier"
          placeholder="Курьерская служба"
          value={this.props.courierValue}
          options={this.props.courierData}
          onChange={this.props.handleCourierValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}
          multi={true}/>
        <Select
          name="period"
          placeholder="Период"
          value={this.props.periodValue}
          options={this.props.periodData}
          onChange={this.props.handlePeriodValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="tax"
          placeholder="НДС (максимум 100)"
          value={this.props.taxValue}
          options={this.props.taxData}
          onChange={this.props.handleTaxValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-1"
          placeholder="Продукт"
          value={this.props.articleOneValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleOneValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-2"
          placeholder="Продукт"
          value={this.props.articleTwoValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleTwoValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-3"
          placeholder="Продукт"
          value={this.props.articleThreeValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleThreeValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-4"
          placeholder="Продукт"
          value={this.props.articleFourValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleFourValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-5"
          placeholder="Продукт"
          value={this.props.articleFiveValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleFiveValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
        <Select
          name="article-6"
          placeholder="Продукт"
          value={this.props.articleSixValue}
          options={this.props.articleData}
          onChange={this.props.handleArticleSixValue}
          clearable={this.props.clearable}
          backspaceRemoves={this.props.backspaceRemoves}/>
      </Col>
    )
  }
}

export default AnalyzerColumn