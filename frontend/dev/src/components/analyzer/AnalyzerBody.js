import React, { Component } from 'react'
import { Col, Row, CardTitle, Button } from 'reactstrap'
import Select from 'react-select'
import Panel from '../common/Panel'

class AnalyzerBody extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Col xs="12" sm="12" md="10" lg="10">
        <Row>
          <Col xs="6" sm="6" md="6" lg="6">
            <Panel inverse style={{backgroundColor: '#333', borderColor: '#333'}}>
              <CardTitle>Выручка</CardTitle>
            </Panel>
          </Col>
          <Col xs="6" sm="6" md="6" lg="6">
            <Panel inverse style={{backgroundColor: '#333', borderColor: '#333'}}>
              <CardTitle>Прибыль</CardTitle>
            </Panel>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Button block color="success">Посчитать</Button>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
        <Row>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
          <Col xs="2" sm="2" md="2" lg="2">
            <Select
              name="country"
              placeholder="Продукт"/>
          </Col>
        </Row>
      </Col>
    )
  }
}

export default AnalyzerBody