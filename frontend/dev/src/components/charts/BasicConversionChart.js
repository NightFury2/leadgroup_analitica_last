import React, { Component } from 'react'
import { Table, Button } from 'reactstrap'
import ConversionChartHead from './ConversionChartHead'
import Loading from 'react-loading-spinner'
import ReactDOMServer from 'react-dom/server'
import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import ReactTable from 'react-table';
import _ from 'lodash';

class BasicConversionChart extends Component {
  constructor(props) {
    super(props)
    this.exportBasicData = this.exportBasicData.bind(this)
    this.options = {
      defaultSortName: 'date',  // default sort column name
      defaultSortOrder: 'desc'  // default sort order
    }
  }
  exportBasicData() {
    const { data } = this.props
    let percentReplacer = /%<\/td/g
    let dollarReplacer = /\$<\/td/g
    let table = document.querySelector(`.conversion-basic-data`).outerHTML
    let stringedTable = ReactDOMServer.renderToStaticMarkup(<div dangerouslySetInnerHTML={{__html: table}}></div>)
    let replacedStringedTable = stringedTable.toString().replace(percentReplacer, '</td')
    replacedStringedTable = replacedStringedTable.toString().replace(dollarReplacer, '</td>')
    let container = document.createElement('div')
    container.innerHTML = replacedStringedTable
    let wb = XLSX.utils.table_to_book(container)
    let wopts = { bookType:'xlsx', bookSST: false, type: 'binary', cellDates: false };
    let wbout = XLSX.write(wb, wopts);
    const s2ab = (s) => {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i);
      return buf;
    }
    saveAs(
      new Blob(
        [s2ab(wbout)],
        {type:"application/octet-stream"}
      ),
      `leadgroup-conversion-basic-stat-${new Date(Date.now()).getFullYear()}-${new Date(Date.now()).getMonth() + 1}-${new Date(Date.now()).getDate()}.xlsx`
    )
  }
  render() {
    const { data, toPercents, parseSum, currentCurrency, symbols, isLoading } = this.props;
    _.map(data, (dataItem, dataIndex) => {
      let approvedPercentsTotal = 0;
      approvedPercentsTotal = ((dataItem.approved + dataItem.return + dataItem.canceled) / dataItem.total * 100).toFixed(1);
      return dataItem.approvedPercentsTotal = `${approvedPercentsTotal}%`;
    });
    // console.log(data);
    const total = values => {
      let param = values.column.id
      let total = 0
      values.data.map(col => {
        return total = total + col[param]
      })
      return total
    }
    const percents = values => {
      let total = 0
      let current = 0
      let param = values.column.id
      let percents
      values.data.map(col => {
        return (
          current = current + col[param.toString().split('P')[0]],
          total = total + col['total']
        )
      })
      return percents = toPercents(current, total)
    }
    const average_bill = (values, currency) => {
      let total = 0
      let average = 0
      let cols = Object.keys(values.data).length
      let param = values.column.id
      values.data.map(col => {
        return total = total + Number(col[param])
      })
      return average = `${(total / cols).toFixed(1)} ${currency}`
    }
    const columns = [
      {
        Header: 'Дата',
        columns: [{
          Header: 'ГГГГ/ММ/ДД',
          accessor: 'date',
          Footer: 'Итого'
        }],
      },
      {
        Header: 'Всего',
        headerClassName: 'blue-grey lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'total',
          Footer: values => total(values)
        }]
      },
      {
        Header: 'В обработке',
        headerClassName: 'orange lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'processed',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'processedPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Принято',
        headerClassName: 'teal lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'approved',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'approvedPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Отмен',
        headerClassName: 'red lighten-3',
        columns: [{
          Header: 'Кол-во',
          accessor: 'canceled',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'canceledPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Спам/ошибки/дубль',
        headerClassName: 'pink lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'error',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'errorPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Возвратов',
        headerClassName: 'red accent-1',
        columns: [{
          Header: 'Кол-во',
          accessor: 'return',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'returnPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Средний чек',
        headerClassName: 'blue lighten-3',
        columns: [{
          Header: 'USD',
          accessor: 'average_bill_usd',
          Footer: (values, rows) => average_bill(values, '$')
        },{
          Header: currentCurrency,
          accessor: 'average_bill',
          Footer: (values, rows) => average_bill(values, currentCurrency)
        }]
      },
      {
        Header: 'Оплачено',
        headerClassName: 'green accent-1',
        columns: [{
          Header: 'Кол-во',
          accessor: 'paid',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'paidPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Апрув',
        columns: [{
          Header: '%',
          accessor: 'approvedPercentsTotal',
          Footer: values => percents(values)
        }]
      },
    ];
    const TfootComponent = (row) => {
      return (
        <div className={row.className !== undefined ? row.className : 'rt-tfoot'} style={row.style !== undefined ? row.style : 'min-width: 1700px;'} key={0}>
          <div className={`rt-tr ${row.children.props.className !== undefined ? row.children.props.className : ''}`} style={row.children.props.style !== undefined ? row.children.props.style : {}}>
            {
              _.map(row.children.props.children, (child, index) => {
                return (
                  <div className={`rt-td ${child.props.className !== undefined ? child.props.className : ''}`} style={child.props.style !== undefined ? {...child.props.style} : {}} key={index}>
                    <strong>{child.props.children}</strong>
                  </div>
                );
              })
            }
          </div>
          {/*TODO double header*/}
        </div>
      );
    };
    return (
      <Loading
        isLoading={isLoading}
        loadingClassName='loading-md'
      >
        <div>
          {
            data ?
              <Button
                size="sm"
                onClick={this.exportBasicData}
              >
                Экспорт в Excel
              </Button>
              : null
          }
          <div className="pad-a-bit" />
          <ReactTable
            data={data}
            columns={columns}
            minRows={7}
            nextText="Далее"
            loadingText="Загрузка..."
            previousText="Назад"
            pageText="Страница"
            ofText="из"
            className="-highlight -striped"
            noDataText="Строка не найдена"
            rowsText="строк"
            TfootComponent={TfootComponent}
          />
          <div className="pad-a-bit" />
        </div>
      </Loading>
    )
  }
}

export default BasicConversionChart