import React from 'react'
import { Table, Col } from 'reactstrap'
import ReactHighcharts from 'react-highcharts'
import GridRow from '../common/GridRow'

const OfferChart = (props) => {
  const config = {
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      type: 'datetime'
    },
    series: [{
      data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
    }],
    chart: {
      backgroundColor: 'transparent'
    }
  }
  // Захуярь сюда еще тултип
  return (
    <GridRow>
      <Col xs={12} sm={12} md={4} lg={4}>
        <Table striped className="no-padding" responsive size="sm">
          <tbody>
            <tr>
              <td>Доход</td>
              <td>12000</td>
            </tr>
            <tr>
              <td>Кол-во заказов</td>
              <td>315</td>
            </tr>
            <tr>
              <td>Profit</td>
              <td>50$</td>
            </tr>
            <tr>
              <td>Средний чек</td>
              <td>70$</td>
            </tr>
            <tr>
              <td>Апрув</td>
              <td>55%</td>
            </tr>
            <tr>
              <td>Операторы</td>
              <td>67</td>
            </tr>
            <tr>
              <td>Задержки</td>
              <td>4</td>
            </tr>
            <tr>
              <td>Повторные продажи</td>
              <td>30</td>
            </tr>
            <tr>
              <td>Доля отмененных товаров</td>
              <td>30%</td>
            </tr>
            <tr>
              <td>Закупочная стоимость проданных товаров</td>
              <td>1500$</td>
            </tr>
            <tr>
              <td>Среднее время выполнения заказа</td>
              <td>3 дн. 16 ч.</td>
            </tr>
            <tr>
              <td>Баланс web</td>
              <td>20500$</td>
            </tr>
            <tr>
              <td>Баланс Hendel</td>
              <td>6200$</td>
            </tr>
            <tr>
              <td>Баланс Миронова</td>
              <td>1500$</td>
            </tr>
          </tbody>
        </Table>
      </Col>
      <Col xs={12} sm={12} md={8} lg={8}>
        <ReactHighcharts config={config} />
      </Col>
    </GridRow>
  )
}

export default OfferChart
