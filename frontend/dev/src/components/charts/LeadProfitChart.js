import React from 'react'
import { Table, Col } from 'reactstrap'

const LeadProfitChart = (props) => {

  return (
    <Col xs="12" sm="12" md="12" lg="12">
      <Table responsive size="sm" bordered>
        <thead>
        <tr>
          <th></th>
          <th>Доля оффера в стране (сумма)</th>
          <th>Profit за 1 лид</th>
          <th>Выкуп</th>
          <th>Средний чек</th>
          <th>Апрув</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Оффер 1</td>
          <td>10%</td>
          <td>5$</td>
          <td>60%</td>
          <td>26%</td>
          <td>47%</td>
        </tr>
        {props.children}
        </tbody>
      </Table>
    </Col>
  )
}

export default LeadProfitChart
