import React from 'react'
import { Table, Col } from 'reactstrap'

const ProductsChart = (props) => {
  return (
    <Col xs="12" sm="12" md="12" lg="12">
      <Table responsive size="sm" bordered>
        <thead>
          <tr>
            <th>#</th>
            <th>Дата</th>
            <th>Название товара</th>
            <th>Остаток</th>
            <th>Дней до израсходования товара</th>
            <th>Расход</th>
            <th>В пути товара</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>21.10.12</td>
            <td>MilkyWay</td>
            <td>1200</td>
            <td>5</td>
            <td>20</td>
            <td>2200</td>
          </tr>
          {props.children}
        </tbody>
      </Table>
    </Col>
  )
}
export default ProductsChart
