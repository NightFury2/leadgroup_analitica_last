import React from 'react'
import { Table, Col } from 'reactstrap'

const OrderChart = (props) => {
  return (
    <Col xs="12" sm="12" md="12" lg="12">
      <Table responsive size="sm" bordered>
        <thead>
        <tr>
          <th>Этап</th>
          <th>Заказы</th>
          <th>Прогресс</th>
          <th>Время нахождения на этапе</th>
          <th>Товары</th>
          <th>Стоимость</th>
        </tr>
        </thead>
        <tbody>
        {props.children}
        </tbody>
      </Table>
    </Col>
  )
}

export default OrderChart
