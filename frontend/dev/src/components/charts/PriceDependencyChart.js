import React, { Component } from 'react'
import { Row, Col, Button } from 'reactstrap'
import Select from 'react-select'
import ReactHighcharts from 'react-highcharts'

class PriceDependencyChart extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const config = {
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        type: 'datetime'
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
      }],
      chart: {
        backgroundColor: 'transparent'
      }
    }
    return (
      <Col xs="12" sm="12" md="12" lg="12">
        <Row>
          <Col xs="12" sm="12" md="4" lg="4">
            <Select
              name="country"
              placeholder="Страна"/>
            <Select
              name="country"
              placeholder="Оффер"/>
            <Select
              name="country"
              placeholder="Курьер"/>
          </Col>
          <Col xs="12" sm="12" md="8" lg="8">
            <ReactHighcharts config={config} />
          </Col>
        </Row>
      </Col>
    )
  }
}

export default PriceDependencyChart