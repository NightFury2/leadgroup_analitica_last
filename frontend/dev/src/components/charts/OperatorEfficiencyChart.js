import React from 'react'
import { Table, Col } from 'reactstrap'

const OperatorEfficiencyChart = (props) => (
  <Col xs="12" sm="12" md="12" lg="12">
    <Table responsive size="sm" bordered>
      <thead>
      <tr>
        <th>Всего обработано</th>
        <th>Апрув (Подтвержденные)</th>
        <th>Отмены</th>
        <th>До выкупа (заказы)</th>
        <th>До выкупа (попытки)</th>
        <th>Перезвоны (заказы)</th>
        <th>Перезвоны (попытки)</th>
        <th>Средний чек</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td>211012</td>
        <td>88</td>
        <td>9</td>
        <td>11</td>
        <td>22</td>
        <td>2</td>
        <td>1</td>
        <td>2.99</td>
      </tr>
      {props.children}
      </tbody>
    </Table>
  </Col>
)

export default OperatorEfficiencyChart
