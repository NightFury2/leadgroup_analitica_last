import React, { Component } from 'react'
import { Table, Button } from 'reactstrap'
import ConversionChartHead from './ConversionChartHead'
import Loading from 'react-loading-spinner'
import ReactDOMServer from 'react-dom/server'
import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import ReactTable from 'react-table'
import _ from 'lodash';

class ConversionChart extends Component {
  constructor(props) {
    super(props)
    this.exportOfferData = this.exportOfferData.bind(this);
    this.options = {
      defaultSortName: 'date',  // default sort column name
      defaultSortOrder: 'desc'  // default sort order
    }
  }
  exportOfferData() {
    const { data, offerList, currentCurrency } = this.props;
    let
      percentReplacer = /%<\/td/g,
      dollarReplacer = /\$<\/td/g,
      worksheets = {},
      tbl = {},
      wb,
      sheetsList = [],
      offerIds = {};
    XLSX.SSF.init_table(tbl);
    Object.keys(offerList).map(item => {
      offerIds[offerList[item].id] = {
        id: offerList[item].id,
        name: offerList[item].name
      }
    });
    Object.keys(data).map(item => {
      let name = offerIds[item].name
      sheetsList.push(name)
      const TableSheet = () => (
        <BootstrapTable
          tableContainerClass={`conversion-offer-data-${item}`}
          tableHeaderClass="custom-caret"
          striped
          tableStyle={{fontSize: '12px'}}
          data={data[item]}
          options={this.options}
        >
          <TableHeaderColumn row="0" colSpan="2" isKey dataField='date' dataSort>Дата</TableHeaderColumn>

          <TableHeaderColumn row="1" colSpan="2" tdAttr={{'colSpan': '2'}} dataField='date'
                             dataSort>ГГГГ/ММ/ДД</TableHeaderColumn>

          <TableHeaderColumn row="0" colSpan="2" dataField='total' dataSort>Всего заказов</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='total' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='totalPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'processed'} row="0" colSpan="2" dataField='processed' dataSort>В
            обработке</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='processed' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='processedPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'accepted'} row="0" colSpan="2" dataField='approved'
                             dataSort>Принято</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='approved' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='approvedPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'canceled'} row="0" colSpan="2" dataField='canceled'
                             dataSort>Отмен</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='canceled' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='canceledPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'error'} row="0" colSpan="2" dataField='error' dataSort>Спам/ошибки/дубль</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='error' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='errorPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'return'} row="0" colSpan="2" dataField='return'
                             dataSort>Возвратов</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='return' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='returnPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn className={'average_bill'} row="0" colSpan="2" dataField='average_bill_usd'
                             dataSort>Средний чек</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='average_bill_usd' dataSort>USD</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='average_bill' dataSort>{currentCurrency}</TableHeaderColumn>

          <TableHeaderColumn className={'paid'} row="0" colSpan="2" dataField='paid'
                             dataSort>Оплачено</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='paid' dataSort>Кол-во</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField='paidPercents' dataSort>%</TableHeaderColumn>

          <TableHeaderColumn row="0" className={'approved'} dataField='approved'
                             dataSort>Апрув</TableHeaderColumn>

          <TableHeaderColumn row="1" dataField='approvedPercents' dataSort>%</TableHeaderColumn>
        </BootstrapTable>
      )
      let stringedTable = ReactDOMServer.renderToStaticMarkup(<TableSheet />)
      let replacedStringedTable = stringedTable.toString().replace(percentReplacer, '</td')
      replacedStringedTable = replacedStringedTable.toString().replace(dollarReplacer, '</td>')
      let container = document.createElement('div')
      container.innerHTML = replacedStringedTable
      let ws = XLSX.utils.table_to_sheet(container)
      worksheets[name] = ws
    })
    wb = {
      SSF: tbl,
      SheetNames: sheetsList,
      Sheets: worksheets
    };
    let wopts = { bookType:'xlsx', bookSST: false, type: 'binary', cellDates: false };
    let wbout = XLSX.write(wb, wopts);
    const s2ab = (s) => {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i);
      return buf;
    };
    saveAs(
      new Blob(
        [s2ab(wbout)],
        {type:"application/octet-stream"}
      ),
      `leadgroup-conversion-offer-stat-${new Date(Date.now()).getFullYear()}-${new Date(Date.now()).getMonth() + 1}-${new Date(Date.now()).getDate()}.xlsx`
    )
  }
  render() {
    const {
      offerList,
      isLoading,
      data,
      currentCurrency,
      toPercents,
      parseSum,
      symbols
    } = this.props;
    console.log(data, offerList);
    const total = values => {
      let param = values.column.id
      let total = 0
      values.data.map(col => {
        return total = total + col[param]
      })
      return total
    };
    const percents = values => {
      let total = 0
      let current = 0
      let param = values.column.id
      let percents
      values.data.map(col => {
        return (
          current = current + col[param.toString().split('P')[0]],
          total = total + col['total']
        )
      })
      return percents = toPercents(current, total)
    };
    const columns = [
      {
        Header: 'Дата',
        columns: [{
          Header: 'ГГГГ/ММ/ДД',
          accessor: 'date',
          Footer: 'Итого'
        }]
      },
      {
        Header: 'Всего',
        headerClassName: 'blue-grey lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'total',
          Footer: values => total(values)
        }]
      },
      {
        Header: 'В обработке',
        headerClassName: 'orange lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'processed',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'processedPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Принято',
        headerClassName: 'teal lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'approved',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'approvedPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Отмен',
        headerClassName: 'red lighten-3',
        columns: [{
          Header: 'Кол-во',
          accessor: 'canceled',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'canceledPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Спам/ошибки/дубль',
        headerClassName: 'pink lighten-4',
        columns: [{
          Header: 'Кол-во',
          accessor: 'error',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'errorPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Возвратов',
        headerClassName: 'red accent-1',
        columns: [{
          Header: 'Кол-во',
          accessor: 'return',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'returnPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Средний чек',
        headerClassName: 'blue lighten-3',
        columns: [{
          Header: 'USD',
          accessor: 'average_bill_usd'
        },{
          Header: currentCurrency,
          accessor: 'average_bill'
        }]
      },
      {
        Header: 'Оплачено',
        headerClassName: 'green accent-1',
        columns: [{
          Header: 'Кол-во',
          accessor: 'paid',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'paidPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Апрув',
        columns: [{
          Header: '%',
          accessor: 'approvedPercentsTotal',
          Footer: values => percents(values)
        }]
      },
    ];
    const TfootComponent = (row) => {
      return (
        <div className={row.className !== undefined ? row.className : 'rt-tfoot'} style={row.style !== undefined ? row.style : 'min-width: 1700px;'}>
          <div className={`rt-tr ${row.children.props.className !== undefined ? row.children.props.className : ''}`} style={row.children.props.style !== undefined ? row.children.props.style : {}}>
            {
              _.map(row.children.props.children, (child, index) => {
                return (
                  <div className={`rt-td ${child.props.className !== undefined ? child.props.className : ''}`} style={child.props.style !== undefined ? {...child.props.style} : {}}>
                    <strong>{child.props.children}</strong>
                  </div>
                );
              })
            }
          </div>
          {/*TODO double header*/}
        </div>
      );
    };
    return (
      <Loading
        isLoading={isLoading}
        loadingClassName='loading-md'
      >
        <div>
          {
            data ?
              <Button
                size="sm"
                onClick={this.exportOfferData}
              >
                Экспорт в Excel
              </Button>
              : null
          }
          {
            data ? (
              <div>
                {
                  _.map(data, (dataItem, dataIndex) => {
                    const offer = _.find(offerList, {id: parseInt(dataIndex)});
                    _.map(dataItem, (dataItem) => {
                      let approvedPercentsTotal = 0;
                      approvedPercentsTotal = (dataItem.total / (dataItem.approved + dataItem.return + dataItem.canceled) * 100).toFixed(1);
                      return dataItem.approvedPercentsTotal = `${approvedPercentsTotal}%`;
                    });
                    return (
                      <div key={dataIndex}>
                        <h4 className="text-center">
                          {offer ? offer.title : 'Оффер не найден'}
                        </h4>
                        <ReactTable
                          data={dataItem}
                          columns={columns}
                          minRows={7}
                          nextText="Далее"
                          loadingText="Загрузка..."
                          previousText="Назад"
                          pageText="Страница"
                          ofText="из"
                          className="-highlight -striped"
                          noDataText="Строка не найдена"
                          rowsText="строк"
                          TfootComponent={TfootComponent}
                        />
                      </div>
                    );
                  })
                }
              </div>
            ) : null
          }
        </div>
      </Loading>
    )
  }
}

export default ConversionChart