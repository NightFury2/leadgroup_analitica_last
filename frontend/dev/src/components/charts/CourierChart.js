import React from 'react'
import { Table, Col } from 'reactstrap'

const CourierChart = (props) => {

  // Захуярь сюда еще тултип
  return (
    <Col xs="12" sm="12" md="12" lg="12">
      <Table responsive size="sm" bordered>
        <thead>
        <tr>
          <th></th>
          <th>Покрытие</th>
          <th>Доля</th>
          <th>Лиды</th>
          <th>Выкуп</th>
          <th>Средний чек</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Почта 1</td>
          <td>31%</td>
          <td>27%</td>
          <td>1250</td>
          <td>26%</td>
          <td>60 $</td>
          <td>12 000 $</td>
        </tr>
        {props.children}
        </tbody>
      </Table>
    </Col>
  )
}

export default CourierChart
