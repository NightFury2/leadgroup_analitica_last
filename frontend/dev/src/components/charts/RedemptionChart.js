import React, { Component } from 'react'
import { Col, Button } from 'reactstrap'
import Loading from 'react-loading-spinner'
import ReactDOMServer from 'react-dom/server'
import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import ReactTable from 'react-table'

class RedemptionChart extends Component {
  constructor(props) {
    super(props)
    this.exportData = this.exportData.bind(this)
    this.options = {
      defaultSortName: 'date',  // default sort column name
      defaultSortOrder: 'asc',  // default sort order
    }
  }
  exportData() {
    const { data } = this.props
    // Экспортируем совсем другую таблицу, ибо в апи ReactTable нет выгрузки.
    const TableToExport = () => (
      <BootstrapTable
        tableStyle={{fontSize: '12px'}}
        data={data}
        striped
        options={this.options}
        tableContainerClass="redemption-stats-data"
      >
        <TableHeaderColumn row="0" colSpan="2" dataField='date' dataSort>Дата</TableHeaderColumn>

        <TableHeaderColumn row="1" colSpan="2" isKey tdAttr={{'colSpan': '2'}} dataField='date' dataSort>ГГГГ/ММ/ДД</TableHeaderColumn>

        <TableHeaderColumn row="0" dataSort>Всего</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='total' dataSort>Кол-во</TableHeaderColumn>

        <TableHeaderColumn row="0" colSpan="2" dataField='paid' dataSort>Оплачено</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='paid' dataSort>Кол-во</TableHeaderColumn>
        <TableHeaderColumn row="1" dataField='paidPercents' dataSort>%</TableHeaderColumn>

        <TableHeaderColumn row="0" colSpan="2" dataField='return' dataSort>Возвратов</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='return' dataSort>Кол-во</TableHeaderColumn>
        <TableHeaderColumn row="1" dataField='returnPercents' dataSort>%</TableHeaderColumn>

        <TableHeaderColumn row="0" colSpan="2" dataField='processed' dataSort>В обработке</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='processed' dataSort>Кол-во</TableHeaderColumn>
        <TableHeaderColumn row="1" dataField='processedPercents' dataSort>%</TableHeaderColumn>

        <TableHeaderColumn row="0" dataField='approvedPercents' dataSort>Апрув</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='approvedPercents' dataSort>%</TableHeaderColumn>
      </BootstrapTable>
    )
    let percentReplacer = /%<\/td/g
    let dollarReplacer = /\$/g
    let stringedTable = ReactDOMServer.renderToStaticMarkup(<TableToExport/>)
    let replacedStringedTable = stringedTable.toString().replace(percentReplacer, '</td')
    replacedStringedTable = replacedStringedTable.toString().replace(dollarReplacer, '')
    let container = document.createElement('div')
    container.innerHTML = replacedStringedTable
    let wb = XLSX.utils.table_to_book(container)
    Object.keys(wb.Sheets).map( sheet => {
      Object.keys(wb.Sheets[sheet]).map( cell => {
        if (cell !== '!merges' && cell !== '!ref') {
          wb.Sheets[sheet][cell].s = {
            font: {
              color: {
                sz: '25'
              }
            },
            border: {
              left: {
                style: 'medium',
                auto: '1'
              }
            }
          }
        } else {
          console.log(`else triggered. cell is: ${cell}`)
        }
        console.log(wb.Sheets[sheet][cell])
      } )
    } )
    let wopts = { bookType:'xlsx', bookSST: false, type: 'binary', cellDates: false, cellStyles: true };
    let wbout = XLSX.write(wb, wopts);
    const s2ab = (s) => {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i);
      return buf;
    }
    saveAs(
      new Blob(
        [s2ab(wbout)],
        {type:"application/octet-stream"}
      ),
      `leadgroup-operators-stats-${new Date(Date.now()).getFullYear()}-${new Date(Date.now()).getMonth() + 1}-${new Date(Date.now()).getDate()}.xlsx`
    )
  }
  render() {
    const { data, isLoading, toPercents, parseSum } = this.props
    const total = values => {
      let param = values.column.id
      let total = 0
      values.data.map(col => {
        return total = total + col[param]
      })
      return total
    }
    const percents = values => {
      let total = 0
      let current = 0
      let param = values.column.id
      let percents
      values.data.map(col => {
        return (
          current = current + col[param.toString().split('P')[0]],
            total = total + col['total']
        )
      })
      return percents = toPercents(current, total)
    }
    const average_bill = (values, currency) => {
      let total = 0
      let average = 0
      let cols = Object.keys(values.data).length
      let param = values.column.id
      values.data.map(col => {
        return total = total + Number(col[param])
      })
      return average = `${(total / cols).toFixed(1)} ${currency}`
    }
    const columns = [
      {
        Header: 'Дата',
        columns: [{
          Header: 'ГГГГ/ММ/ДД',
          accessor: 'date',
          Footer: 'Итого'
        }]
      },
      {
        Header: 'Всего',
        columns: [{
          Header: 'Кол-во',
          accessor: 'total',
          Footer: values => total(values)
        }]
      },
      {
        Header: 'Оплачено',
        columns: [{
          Header: 'Кол-во',
          accessor: 'paid',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'paidPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Возвратов',
        columns: [{
          Header: 'Кол-во',
          accessor: 'return',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'returnPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'В процессе',
        columns: [{
          Header: 'Кол-во',
          accessor: 'processed',
          Footer: values => total(values)
        },{
          Header: '%',
          accessor: 'processedPercents',
          Footer: values => percents(values)
        }]
      },
      {
        Header: 'Апрув',
        columns: [{
          Header: '%',
          accessor: 'approvedPercents',
          Footer: values => percents(values)
        }]
      },
    ]
    return (
      <Col xs="12" sm="12" md="12" lg="12">
        <Loading
          isLoading={isLoading}
          loadingClassName='loading-md'
        >
          <div>
            {
              data ?
                <Button
                  size="sm"
                  onClick={this.exportData}
                >
                  Экспорт в Excel
                </Button>
                : null
            }
            <div className="pad-a-bit" />
            <ReactTable
              data={data}
              columns={columns}
              className="redemption-stats-data"
            />
          </div>
        </Loading>
      </Col>
    )
  }
}

export default RedemptionChart
