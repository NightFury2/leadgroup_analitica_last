import React, { PureComponent } from 'react';
import { Table } from 'reactstrap';
import Dimensions from 'react-dimensions';
import { AutoSizer } from 'react-virtualized';
import TimeItem from '../../components/common/TimeItem';
import Loading from 'react-loading-spinner';
import { Chart } from 'react-google-charts'
import _ from 'lodash'

class ActivityChart extends PureComponent {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false
    }
  }
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    })
  }
  // renderGraphic(date) {
  //   const timeContainer = [{"00": []}, {"01": []}, {"02": []}, {"03": []}, {"04": []}, {"05": []}, {"06": []}, "07": [], "08": [], "09": [], "10": [], "11": [], "12": [], "13": [], "14": [], "15": [], "16": [], "17": [], "18": [], "19": [], "20": [], "21": [], "22": [], "23": []];
  //   return _.reduce(date, (result, value, key) => {
  //     if (result[key.split(':')[0]]) {
		//     result[key.split(':')[0]].push(`${key}-${value}`);
  //     }
  //     return result;
  //   }, timeContainer)
  // };
  render() {
    const self = this;
    const { data, isLoading, operators } = this.props;
    let operatorName;
    let operatorId;
    return (
      <Loading
        isLoading={isLoading}
        loadingClassName='loading-lg'
      >
        {
          data ? (
            <div style={{minHeight: '200px'}}>
              {
                Object.keys(data).length > 0 ?
                  _.map(data, (item, index) => {
                    const operator = _.find(operators, {id: index});
                    return (
                      <div key={index}>
                        {/*TODO*/}
                         {/*<Chart*/}
                          {/*chartType="timeline"*/}
                          {/*data={[['Age', 'Weight'], [8, 12], [4, 5.5]]}*/}
                          {/*options={{}}*/}
                          {/*graph_id="ScatterChart"*/}
                          {/*width="100%"*/}
                          {/*height="400px"*/}
                          {/*legend_toggle*/}
                        {/*/>*/}
                        <h4><span style={{opacity: '0.6'}}>Оператор</span> {`${operator !== undefined ? operator.login : 'login'} (${operator !== undefined ? operator.name : 'name'})`}</h4>
                        <Table id={`activity-data-${operator !== undefined ? operator.id : index}`} className="activity" responsive striped bordered>
                          <thead>
                            <tr style={{textAlign: 'center'}}>
                              <th className="text-center">Дата:</th>
                              <th className="text-center">00</th>
                              <th className="text-center">01</th>
                              <th className="text-center">02</th>
                              <th className="text-center">03</th>
                              <th className="text-center">04</th>
                              <th className="text-center">05</th>
                              <th className="text-center">06</th>
                              <th className="text-center">07</th>
                              <th className="text-center">08</th>
                              <th className="text-center">09</th>
                              <th className="text-center">10</th>
                              <th className="text-center">11</th>
                              <th className="text-center">12</th>
                              <th className="text-center">13</th>
                              <th className="text-center">14</th>
                              <th className="text-center">15</th>
                              <th className="text-center">16</th>
                              <th className="text-center">17</th>
                              <th className="text-center">18</th>
                              <th className="text-center">19</th>
                              <th className="text-center">20</th>
                              <th className="text-center">21</th>
                              <th className="text-center">22</th>
                              <th className="text-center">23</th>
                            </tr>
                          </thead>
                          <tbody>

                          {/*{*/}
                            {/*_.map(item, (date, dateIndex) => {*/}
                              {/*let graphic = this.renderGraphic(date);*/}
                              {/*console.log(graphic);*/}
                              {/*return (*/}
                                {/*<tr style={{fontSize: '10px', textAlign: 'center'}} key={dateIndex}>*/}
                                  {/*<th><span>{dateIndex}</span></th>*/}
                                  {/*{*/}
                                    {/*graphic.map((item, index) => {*/}
                                      {/*console.log(item, index);*/}
                                      {/*if (item.length > 0) {*/}
                                        {/*return (*/}
                                          {/*<th>*/}
                                            {/*{*/}
                                              {/*// TODO*/}
                                              {/*_.map(item, (time, indexTIme) => {*/}
                                                {/*return (*/}
                                                  {/*<TimeItem*/}

                                                  {/*/>*/}
                                                {/*)*/}
                                              {/*})*/}
                                            {/*}*/}
                                          {/*</th>*/}
                                        {/*)*/}
                                      {/*} else {*/}
                                        {/*return (*/}
                                          {/*<th>*/}
                                            {/*<span style={{backgroundColor: '#CCC', cursor: 'pointer', padding: '0 1px'}} />*/}
                                          {/*</th>*/}
                                        {/*)*/}
                                      {/*}*/}
                                    {/*})*/}
                                  {/*}*/}
                                {/*</tr>*/}
                              {/*);*/}
                            {/*})*/}
                          {/*}*/}
                          </tbody>
                        </Table>
                      </div>
                    );
                  })
                  // _.map(data, (item, index) => {
                  //   const operator = _.find(operators, {id: index});
                  //   return (
                  //     <div>
                  //       <h4><span style={{opacity: '0.6'}}>Оператор</span> {`${operator.login} (${operator.name})`}</h4>
                  //       <Table id={`activity-data-${operator.id}`} className="activity" responsive striped bordered>
                  //         <thead>
                  //         <tr style={{textAlign: 'center'}}>
                  //           <th className="text-center">Дата:</th>
                  //           <th className="text-center">00</th>
                  //           <th className="text-center">01</th>
                  //           <th className="text-center">02</th>
                  //           <th className="text-center">03</th>
                  //           <th className="text-center">04</th>
                  //           <th className="text-center">05</th>
                  //           <th className="text-center">06</th>
                  //           <th className="text-center">07</th>
                  //           <th className="text-center">08</th>
                  //           <th className="text-center">09</th>
                  //           <th className="text-center">10</th>
                  //           <th className="text-center">11</th>
                  //           <th className="text-center">12</th>
                  //           <th className="text-center">13</th>
                  //           <th className="text-center">14</th>
                  //           <th className="text-center">15</th>
                  //           <th className="text-center">16</th>
                  //           <th className="text-center">17</th>
                  //           <th className="text-center">18</th>
                  //           <th className="text-center">19</th>
                  //           <th className="text-center">20</th>
                  //           <th className="text-center">21</th>
                  //           <th className="text-center">22</th>
                  //           <th className="text-center">23</th>
                  //         </tr>
                  //         </thead>
                  //         <tbody>
                  //           {
                  //             _.map(item, (date, dateIndex) => {
                  //               return (
                  //                 <tr style={{fontSize: '10px', textAlign: 'center'}} key={dateIndex}>
                  //                   <th><span>{dateIndex}</span></th>
                  //                   {
                  //                     _.map(date, (time, timeIndex) => {
                  //                       console.log(time, timeIndex);
                  //                     })
                  //                   }
                  //                 </tr>
                  //               );
                  //             })
                  //           }
                  //         </tbody>
                  //       </Table>
                  //     </div>
                  //   );
                  // })
                  ///////////
                  // Object.keys(data).map( (item, operatorIndex) => {
                  //   const operator = _.find(operators, {id: item});
                  //   return (
                  //     <div>
                  //       <h4><span style={{opacity: '0.6'}}>Оператор</span> {`${operator.login} (${operator.name})`}</h4>
                  //       <Table id={`activity-data-${operator.id}`} className="activity" responsive striped bordered key={operatorIndex}>
                  //         <thead>
                  //         <tr style={{textAlign: 'center'}}>
                  //           <th className="text-center">Дата:</th>
                  //           <th className="text-center">00</th>
                  //           <th className="text-center">01</th>
                  //           <th className="text-center">02</th>
                  //           <th className="text-center">03</th>
                  //           <th className="text-center">04</th>
                  //           <th className="text-center">05</th>
                  //           <th className="text-center">06</th>
                  //           <th className="text-center">07</th>
                  //           <th className="text-center">08</th>
                  //           <th className="text-center">09</th>
                  //           <th className="text-center">10</th>
                  //           <th className="text-center">11</th>
                  //           <th className="text-center">12</th>
                  //           <th className="text-center">13</th>
                  //           <th className="text-center">14</th>
                  //           <th className="text-center">15</th>
                  //           <th className="text-center">16</th>
                  //           <th className="text-center">17</th>
                  //           <th className="text-center">18</th>
                  //           <th className="text-center">19</th>
                  //           <th className="text-center">20</th>
                  //           <th className="text-center">21</th>
                  //           <th className="text-center">22</th>
                  //           <th className="text-center">23</th>
                  //         </tr>
                  //         </thead>
                  //         <tbody>
                  //         {
                  //           Object.keys(data[item]).map((element, dateIndex) => {
                  //             console.log(element);
                  //             return (
                  //               <tr style={{fontSize: '10px', textAlign: 'center'}} key={dateIndex}>
                  //                 <th className="text-center">{element}</th>
                  //               </tr>
                  //             );
                  //           })
                  //         }
                  //         <tr style={{fontSize: '10px', textAlign: 'center'}}>
                  //
                  //         </tr>
                  //          {/*{*/}
                  //           {/*Object.keys(data[item]).map((element, dateIndex) => {*/}
                  //             {/*const timeContainer = {*/}
                  //               {/*"00": [],*/}
                  //               {/*"01": [],*/}
                  //               {/*"02": [],*/}
                  //               {/*"03": [],*/}
                  //               {/*"04": [],*/}
                  //               {/*"05": [],*/}
                  //               {/*"06": [],*/}
                  //               {/*"07": [],*/}
                  //               {/*"08": [],*/}
                  //               {/*"09": [],*/}
                  //               {/*"10": [],*/}
                  //               {/*"11": [],*/}
                  //               {/*"12": [],*/}
                  //               {/*"13": [],*/}
                  //               {/*"14": [],*/}
                  //               {/*"15": [],*/}
                  //               {/*"16": [],*/}
                  //               {/*"17": [],*/}
                  //               {/*"18": [],*/}
                  //               {/*"19": [],*/}
                  //               {/*"20": [],*/}
                  //               {/*"21": [],*/}
                  //               {/*"22": [],*/}
                  //               {/*"23": [],*/}
                  //             {/*}*/}
                  //             {/*return (*/}
                  //               {/*<tr key={dateIndex} style={{fontSize: '10px', textAlign: 'center'}}>*/}
                  //                 {/*{*/}
                  //                   {/*// Кто-нибудь, перепишите это!*/}
                  //                   {/*Object.keys(data[item][element]).map((time) => {*/}
                  //                     {/*if (time.split(':')[0] == '00') {*/}
                  //                       {/*timeContainer['00'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '01') {*/}
                  //                       {/*timeContainer['01'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '02') {*/}
                  //                       {/*timeContainer['02'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '03') {*/}
                  //                       {/*timeContainer['03'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '04') {*/}
                  //                       {/*timeContainer['04'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '05') {*/}
                  //                       {/*timeContainer['05'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '06') {*/}
                  //                       {/*timeContainer['06'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '07') {*/}
                  //                       {/*timeContainer['07'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '08') {*/}
                  //                       {/*timeContainer['08'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '09') {*/}
                  //                       {/*timeContainer['09'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '10') {*/}
                  //                       {/*timeContainer['10'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '11') {*/}
                  //                       {/*timeContainer['11'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '12') {*/}
                  //                       {/*timeContainer['12'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '13') {*/}
                  //                       {/*timeContainer['13'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '14') {*/}
                  //                       {/*timeContainer['14'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '15') {*/}
                  //                       {/*timeContainer['15'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '16') {*/}
                  //                       {/*timeContainer['16'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '17') {*/}
                  //                       {/*timeContainer['17'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '18') {*/}
                  //                       {/*timeContainer['18'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '19') {*/}
                  //                       {/*timeContainer['19'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '20') {*/}
                  //                       {/*timeContainer['20'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '21') {*/}
                  //                       {/*timeContainer['21'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '22') {*/}
                  //                       {/*timeContainer['22'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*} else if (time.split(':')[0] == '23') {*/}
                  //                       {/*timeContainer['23'].push(`${time}-${data[item][element][time]}`)*/}
                  //                     {/*}*/}
                  //                   {/*})*/}
                  //                 {/*}*/}
                  //                 {/*<td>*/}
                  //                   {/*{element}*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*// И это.*/}
                  //                 {/*timeContainer['00'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['01'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['02'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['03'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['04'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['05'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['06'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['07'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['08'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['09'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['10'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['11'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['12'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['13'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['14'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['15'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['16'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['17'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['18'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['19'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['20'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['21'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['22'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //                 {/*<td>*/}
                  //             {/*<span>*/}
                  //               {/*{*/}
                  //                 {/*timeContainer['23'].map((timeItem, timeIndex) => {*/}
                  //                   {/*return (*/}
                  //                     {/*<TimeItem*/}
                  //                       {/*MapKey={timeIndex}*/}
                  //                       {/*id={`s${item}-${element}-${timeItem.split(':')[0]}-${timeItem.split(':')[1]}`}*/}
                  //                       {/*isOpen={self.state.tooltipOpen}*/}
                  //                       {/*placeholder={`${timeItem}`}*/}
                  //                     {/*/>*/}
                  //                   {/*)*/}
                  //                 {/*})*/}
                  //               {/*}*/}
                  //             {/*</span>*/}
                  //                 {/*</td>*/}
                  //               {/*</tr>*/}
                  //             {/*)*/}
                  //           {/*})*/}
                  //         {/*}*/}
                  //         </tbody>
                  //       </Table>
                  //     </div>
                  //   )
                  // })
                  : null
              }
            </div>
          ) : null
        }
      </Loading>
    )
  }
}

export default Dimensions()(ActivityChart)