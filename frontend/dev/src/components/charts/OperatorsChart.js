import React, { Component } from 'react'
import { Col, Button } from 'reactstrap'
import Loading from 'react-loading-spinner'
import ReactDOMServer from 'react-dom/server'
import XLSX from 'xlsx'
import { saveAs } from 'file-saver'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import ReactTable from 'react-table'

class OperatorsChart extends Component {
  constructor(props) {
    super(props)
    this.exportData = this.exportData.bind(this)
    this.options = {
      defaultSortName: 'operatorName',  // default sort column name
      defaultSortOrder: 'asc',  // default sort order
    }
  }
  exportData() {
    const { data } = this.props
    // Экспортируем совсем другую таблицу, ибо в апи ReactTable нет выгрузки.
    const TableToExport = () => (
      <BootstrapTable
        tableStyle={{fontSize: '12px'}}
        data={data}
        striped
        options={this.options}
        tableContainerClass="operators-stats-data"
      >
        <TableHeaderColumn row="0" colSpan="2" dataField='operatorName' dataSort>Оператор</TableHeaderColumn>

        <TableHeaderColumn row="1" colSpan="2" isKey tdAttr={{'colSpan': '2'}} dataField='operatorName' dataSort>Имя оператора</TableHeaderColumn>

        <TableHeaderColumn row="0" dataSort>Обработано лидов</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='total' dataSort>Кол-во</TableHeaderColumn>

        <TableHeaderColumn row="0" colSpan="2" dataField='approved' dataSort>Принято заказов</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='approved' dataSort>Кол-во</TableHeaderColumn>
        <TableHeaderColumn row="1" dataField='approvedPercents' dataSort>%</TableHeaderColumn>

        <TableHeaderColumn row="0" dataField='average_bill_usd' dataSort>Средний чек</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='average_bill_usd' dataSort>USD</TableHeaderColumn>

        <TableHeaderColumn row="0" colSpan="2" dataField='buyout' dataSort>Выкуп</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='buyout' dataSort>Кол-во</TableHeaderColumn>
        <TableHeaderColumn row="1" dataField='buyoutPercents' dataSort>%</TableHeaderColumn>

        <TableHeaderColumn className={'approvedSummary'} row="0" dataField='approvedSummary' dataSort>Принято на сумму</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='approvedSummary' dataSort>USD</TableHeaderColumn>

        <TableHeaderColumn className={'buyoutSummary'} row="0" dataField='buyoutSummary' dataSort>Выкуплено на сумму</TableHeaderColumn>

        <TableHeaderColumn row="1" dataField='buyoutSummary' dataSort>USD</TableHeaderColumn>
      </BootstrapTable>
    )
    let percentReplacer = /%<\/td/g
    let dollarReplacer = /\$/g
    let stringedTable = ReactDOMServer.renderToStaticMarkup(<TableToExport/>)
    let replacedStringedTable = stringedTable.toString().replace(percentReplacer, '</td')
    replacedStringedTable = replacedStringedTable.toString().replace(dollarReplacer, '')
    let container = document.createElement('div')
    container.innerHTML = replacedStringedTable
    let wb = XLSX.utils.table_to_book(container)
    Object.keys(wb.Sheets).map( sheet => {
      Object.keys(wb.Sheets[sheet]).map( cell => {
        if (cell !== '!merges' && cell !== '!ref') {
          wb.Sheets[sheet][cell].s = {
            font: {
              color: {
                sz: '25'
              }
            },
            border: {
              left: {
                style: 'medium',
                auto: '1'
              }
            }
          }
        } else {
          console.log(`else triggered. cell is: ${cell}`)
        }
        console.log(wb.Sheets[sheet][cell])
      } )
    } )
    let wopts = { bookType:'xlsx', bookSST: false, type: 'binary', cellDates: false, cellStyles: true };
    let wbout = XLSX.write(wb, wopts);
    const s2ab = (s) => {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i);
      return buf;
    }
    saveAs(
      new Blob(
        [s2ab(wbout)],
        {type:"application/octet-stream"}
      ),
      `leadgroup-operators-stats-${new Date(Date.now()).getFullYear()}-${new Date(Date.now()).getMonth() + 1}-${new Date(Date.now()).getDate()}.xlsx`
    )
  }
  render() {
    const { isLoading, data } = this.props
    const columns = [
      {
        Header: 'Оператор',
        columns: [{
          Header: 'Имя оператора',
          accessor: 'operatorName',
          minWidth: 250,
        }],
      },
      {
        Header: 'Обработано',
        columns: [{
          Header: 'Кол-во',
          accessor: 'total'
        }]
      },
      {
        Header: 'Принято',
        columns: [{
          Header: 'Кол-во',
          accessor: 'approved'
        },{
          Header: '%',
          accessor: 'approvedPercents'
        }]
      },
      {
        Header: 'Средний чек',
        columns: [{
          Header: 'USD',
          accessor: 'average_bill_usd'
        }]
      },
      {
        Header: 'Выкуп',
        columns: [{
          Header: 'Кол-во',
          accessor: 'buyout'
        },{
          Header: '%',
          accessor: 'buyoutPercents'
        }]
      },
      {
        Header: 'Принято на сумму',
        columns: [{
          Header: 'USD',
          accessor: 'approvedSummary'
        }]
      },
      {
        Header: 'Выкуплено на сумму',
        columns: [{
          Header: 'USD',
          accessor: 'buyoutSummary'
        }]
      }
    ]
    return (
      <Col xs="12" sm="12" md="12" lg="12">
        <Loading
          isLoading={isLoading}
          loadingClassName='loading-md'
        >
          <div>
            {
              data ?
                <Button
                  size="sm"
                  onClick={this.exportData}
                >
                  Экспорт в Excel
                </Button>
                : null
            }
            <div className="pad-a-bit" />
            <ReactTable
              data={data}
              columns={columns}
            />
          </div>
        </Loading>
      </Col>
    )
  }
}

export default OperatorsChart
