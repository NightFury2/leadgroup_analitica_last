import React from 'react'
import { Table, Col } from 'reactstrap'

const CityChart = (props) => {

  // Захуярь сюда еще тултип
  return (
    <Col xs="12" sm="12" md="12" lg="12">
      <Table responsive size="sm" bordered>
        <thead>
        <tr>
          <th>Город</th>
          <th>Доля продаж</th>
          <th>Почта 1</th>
          <th>Почта 2</th>
          <th>Почта 3</th>
          <th>Почта 4</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Город 1</td>
          <td>10%</td>
          <td>27%</td>
          <td>30%</td>
          <td>26%</td>
          <td>17%</td>
          <td>12 000 $</td>
        </tr>
        {props.children}
        </tbody>
      </Table>
    </Col>
  )
}

export default CityChart
