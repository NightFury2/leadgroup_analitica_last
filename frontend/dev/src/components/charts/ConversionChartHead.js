import React from 'react'
import { Table } from 'reactstrap'

const ConversionChartHead = (props) => (
  <thead style={{fontSize: '12px'}}>
    <tr>
      <th colSpan={2}>Дата</th>
      <th>Всего заказов</th>
      <th
        colSpan={2}
        style={{backgroundColor: 'rgba(127, 131, 132, 0.4)'}}
      >
        В обработке
      </th>
      <th colSpan={2} style={{backgroundColor: 'rgba(48, 112, 143, 0.4)'}}>
        Принято
      </th>
      <th colSpan={2} style={{backgroundColor: '#f2dede'}}>Отмен</th>
      <th colSpan={2} style={{backgroundColor: 'rgba(169, 68, 66, 0.4)'}}>
        Спам/ошибки/дубль</th>
      <th colSpan={2} style={{backgroundColor: 'rgba(162, 102, 252, 0.4)'}}>
        Возвратов</th>
      <th colSpan={2} style={{backgroundColor: '#dff0d8'}}>Средний чек</th>
      <th colSpan={2} style={{backgroundColor: '#bcdff1'}}>Оплачено</th>
      <th style={{backgroundColor: 'rgba(60, 118, 61, 0.4)'}}>Апрув</th>
    </tr>
  <tr>
    <th colSpan={2}>
      ГГГГ/ММ/ДД
    </th>
    <th>
      Кол-во
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      USD
    </th>
    <th>
      {props.currency}
    </th>
    <th>
      Кол-во
    </th>
    <th>
      %
    </th>
    <th>
      %
    </th>
  </tr>
  </thead>
)

export default ConversionChartHead