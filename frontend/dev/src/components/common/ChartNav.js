import React, { Component } from 'react'
import { Tab, Nav, NavItem, NavLink, TabPane, Col, Row, TabContent, Card, Button, CardTitle, CardText } from 'reactstrap'
import classnames from 'classnames'

class ChartNav extends Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }
  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' }) + ' pointer'}
              onClick={() => { this.toggle('1'); }}
            >
              Общая по стране
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' }) + ' pointer'}
              onClick={() => { this.toggle('2'); }}
            >
              По офферам
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Col style={{display: this.state.activeTab === '1' ? 'block' : 'none'}}>
              {this.props.basicStats}
              {/*{*/}
                {/*this.state.activeTab == 1 ? this.props.basicStats : null*/}
              {/*}*/}
            </Col>
          </TabPane>
          <TabPane tabId="2">
            <Col style={{display: this.state.activeTab === '2' ? 'block' : 'none'}}>
              {
                this.props.offerStats
                // this.state.activeTab == 2 ? this.props.offerStats : null
              }
            </Col>
          </TabPane>
        </TabContent>
      </div>
    )
  }
}

export default ChartNav