import React, { Component } from 'react'
import { Card, CardTitle, CardBlock, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Icon } from 'react-fa'
import { connect } from 'react-redux'
import { countryListRequest, offerListRequest, operatorsListRequest, currencyListRequest } from '../../actions/filters'

const DashboardLinkCol = (props) => (
  <Col xs="12" sm="6" md="4" lg="3">
    <Link className="dashboard-link" to={props.to}>
      <Card className="text-center dashboard-item">
        {props.children}
      </Card>
    </Link>
  </Col>
)

class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.handleDate = this.handleDate.bind(this)
  }
  handleDate() {
    let timeLocale = moment().locale('ru')
    let date = timeLocale.format(' DD MMMM[, ]dddd')
    return date
  }
  componentDidMount() {
    const { loggedIn, loadCountries, loadOffers, loadOperators, loadCurrencySymbols, token } = this.props
    if (loggedIn && token) {
      loadCountries(token)
      loadOffers(token)
      loadOperators(token)
      loadCurrencySymbols(token)
    }
  }
  render() {
    const dashboard = (
      <CardBlock>
        <Row>
          <DashboardLinkCol to="/conversion">
            <CardBlock><Icon name="bar-chart fa-5x"/></CardBlock>
            <CardTitle>Статистика по конверсии</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/operators">
            <CardBlock><Icon name="group fa-5x"/></CardBlock>
            <CardTitle>Статистика по операторам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/redemption">
            <CardBlock><Icon name="shekel fa-5x"/></CardBlock>
            <CardTitle>Статистика по выкупаемости</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/activity">
            <CardBlock><Icon name="industry fa-5x"/></CardBlock>
            <CardTitle>Активность сотрудников</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/products">
            <CardBlock><Icon name="product-hunt fa-5x"/></CardBlock>
            <CardTitle>Статистика по товарам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/city">
            <CardBlock><Icon name="braille fa-5x"/></CardBlock>
            <CardTitle>Статистика по городам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/courier">
            <CardBlock><Icon name="taxi fa-5x"/></CardBlock>
            <CardTitle>Статистика по курьерам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/offers">
            <CardBlock><Icon name="level-up fa-5x"/></CardBlock>
            <CardTitle>Ключевые показатели</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/leadprofit">
            <CardBlock><Icon name="exchange fa-5x"/></CardBlock>
            <CardTitle>Статистика по лидам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/order">
            <CardBlock><Icon name="area-chart fa-5x"/></CardBlock>
            <CardTitle>Статистика по заказам</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/analyzer">
            <CardBlock><Icon name="calculator fa-5x"/></CardBlock>
            <CardTitle>Онлайн анализатор</CardTitle>
          </DashboardLinkCol>
          <DashboardLinkCol to="/price-dependency">
            <CardBlock><Icon name="line-chart fa-5x"/></CardBlock>
            <CardTitle>Зависимость цены</CardTitle>
          </DashboardLinkCol>
        </Row>
      </CardBlock>
    )
    return (
      <div className="animated fadeIn">
        <Card block>
          <CardTitle>
            <Icon name="calendar-o" />
            {this.handleDate()}
            <hr />
          </CardTitle>
          {dashboard}
        </Card>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    token: state.handleLogin.accessToken
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loadCountries: (token) => dispatch(countryListRequest(token)),
    loadOffers: (token) => dispatch(offerListRequest(token)),
    loadOperators: (token) => dispatch(operatorsListRequest(token)),
    loadCurrencySymbols: (token) => dispatch(currencyListRequest(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
