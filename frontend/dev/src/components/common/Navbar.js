import React from 'react'
import { Navbar } from 'reactstrap'

const BsNavbar = (props) => (
  <Navbar fluid>
    {props.children}
  </Navbar>
)

export default BsNavbar