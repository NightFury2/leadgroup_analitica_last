import React from 'react'
import { Nav, NavItem } from 'reactstrap'
import RouterLink from './RouterLink'

const BsNav = props => (
  <Nav className="sidebar-menu">
    <NavItem className="brand text-center" disabled><b>Lead</b>Group</NavItem>
    <RouterLink to="/conversion" className="nav-link">Статистика конверсии</RouterLink>
    <RouterLink to="/operator-efficiency" className="nav-link">Статистика эффективности операторов</RouterLink>
    <RouterLink to="/activity" className="nav-link">Статистика по активности сотрудников</RouterLink>
    <RouterLink to="/redemption" className="nav-link">Статистика по выкупаемости</RouterLink>
    <RouterLink to="/products" className="nav-link">Статистика по товарам</RouterLink>
    <RouterLink to="/city" className="nav-link">Статистика по городам</RouterLink>
    <RouterLink to="/courier" className="nav-link">Статистика по курьерам</RouterLink>
    <RouterLink to="/offers" className="nav-link">Статистика по ключевым показателям</RouterLink>
    <RouterLink to="/operators" className="nav-link">Статистика по операторам</RouterLink>
    <RouterLink to="/leadprofit" className="nav-link">Статистика по лидам</RouterLink>
    <RouterLink to="/order" className="nav-link">Статистика по заказам</RouterLink>
    <RouterLink to="/analyzer" className="nav-link">Онлайн-анализатор</RouterLink>
  </Nav>
);

export default BsNav