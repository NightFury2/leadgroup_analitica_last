import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { Icon } from 'react-fa'

class RouterLink extends Component {
  constructor(props) {
    super(props)
  }
  // Сломался переключатель активных менюшек
  render() {
    return (
      <li className="nav-item">
        <Link
          onClick={this.props.onClick}
          className={'nav-link ' + this.props.className}
          to={this.props.to}
        >
          <Icon name={this.props.icon} />
          {this.props.children}
        </Link>
      </li>
    )
  }
}

export default RouterLink