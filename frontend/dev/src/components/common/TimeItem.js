import React, { PureComponent } from 'react'
import { Tooltip } from 'reactstrap'

class TimeItem extends PureComponent {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false
    }
  }
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    })
  }
  render() {
    const { MapKey, id, placeholder } = this.props
    const defineProgressState = (count) => {
      if (count > 0 && count < 10) {
        return "#a9100f"
      } else if (count > 9 && count < 20) {
        return "#e6db74"
      } else if (count > 19) {
        return "#3c763d"
      } else {
        return "#CCC"
      }
    }
    return (
      <span key={MapKey}>
        <span
          id={id}
          style={{backgroundColor: defineProgressState(placeholder.split('-')[1]), cursor: 'pointer', padding: '0 1px'}}
        >
          &nbsp;
        </span>
        <Tooltip
          placement="bottom"
          isOpen={this.state.tooltipOpen}
          target={id}
          toggle={this.toggle}
          delay={0}
        >
          Время: {placeholder.split('-')[0]}
          <br />
          Действий: {placeholder.split('-')[1]}
        </Tooltip>
      </span>
    )
  }
}

export default TimeItem
