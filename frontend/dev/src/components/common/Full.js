import React, { Component } from 'react';
import Header from './Header';
import Sidebar from '../../containers/common/Sidebar';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <main className="main container-fluid">
            {this.props.children}
          </main>
          <Sidebar {...this.props}/>
        </div>
      </div>
    );
  }
}

export default Full;
