import React from 'react'
import { Container, Row } from 'reactstrap'

const BsRow = props => (
  <Row className="clearfix">
    {props.children}
  </Row>
);
export default BsRow