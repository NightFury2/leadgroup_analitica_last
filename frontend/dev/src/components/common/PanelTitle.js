import React from 'react'

const PanelTitle = (props) => (
  <h1 className="content-header">
    {props.children}
  </h1>
)

export default PanelTitle