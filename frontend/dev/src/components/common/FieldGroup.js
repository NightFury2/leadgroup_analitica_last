import React from 'react'
import { FormGroup, Label, Input } from 'reactstrap'

const FieldGroup = ({ props, id, label, help, onChange, type, placeholder }) => (
  <FormGroup>
    <Label for={id}>{label}</Label>
    <Input type={type} id={id} required onChange={onChange} placeholder={placeholder} />
  </FormGroup>
)

export default FieldGroup