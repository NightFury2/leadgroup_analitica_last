import React from 'react'
import { Jumbotron } from 'reactstrap'

const NotFound = props => (
  <Jumbotron>
    <h1>Not Found</h1>
    <p>Неверный адрес страницы.</p>
  </Jumbotron>
);


export default NotFound