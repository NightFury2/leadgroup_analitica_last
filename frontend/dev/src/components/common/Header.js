import React, { Component } from 'react';
import { connect } from 'react-redux'
import NavProfile from './NavProfile'
import logo from '../../styles/img/leadgroup.png'
import { Link } from 'react-router-dom'
import noImage from '../../styles/img/no-profile-pic-but-trust-me-i-m-cute.png'

class Header extends Component {
  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-compact');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    const { loggedIn, photo, firstName, lastName } = this.props
    const header = (
      <header
        className="app-header navbar"
      >
        <button
          className="navbar-toggler mobile-sidebar-toggler d-lg-none"
          onClick={this.mobileSidebarToggle}
          type="button"
        >
          &#9776;
        </button>
        <Link
          className="navbar-brand custom-brand d-lg-none"
          to="/home">
          <b>Lead</b>Group
        </Link>
        <ul
          className="nav navbar-nav d-md-down-none"
        >
          <li
            className="nav-item"
          >
            <Link
              className="navbar-brand custom-brand"
              to="/home"
            >
              <img
                className="logo"
                src={logo}
              />
            </Link>
          </li>
          <li
            className="nav-item"
          >
            <a
              className="nav-link navbar-toggler sidebar-toggler"
              id="sidebar-toggler-custom"
              onClick={this.sidebarMinimize}
              href="#"
            >
              &#9776;
            </a>
          </li>
        </ul>
        <ul
          className="nav navbar-nav d-md-down-none"
        >
          <li className="nav-item">
            <NavProfile
              firstName={firstName}
              lastName={lastName}
              photo={typeof photo === 'number' ? `/images/${photo}.png` : noImage}
            />
          </li>
        </ul>
      </header>
    )
    return (
      loggedIn
        ?
        header
        : null
    )
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.handleLogin.loggedIn,
    firstName: state.handleLogin.firstName,
    lastName: state.handleLogin.lastName,
    photo: state.handleLogin.photo
  }
}

export default connect(mapStateToProps)(Header)
