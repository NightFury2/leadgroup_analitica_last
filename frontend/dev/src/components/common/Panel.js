import React from 'react'
import { Card, CardBlock } from 'reactstrap'

const Panel = (props) => (
  <Card inverse={props.inverse} style={props.style} className={"animated fadeIn " + props.className}>
    <CardBlock>
      {props.children}
    </CardBlock>
  </Card>
)

export default Panel