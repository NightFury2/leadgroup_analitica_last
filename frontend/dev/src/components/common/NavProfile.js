import React from 'react';
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom'

class NavProfile extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <a onClick={this.toggle} className="nav-link dropdown-toggle nav-link profile" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
          <img src={this.props.photo} className="img-avatar" alt={this.props.firstName + ' ' + this.props.lastName}/>
          <span className="d-md-down-none">{this.props.firstName + '  ' + this.props.lastName}</span>
        </a>

        <DropdownMenu className="dropdown-menu-right">
          <DropdownItem header className="text-center">
            <strong> Настройки</strong>
          </DropdownItem>

          <Link to="/profile" className="profile-dropdown-link">
            <DropdownItem>
              <i className="fa fa-user"></i>
              Профиль
            </DropdownItem>
          </Link>
          <DropdownItem divider />
          <Link to="/logout" className="profile-dropdown-link">
            <DropdownItem>
              <i className="lock"></i>
              Выйти
            </DropdownItem>
          </Link>

        </DropdownMenu>
      </Dropdown>
    );
  }
}

export default NavProfile