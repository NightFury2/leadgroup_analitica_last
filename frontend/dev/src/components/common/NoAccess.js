import React from 'react'
import { Jumbotron, Container } from 'reactstrap'
import { Link } from 'react-router-dom'
import sadface from '../../styles/img/sadface.jpg';

const NoAccess = props => (
  <Jumbotron className="text-center">
    <Container>
      <h1>Нет доступа</h1>
      <div>
        <img src={sadface} />
      </div>
      <hr />
      <h4>Вы не авторизованы. <Link to="/">Авторизоваться</Link></h4>
    </Container>
  </Jumbotron>
);


export default NoAccess