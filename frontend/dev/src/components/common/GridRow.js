import React from 'react'
import { Container, Row } from 'reactstrap'

const GridRow = (props) => (
  <Container className={props.className} fluid>
    <Row>
      {props.children}
    </Row>
  </Container>
)

export default GridRow