import React, { Component } from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import { ButtonGroup, Button, Col } from 'reactstrap'
import Panel from '../common/Panel'
import {
  dateRangeStart,
  dateRangeEnd,
  dateRangeCustom
} from '../../actions/filters'
import { connect } from 'react-redux'

class DateRangePicker extends Component {
  constructor(props) {
    super(props)
    this.handleEnd = this.handleEnd.bind(this)
    this.handleStart = this.handleStart.bind(this)
    this.handleDateYesterday = this.handleDateYesterday.bind(this)
    this.handleDateWeek = this.handleDateWeek.bind(this)
    this.handleDateMonth = this.handleDateMonth.bind(this)
    this.handleDateLastMonth = this.handleDateLastMonth.bind(this)
    this.state = {
      startDate: moment().subtract(6, 'days'),
      endDate: moment()
    }
  }
  handleStart(date) {
    this.setState({
      startDate: date
    })
    const { dateRangeStart } = this.props
    const startDate = date.format('YYYY-MM-DD')
    dateRangeStart(startDate)
  }
  handleEnd(date) {
    this.setState({
      endDate: date
    })
    const { dateRangeEnd } = this.props
    const endDate = date.format('YYYY-MM-DD')
    dateRangeEnd(endDate)
  }
  handleDateYesterday() {
    this.setState({
      startDate: moment().subtract(1, 'days'),
      endDate: moment().subtract(1, 'days')
    })
    const { dateRangeCustom } = this.props
    const startDate = moment().subtract(1, 'days').format('YYYY-MM-DD')
    const endDate = moment().subtract(1, 'days').format('YYYY-MM-DD')
    dateRangeCustom(startDate, endDate)
  }
  handleDateWeek() {
    this.setState({
      startDate: moment().subtract(6, 'days'),
      endDate: moment()
    })
    const { dateRangeCustom } = this.props
    const startDate = moment().subtract(6, 'days').format('YYYY-MM-DD')
    const endDate = moment().format('YYYY-MM-DD')
    dateRangeCustom(startDate, endDate)
  }
  handleDateMonth() {
    this.setState({
      startDate: moment().startOf('month'),
      endDate: moment().endOf('month')
    })
    const { dateRangeCustom } = this.props
    const startDate = moment().startOf('month').format('YYYY-MM-DD')
    const endDate = moment().endOf('month').format('YYYY-MM-DD')
    dateRangeCustom(startDate, endDate)
  }
  handleDateLastMonth() {
    this.setState({
      startDate: moment().subtract(1, 'month').startOf('month'),
      endDate: moment().subtract(1, 'month').endOf('month')
    })
    const { dateRangeCustom } = this.props
    const startDate = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD')
    const endDate = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD')
    dateRangeCustom(startDate, endDate)
  }
  render() {
    const DatepickerInput = (props) => (
      <button className="btn btn-default btn-sm daterange-btn" type="button" onClick={props.onClick}>{props.value}</button>
    )
    return (
      <Panel className="filter-block">
        <h4>Выберите период</h4>
        <hr />
          <Col>
            <ButtonGroup size="sm">
              <Button className="daterange-btn" onClick={this.handleDateYesterday}>Вчера</Button>
              <Button className="daterange-btn" onClick={this.handleDateWeek}>Неделя</Button>
            </ButtonGroup>
          </Col>
          <br />
          <Col>
            <ButtonGroup size="sm">
              <Button className="daterange-btn" onClick={this.handleDateMonth}>Этот месяц</Button>
              <Button className="daterange-btn" onClick={this.handleDateLastMonth}>Прошлый месяц</Button>
            </ButtonGroup>
          </Col>
        <hr />
        <Col>
          <DatePicker
            customInput={<DatepickerInput />}
            selected={this.state.startDate}
            selectsStart
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onChange={this.handleStart}
          />
          {'  '}
          <DatePicker
            customInput={<DatepickerInput />}
            selected={this.state.endDate}
            selectsEnd
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onChange={this.handleEnd}
          />
        </Col>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    dateFrom: state.handleFilters.dateFrom,
    dateTo: state.handleFilters.dateTo
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dateRangeStart: (from) => dispatch(dateRangeStart(from)),
    dateRangeEnd: (to) => dispatch(dateRangeEnd(to)),
    dateRangeCustom: (from, to) => dispatch(dateRangeCustom(from, to))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DateRangePicker)