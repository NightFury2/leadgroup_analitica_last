import React, { Component } from 'react'
import { FormGroup, Button, FormFeedback } from 'reactstrap'
import Panel from '../common/Panel'
import { connect } from 'react-redux'
import { chosenCountry, countryListRequest, offerListRequest } from '../../actions/filters'
import { Icon } from 'react-fa'
import Select from 'react-select'

// Этот класс нужен только здесь, поэтому пихнул его поближе к фильтру по странам, чтобы было легче редактировать.

class OptionComponent extends Component {
  constructor(props) {
    super(props)
    this.handleMouseDown = this.handleMouseDown.bind(this)
    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.handleMouseMove = this.handleMouseMove.bind(this)
  }
  handleMouseDown(e) {
    e.preventDefault();
    e.stopPropagation();
    this.props.onSelect(this.props.option, e);
  }
  handleMouseEnter(e) {
    this.props.onFocus(this.props.option, e);
  }
  handleMouseMove(e) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, e);
  }
  render() {
    return (
      <div
        className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
        title={this.props.option.title}
      >
        <span className={"sprite sprite-" + this.props.option.currency} />
        <span>{this.props.children}</span>
      </div>
    )
  }
}

class ValueComponent extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div className="Select-value" title={this.props.value.title}>
				<span className="Select-value-label">
					<span className={"sprite sprite-" + this.props.value.currency} />
          <span>{this.props.children}</span>
				</span>
      </div>
    )
  }
}

class CountryPicker extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const countriesValue = []
    const { countryList, error, chosenCountry, getCountries, token, loading, chosenCountryId } = this.props
    const countries = () => {
      countryList.map((item, index) => {
        countriesValue.push({value: item.id, label: item.name, currency: item.currency})
      })
    }
    return (
      <Panel className="filter-block">
        <h4>Выберите страну</h4>
        <hr />
        <FormGroup>
          {
            countryList.length > 0
              ?
                <Select
                  value={chosenCountryId}
                  optionComponent={OptionComponent}
                  valueComponent={ValueComponent}
                  options={countriesValue}
                  onChange={(val, currency) => {chosenCountry(val, currency, token); }}
                  searchPromptText={"Искать..."}
                  loadingPlaceholder={"Загрузка..."}
                  placeholder={"Страны..."}
                  noResultsText={"Ничего не найдено"}
                  clearable={false}
                />
              :
                error
                  ? null
                  :
                    <Button disabled>Загружаем страны...</Button>
          }
          {
            error
              ?
              <FormFeedback>
                { error }
                <br />
                <Button
                  type="button"
                  onClick={() => getCountries(token)}
                >
                  Попробовать снова {loading ? <Icon name="flag circle fa-spin" /> : null }
                </Button>
              </FormFeedback>
              : countries()
          }
        </FormGroup>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    countryList: state.handleFilters.countryList,
    loading: state.handleFilters.countryIsLoading,
    error: state.handleFilters.countryError,
    chosenCountryId: state.handleFilters.chosenCountryId
  }
}

function mapDispatchToProps(dispatch) {
  return {
    chosenCountry: (val, currency, token) => {dispatch(chosenCountry(val.value, val.currency)); dispatch(offerListRequest(token, val.value))},
    getCountries: (token) => dispatch(countryListRequest(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CountryPicker)