import React, { Component } from 'react'
import { FormGroup, Label, Input } from 'reactstrap'
import Panel from '../common/Panel'

class MultiCountryPicker extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Panel className="filter-block">
        <h4>Выберите страны</h4>
        <hr />
        <FormGroup>
          <Label for="exampleSelectMulti">Выберите страну(ны)</Label>
          <hr />
          <Input type="select" name="selectMulti" id="exampleSelectMulti" multiple>
            <option>Странанейм</option>
            <option>Странанейм</option>
            <option>Странанейм</option>
            <option>Странанейм</option>
            <option>Странанейм</option>
          </Input>
        </FormGroup>
      </Panel>
    )
  }
}

export default MultiCountryPicker