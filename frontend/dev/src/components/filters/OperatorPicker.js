import React, { Component } from 'react'
import { FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap'
import Panel from '../common/Panel'
import { connect } from 'react-redux'
import { chosenOperatorList, clearOperatorList } from '../../actions/filters'
import Select from 'react-select'

class OperatorPicker extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    // Создаем два пустых массива, один для выбранных операторов
    // Другой массив для того, чтобы фильтровать дубликаты
    // При совпадении выбранного оффера, а точнее его ида и
    // одного из оффер идов в операторе, мы проверяем, нет ли такого же ида
    // в массиве alreadyAdded, чтобы удостовериться, что это не дубликат.
    // Если же это не дубликат, мы добавляем этого оператора в
    // массив operatorsValue с его именем и идом, а также добавляем
    // ид этого оператора в alreadyAdded, чтобы не добавлять этого же оператора снова
    // в случае, если этот оператор принадлежит также и к другому офферу.
    let operatorsValue = []
    let alreadyAdded = []
    const { operatorsList, chosenCountryId, selectedOffers, selectedOperators, chooseOperator, clearOperators } = this.props
    const operatorsByOffers = () => {
      return (
        Object.keys(operatorsList).map(operator => {
          let itemOffer = operatorsList[operator]["offer_id"]
          let item = operatorsList[operator]
          selectedOffers.map((offer, offerIndex) => {
            if (itemOffer[offerIndex] == offer.value) {
              if (alreadyAdded.indexOf(item.id) < 0) {
                operatorsValue.push(
                  {
                    value: item.id,
                    label: `${item.login} (${item.name})`
                  }
                )
                alreadyAdded.push(item.id)
              }
            }
          })
        })
      )
    }
    const operatorsByCountries = () => (
      Object.keys(operatorsList).map(operator => {
        let itemCountry = operatorsList[operator]["country_id"]
        let item = operatorsList[operator]
        if (itemCountry == chosenCountryId) {
          operatorsValue.push(
            {
              value: item.id,
              label: `${item.login} (${item.name})`
            }
          )
        }
      })
    )
    return (
      <Panel className="filter-block">
        <FormGroup>
          <h4>Выберите оператора(ов)</h4>
          <p className="muted">Если оператор не выбран, то загрузятся статистики по всем операторам в выбранной стране.</p>
          <hr />
          {
            chosenCountryId
              ?
                <div className="animated fadeIn">
                <Select
                  value={selectedOperators}
                  options={operatorsValue}
                  onChange={chooseOperator}
                  multi={true}
                  searchPromptText={"Искать..."}
                  loadingPlaceholder={"Загрузка..."}
                  placeholder={"Операторы..."}
                  noResultsText={"Ничего не найдено"}
                  clearable={false}
                />
                <hr />
                <Button
                  type="button"
                  size="sm"
                  onClick={ () => chooseOperator(operatorsValue) }
                >
                  Выбрать все
                </Button>
                <Button
                  type="button"
                  size="sm"
                  onClick={ clearOperators }
                >
                  Снять все
                </Button>
              </div>
              :
                <Button disabled>
                  Выберите страну, чтобы загрузить операторов.
                </Button>
          }
          {
            chosenCountryId
              ?
                selectedOffers.length > 0
                  ?
                    operatorsByOffers()
                  :
                    operatorsByCountries()
              : null
          }
        </FormGroup>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    operatorsList: state.handleFilters.operatorsList,
    loading: state.handleFilters.operatorsIsLoading,
    error: state.handleFilters.operatorsError,
    chosenCountryId: state.handleFilters.chosenCountryId,
    selectedOffers: state.handleFilters.selectedOffers,
    selectedOperators: state.handleFilters.selectedOperators
  }
}

function mapDispatchToProps(dispatch) {
  return {
    chooseOperator: (val) => dispatch(chosenOperatorList(val)),
    clearOperators: () => dispatch(clearOperatorList())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OperatorPicker)