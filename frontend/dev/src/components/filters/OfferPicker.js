import React, { Component } from 'react'
import { FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap'
import Panel from '../common/Panel'
import { connect } from 'react-redux'
import { chosenOfferList, clearOfferList } from '../../actions/filters'
import Select from 'react-select'

class OfferPicker extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const offersValue = [];
    const { offerList, chosenCountryId, selectedOffers, chooseOffer, clearOffers } = this.props;
    const offersById = () => (
      offerList.map((item) => {
        offersValue.push(
          {
            value: item.id,
            label: `${item.title} (${item.name})`,
            name: item.name
          }
        )
      })
    );
    return (
      <Panel className="filter-block">
        <FormGroup>
          <h4>Выберите оффер(ы)</h4>
          <p className="muted">Если оффер не выбран, то загрузятся данные по всем офферам в выбранной стране.</p>
          <hr />
          {
            chosenCountryId
              ?
                <div className="animated fadeIn">
                  <Select
                    value={selectedOffers}
                    options={offersValue}
                    onChange={chooseOffer}
                    multi={true}
                    searchPromptText={"Искать..."}
                    loadingPlaceholder={"Загрузка..."}
                    placeholder={"Офферы..."}
                    noResultsText={"Ничего не найдено"}
                    clearable={false}
                  />
                  <hr />
                  <Button
                    type="button"
                    size="sm"
                    onClick={ () => chooseOffer(offersValue) }
                  >
                    Выбрать все
                  </Button>
                  <Button
                    type="button"
                    size="sm"
                    onClick={ clearOffers }
                  >
                    Снять все
                  </Button>
                </div>
              : null
          }
          {
            chosenCountryId
              ?
                offersById()
              :
                <Button disabled>
                  Выберите страну, чтобы загрузить офферы.
                </Button>
          }
        </FormGroup>
      </Panel>
    )
  }
}

function mapStateToProps(state) {
  return {
    token: state.handleLogin.accessToken,
    offerList: state.handleFilters.offerList,
    loading: state.handleFilters.offerIsLoading,
    error: state.handleFilters.offerError,
    chosenCountryId: state.handleFilters.chosenCountryId,
    selectedOffers: state.handleFilters.selectedOffers
  }
}

function mapDispatchToProps(dispatch) {
  return {
    chooseOffer: (val) => dispatch(chosenOfferList(val)),
    clearOffers: () => dispatch(clearOfferList())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OfferPicker)