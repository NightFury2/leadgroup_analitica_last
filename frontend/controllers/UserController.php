<?php

namespace frontend\controllers;

use yii;
use common\models\User;
use common\models\Profile;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use filsh\yii2\oauth2server\models\OauthRefreshTokens;
use filsh\yii2\oauth2server\models\OauthAccessTokens;

class UserController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'edit' => ['POST']
                    ],
                ],
            ]);
    }

    private function identify()
    {
        return User::findIdentityByAccessToken(Yii::$app->getModule('oauth2')
            ->getServer()
            ->getResourceController()
            ->getToken()
        );
    }

    public function actionLogout()
    {
        $user = $this->identify();

        //TODO: на модель

        Yii::$app->db->createCommand('DELETE FROM oauth_access_tokens WHERE user_id = :id', [
            ':id' => $user->id
        ])->execute();

        Yii::$app->db->createCommand('DELETE FROM oauth_refresh_tokens WHERE user_id = :id', [
            ':id' => $user->id
        ])->execute();
    }

    public function actionIndex()
    {
        $user = $this->identify();
        $profile = Profile::findOne(['user_id' => $user->id]);

        return [
            'username' => $user->username,
            'email' => $user->email,
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name,
            'photo' => $profile->photo,
        ];
    }

    public function actionUpdate()
    {
        $user = $this->identify();
        $profile = Profile::findOne(['user_id' => $user->id]);

        $user->scenario = 'update';
        $profile->scenario = 'update';

        $user->load(Yii::$app->request->post(), '');
        $profile->load(Yii::$app->request->post(), '');

        if ($user->validate() && $profile->validate()) {
            $user->save(false);
            $profile->save(false);

            return true;
        } else {
            return ActiveForm::validateMultiple([$user, $profile]);
        }
    }

}