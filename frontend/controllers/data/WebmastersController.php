<?php

namespace frontend\controllers\data;

use frontend\controllers\FrontendController;
use frontend\models\Webmaster;
use Yii;

class WebmastersController extends FrontendController
{
    public function actionIndex()
    {
        return Webmaster::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);

            $webmasters = Webmaster::find()
                ->where(['country_id' => $countryId]);

            return $webmasters->all();
        });
    }
}