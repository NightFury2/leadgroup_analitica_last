<?php

namespace frontend\controllers\data;

use frontend\controllers\FrontendController;

class AvatarsController extends FrontendController
{
    public function actionIndex()
    {
        $avatars = [];

        for ($i = 0; $i !== 10; $i++) {
            $avatars[] = '/images/' . $i . '.png';
        }

        return $avatars;
    }
}