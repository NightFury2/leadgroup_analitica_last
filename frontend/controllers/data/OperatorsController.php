<?php

namespace frontend\controllers\data;

use Yii;
use frontend\models\Operator;
use frontend\controllers\FrontendController;

class OperatorsController extends FrontendController
{
    public function actionIndex()
    {
        return Operator::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);

            $queries = Operator::find()->select([
                'operator.id as operator_id',
                'login',
                'name',
                'country_id',
                'offer_id'
            ])->leftJoin('operator_to_offer', [
                'operator.id' => new \yii\db\Expression('operator_to_offer.operator_id')
            ]);

            $queries->andWhere(['country_id' => $countryId]);

            $operators = [];

            foreach ($queries->all() as $operator) {
                if (!isset($operators[$operator['operator_id']])) {
                    $operators[$operator['operator_id']] = [
                        'id' => $operator['operator_id'],
                        'login' => $operator['login'],
                        'name' => $operator['name'],
                        'country_id' => $operator['country_id'],
                        'offer_id' => [
                            $operator['offer_id']
                        ]
                    ];
                } else {
                    $operators[$operator['operator_id']]['offer_id'][] = $operator['offer_id'];
                }
            }

            return $operators;

        });
    }
}