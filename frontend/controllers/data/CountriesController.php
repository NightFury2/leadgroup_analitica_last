<?php

namespace frontend\controllers\data;

use frontend\controllers\FrontendController;
use Yii;
use frontend\models\Country;

class CountriesController extends FrontendController
{
    public function actionIndex()
    {
        return Country::getDb()->cache(function ($db) {
            return Country::find()->all();
        });
    }
}