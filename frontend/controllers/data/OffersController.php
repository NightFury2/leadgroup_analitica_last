<?php

namespace frontend\controllers\data;

use frontend\controllers\FrontendController;
use frontend\models\Offer;
use Yii;
use yii\db\Expression;

class OffersController extends FrontendController
{
    public function actionIndex()
    {
        return Offer::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);

            $offers = Offer::find()->where(['country_id' => $countryId]);

            return $offers->all();
        });
    }
}