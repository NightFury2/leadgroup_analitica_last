<?php

namespace frontend\controllers\statistic;

use frontend\models\OperatorStatistic;
use frontend\controllers\FrontendController;
use Yii;

class OperatorStatisticController extends FrontendController
{

    public function actionIndex()
    {
        return OperatorStatistic::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);
            $dateFrom = Yii::$app->request->get('date_from', false);
            $dateTo = Yii::$app->request->get('date_to', false);
            $operatorId = Yii::$app->request->get('operator_id', false);

            $statistics = OperatorStatistic::find();

            $statistics->where(['country_id' => $countryId]);

            $statistics->select([
                'operator_id',
                'country_id',
                'date',
                'sum(total) as total',
                'sum(approved) as approved',
                'sum(buyout) as buyout',
                'avg(average_bill) as average_bill',
                'avg(average_bill_usd) as average_bill_usd'
            ])->groupBy(['operator_id'])
                ->orderBy(['date' => SORT_DESC]);

            if ($dateFrom && $dateTo) {
                //Если переданы даты ОТ и ДО - применяем их
                $statistics->andWhere('date >= :date_from AND date <= :date_to', [
                    ':date_from' => $dateFrom,
                    ':date_to' => $dateTo
                ]);
            } else {
                //Либо ставим интервал в неделю
                $statistics->andWhere("date >= (CURDATE() + INTERVAL - 7 DAY)");
            }

            if ($operatorId) {
                $statistics->andWhere(['operator_id' => explode(',', $operatorId)]);
            }

            return $statistics->all();
        });
    }

}