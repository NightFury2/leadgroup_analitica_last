<?php

namespace frontend\controllers\statistic;

use frontend\models\ConverseStatistic;
use frontend\controllers\FrontendController;
use Yii;
use yii\db\Expression;

class ConversionsController extends FrontendController
{
    /**
     * date_from - дата начала
     * date_to - дата конца
     * (дефолт за последнюю неделю)
     *
     * offer_id - id офферов
     * country_id - id страны
     *
     * apart - статистика раздельно для каждого оффера
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return ConverseStatistic::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);
            $offerId = Yii::$app->request->get('offer_id', false);
            $apart = Yii::$app->request->get('apart', 0);
            $dateFrom = Yii::$app->request->get('date_from', false);
            $dateTo = Yii::$app->request->get('date_to', false);

            $conversions = ConverseStatistic::find();

            if ($apart != 1) {
                //Если раздельная статистика не нужна (в целом по стране)
                $conversions->select([
                        'date', 'sum(total) as total', 'sum(approved) as approved',
                        'sum(canceled) as canceled', 'sum(error) as error',
                        'sum(`return`) as `return`', 'sum(paid) as paid',
                        'avg(average_bill) as average_bill',
                        'avg(average_bill_usd) as average_bill_usd',
                        'sum(processed) as processed',
                        'country_id'
                    ])->groupBy(['date'])
                    ->orderBy(['date' => SORT_DESC]);
            } else {
                //Если нужна статистика раздельно для каждого оффера
                $conversions->select([
                    'date', 'total', 'approved',
                    'canceled', 'error',
                    '`return`', 'paid',
                    'average_bill',
                    'average_bill_usd',
                    'processed',
                    new Expression('converse_statistic.country_id'),
                    'offer_id'
                ])->orderBy(['offer_id' => SORT_DESC])
                    ->leftJoin('offer', ['offer_id' => new Expression('offer.id')])
                    ->andWhere(new Expression('offer.id IS NOT NULL'));
            }

            $conversions->andwhere([
                'converse_statistic.country_id' => $countryId]);

            //Если передан оффер ID
            if ($offerId) {
                $offers = explode(',', $offerId);
                $conversions->andWhere(['offer_id' => $offers]);
            }

            if ($dateFrom && $dateTo) {
                //Если переданы даты ОТ и ДО - применяем их
                $conversions->andWhere('date >= :date_from AND date <= :date_to', [
                    ':date_from' => $dateFrom,
                    ':date_to' => $dateTo
                ]);
            } else {
                //Либо ставим интервал в неделю
                $conversions->andWhere("date >= (CURDATE() + INTERVAL - 7 DAY)");
            }

            $conversions = $conversions->all();

            if ($apart == 1) {
                //Раздельно по офферам
                $apartConversions = [];

                //id оффера => [стата]
                foreach ($conversions as $conversion) {
                    $apartConversions[$conversion['offer_id']][] = $conversion;
                }

                return $apartConversions;
            } else {
                return $conversions;
            }

        });
    }
}