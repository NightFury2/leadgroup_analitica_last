<?php

namespace frontend\controllers\statistic;

use frontend\controllers\FrontendController;
use Yii;
use frontend\models\WebmasterStatistic;

class WebmasterStatisticController extends FrontendController
{
    public function actionIndex()
    {
        return WebmasterStatistic::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);
            $offerId = Yii::$app->request->get('offer_id', false);
            $wmId = Yii::$app->request->get('wm_id', false);
            $dateFrom = Yii::$app->request->get('date_from', false);
            $dateTo = Yii::$app->request->get('date_to', false);

            $webmaster = WebmasterStatistic::find();

            $webmaster->select([
                'date', 'sum(total) as total', 'sum(approved) as approved',
                'sum(canceled) as canceled', 'sum(error) as error',
                'sum(`return`) as `return`', 'sum(paid) as paid',
                'country_id'
            ])->orderBy(['date' => SORT_DESC]);


            $webmaster->where('country_id = :country_id', [
                ':country_id' => $countryId
            ]);

            //Если передан оффер ID
            if ($offerId) {
                $webmaster->andWhere('offer_id IN (:offer_id)', [
                    ':offer_id' => $offerId
                ]);
            }

            if ($wmId) {
                $webmaster->andWhere(['wm_id' => explode(',', $wmId)]);
            }

            if ($dateFrom && $dateTo) {
                //Если переданы даты ОТ и ДО - применяем их
                $webmaster->andWhere('date >= :date_from AND date <= :date_to', [
                    ':date_from' => $dateFrom,
                    ':date_to' => $dateTo
                ]);
            } else {
                //Либо ставим интервал в неделю
                $webmaster->andWhere("date >= (CURDATE() + INTERVAL - 7 DAY)");
            }

            $webmaster = $webmaster->all();

            return $webmaster;

        });
    }
}