<?php

namespace frontend\controllers\statistic;

use frontend\models\ConverseStatistic;
use frontend\controllers\FrontendController;
use Yii;

class BuyoutController extends FrontendController
{

    public function actionIndex()
    {
        return ConverseStatistic::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);
            $dateFrom = Yii::$app->request->get('date_from', false);
            $dateTo = Yii::$app->request->get('date_to', false);
            $offerId = Yii::$app->request->get('offer_id', false);

            $buyout = ConverseStatistic::find();

            $buyout->select([
                'date',
                'sum(approved) as total',
                'sum(paid) as paid',
                'sum(`return`) as `return`',
                'sum(processed) as processed',
                'country_id',
                'offer_id'
            ])->groupBy(['date', 'country_id'])
                ->orderBy(['date' => SORT_DESC]);

            $buyout->where('country_id = :country_id', [
                ':country_id' => $countryId
            ]);

            //Если передан оффер ID
            if ($offerId) {
                $buyout->andWhere('offer_id IN (:offer_id)', [
                    ':offer_id' => $offerId
                ]);
            }

            if ($dateFrom && $dateTo) {
                //Если переданы даты ОТ и ДО - применяем их
                $buyout->andWhere('date >= :date_from AND date <= :date_to', [
                    ':date_from' => $dateFrom,
                    ':date_to' => $dateTo
                ]);
            } else {
                //Либо ставим интервал в неделю
                $buyout->andWhere("date >= (CURDATE() + INTERVAL - 7 DAY)");
            }

            $buyout = $buyout->all();

            return $buyout;
        });
    }
}