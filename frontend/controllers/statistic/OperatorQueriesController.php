<?php

namespace frontend\controllers\statistic;

use frontend\models\OperatorQueryStatistic;
use frontend\controllers\FrontendController;
use Yii;

class OperatorQueriesController extends FrontendController
{

    public function actionIndex()
    {
        return OperatorQueryStatistic::getDb()->cache(function ($db) {
            $countryId = Yii::$app->request->get('country_id', Yii::$app->params['defaultCountry']);
            $operatorId = Yii::$app->request->get('operator_id', false);
            $dateFrom = Yii::$app->request->get('date_from', false);
            $dateTo = Yii::$app->request->get('date_to', false);
            $offerId = Yii::$app->request->get('offer_id', false);

            $queries = OperatorQueryStatistic::find()
                ->select([
                    'operator_query_statistic.operator_id as operator_id',
                    'count',
                    'DATE(datetime) as `date`',
                    "TIME_FORMAT(datetime, '%H:%i') as `time`"
                ])->leftJoin('operator', [
                    'operator.id' => new \yii\db\Expression('operator_id')
                ])
                ->where('country_id = :country_id', [':country_id' => $countryId])
                ->orderBy(['date' => SORT_DESC]);

            if ($operatorId) {
                $queries->andWhere(['operator_id' => explode(',', $operatorId)]);
            }

            if ($dateFrom && $dateTo) {
                //Если переданы даты ОТ и ДО - применяем их
                $queries->andWhere('DATE(datetime) >= :date_from AND DATE(datetime) <= :date_to', [
                    ':date_from' => $dateFrom,
                    ':date_to' => $dateTo
                ]);
            } else {
                //Либо ставим 3 дня
                $queries->andWhere("DATE(datetime) >= (CURDATE() + INTERVAL - 2 DAY)");
            }

            if ($offerId) {
                $queries->leftJoin('operator_to_offer', [
                    'operator.id' => new \yii\db\Expression('operator_to_offer.operator_id')
                ])->andWhere(['offer_id' => explode(',', $offerId)]);
            }

            $grouped = [];

            //Собираем ответ
            foreach ($queries->all() as $query) {
                if (!isset($grouped[$query['operator_id']])) {
                    $grouped[$query['operator_id']] = [];
                }

                if (!isset($grouped[$query['operator_id']][$query['date']])) {
                    $grouped[$query['operator_id']][$query['date']] = [];
                }

                $grouped[$query['operator_id']][$query['date']][$query['time']] = (int)$query['count'];
            }

            /*
            foreach ($grouped as $operatorId => $dates) { //Каждого оператора
                foreach ($dates as $date => $day) { //Каждую дату
                    for ($hour = 0; $hour < 24; $hour++) { //Каждый час
                        for ($minutes = 0; $minutes < 60; $minutes += 5) { //Каждую минуту
                            $time = ($hour < 10 ? '0' . $hour : $hour) . ':' . ($minutes < 10 ? '0' . $minutes : $minutes); //Собираем время

                            if (!isset($grouped[$operatorId][$date][$time])) {
                                $grouped[$operatorId][$date][$time] = 0;
                            }
                        }
                    }

                    ksort($grouped[$operatorId][$date]);
                }
            }
            */
            return $grouped;
        });

    }

}